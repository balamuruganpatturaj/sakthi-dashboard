(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-SG": "./node_modules/moment/locale/en-SG.js",
	"./en-SG.js": "./node_modules/moment/locale/en-SG.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _services_authguard_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./services/authguard.service */ "./src/app/services/authguard.service.ts");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/dashboard/dashboard.component.ts");






var routes = [
    { path: '', component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_5__["DashboardComponent"], canActivate: [_services_authguard_service__WEBPACK_IMPORTED_MODULE_4__["AuthguardService"]] },
    { path: 'dashboard', component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_5__["DashboardComponent"] },
    { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] },
    { path: '**', redirectTo: '' }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <div *ngIf=\"appService.showLoading\">\n\t<mat-progress-spinner class=\"example-margin\" [color]=\"primary\"\n\t\t[mode]=\"indeterminate\" [value]=\"100\"> </mat-progress-spinner>\n</div> -->\n<main style=\"max-height:99.9%;overflow:auto;height:100%\">\n\t<router-outlet></router-outlet>\n</main>\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.service */ "./src/app/app.service.ts");



var AppComponent = /** @class */ (function () {
    function AppComponent(appService) {
        this.appService = appService;
        this.title = 'Sakthi Portal';
        console.log("This is Sakthi Portal component");
    }
    AppComponent.prototype.ngOnInit = function () { };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].Default,
            selector: 'app-root',
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/overlay */ "./node_modules/@angular/cdk/esm5/overlay.es5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _services_authguard_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./services/authguard.service */ "./src/app/services/authguard.service.ts");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./app.service */ "./src/app/app.service.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _services_auth_interceptor_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./services/auth-interceptor.service */ "./src/app/services/auth-interceptor.service.ts");
/* harmony import */ var _purchase_history_purchase_history_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./purchase-history/purchase-history.component */ "./src/app/purchase-history/purchase-history.component.ts");
/* harmony import */ var _dynamic_component_dashboard_dynamic_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./dynamic-component/dashboard-dynamic-component */ "./src/app/dynamic-component/dashboard-dynamic-component.ts");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var _supplier_master_supplier_master_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./supplier-master/supplier-master.component */ "./src/app/supplier-master/supplier-master.component.ts");
/* harmony import */ var _purchase_acknowledgement_purchase_acknowledgement_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./purchase-acknowledgement/purchase-acknowledgement.component */ "./src/app/purchase-acknowledgement/purchase-acknowledgement.component.ts");
/* harmony import */ var _purchase_entry_purchase_entry_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./purchase-entry/purchase-entry.component */ "./src/app/purchase-entry/purchase-entry.component.ts");
/* harmony import */ var _user_management_user_management_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./user-management/user-management.component */ "./src/app/user-management/user-management.component.ts");
/* harmony import */ var _user_management_customer_management_customer_management_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./user-management/customer-management/customer-management.component */ "./src/app/user-management/customer-management/customer-management.component.ts");
/* harmony import */ var _user_management_login_user_management_login_user_management_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./user-management/login-user-management/login-user-management.component */ "./src/app/user-management/login-user-management/login-user-management.component.ts");


























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            declarations: [_app_component__WEBPACK_IMPORTED_MODULE_11__["AppComponent"], _login_login_component__WEBPACK_IMPORTED_MODULE_15__["LoginComponent"], _dynamic_component_dashboard_dynamic_component__WEBPACK_IMPORTED_MODULE_18__["DashboardDynamicComponent"], _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_19__["DashboardComponent"], _purchase_history_purchase_history_component__WEBPACK_IMPORTED_MODULE_17__["PurchaseHistoryComponent"], _supplier_master_supplier_master_component__WEBPACK_IMPORTED_MODULE_20__["SupplierMasterComponent"], _purchase_acknowledgement_purchase_acknowledgement_component__WEBPACK_IMPORTED_MODULE_21__["PurchaseAcknowledgementComponent"], _purchase_entry_purchase_entry_component__WEBPACK_IMPORTED_MODULE_22__["PurchaseEntryComponent"], _user_management_user_management_component__WEBPACK_IMPORTED_MODULE_23__["UserManagementComponent"], _user_management_customer_management_customer_management_component__WEBPACK_IMPORTED_MODULE_24__["CustomerManagementComponent"], _user_management_login_user_management_login_user_management_component__WEBPACK_IMPORTED_MODULE_25__["LoginUserManagementComponent"], _user_management_login_user_management_login_user_management_component__WEBPACK_IMPORTED_MODULE_25__["DeleteUserDialog"]],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_10__["AppRoutingModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatProgressSpinnerModule"],
                _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_1__["LayoutModule"],
                _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_2__["OverlayModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatAutocompleteModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatOptionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSliderModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatButtonToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatChipsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogModule"]
            ],
            providers: [
                _services_authguard_service__WEBPACK_IMPORTED_MODULE_12__["AuthguardService"],
                _services_authentication_service__WEBPACK_IMPORTED_MODULE_13__["AuthenticationService"],
                _app_service__WEBPACK_IMPORTED_MODULE_14__["AppService"],
                { provide: _angular_common__WEBPACK_IMPORTED_MODULE_9__["LocationStrategy"], useClass: _angular_common__WEBPACK_IMPORTED_MODULE_9__["HashLocationStrategy"] },
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HTTP_INTERCEPTORS"], useClass: _services_auth_interceptor_service__WEBPACK_IMPORTED_MODULE_16__["HttpsRequestInterceptor"], multi: true }
            ],
            entryComponents: [_user_management_login_user_management_login_user_management_component__WEBPACK_IMPORTED_MODULE_25__["DeleteUserDialog"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_11__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.service.ts":
/*!********************************!*\
  !*** ./src/app/app.service.ts ***!
  \********************************/
/*! exports provided: AppService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppService", function() { return AppService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _util_AppConstants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./util/AppConstants */ "./src/app/util/AppConstants.ts");
/* harmony import */ var _services_index__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./services/index */ "./src/app/services/index.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./../environments/environment */ "./src/environments/environment.ts");







var AppService = /** @class */ (function () {
    function AppService(http, router, authenticationService) {
        this.http = http;
        this.router = router;
        this.authenticationService = authenticationService;
        this.purchaseHistEmit = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.showLoading = false;
        this.urlPath = '';
        this.baseRestUrl = _util_AppConstants__WEBPACK_IMPORTED_MODULE_4__["BaseRestURL"];
        if (!_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].production) {
            this.urlPath = "/SakthiPortal/";
        }
    }
    AppService.prototype.ngOnInit = function () {
        console.log("environment.production: " + _environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].production);
    };
    AppService.prototype.getPurchaseHistoryDetails = function () {
        this.url = this.urlPath + "rest/purchase/getPurchaseHistory";
        return this.http.get(this.url);
    };
    AppService.prototype.getProductNames = function () {
        this.url = this.urlPath + "rest/purchase/getProductNames";
        return this.http.get(this.url);
    };
    AppService.prototype.getProductCategory = function (productName) {
        if (productName != null) {
            this.url = this.urlPath + "rest/purchase/getProductCategory/" + productName;
        }
        else {
            this.url = this.urlPath + "rest/purchase/getProductCategory";
        }
        return this.http.get(this.url);
    };
    AppService.prototype.savePurchaseRecord = function (purchaseData) {
        this.url = this.urlPath + "rest/purchase/addUpdatePurchase";
        return this.http.post(this.url, JSON.stringify(purchaseData), { headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]().set('Content-Type', 'application/json') });
    };
    AppService.prototype.isTokenExpired = function () {
        if (!this.authenticationService.isLoggedIn()) {
            this.authenticationService.logout();
            this.router.navigate(['/login']);
            return false;
        }
        return true;
    };
    AppService.prototype.showLoader = function () {
        var _this = this;
        setTimeout(function () {
            _this.showLoading = true;
        }, 0);
    };
    AppService.prototype.hideLoader = function () {
        var _this = this;
        setTimeout(function () {
            _this.showLoading = false;
        }, 0);
    };
    AppService.prototype.emitChangeEvent = function (page) {
        if (page === "purchasehist")
            this.purchaseHistEmit.emit();
    };
    AppService.prototype.getChangeEvent = function (page) {
        if (page === "purchasehist")
            return this.purchaseHistEmit;
    };
    AppService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _services_index__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"]])
    ], AppService);
    return AppService;
}());



/***/ }),

/***/ "./src/app/dashboard/dashboard.component.css":
/*!***************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".boxShadow{\n    box-shadow: 0 0px 0 rgba(255, 255, 255, 0.25) inset !important;\n}\n#custom-container{\n    padding: 2px 25px 0 20px;\n    height: 8.6rem;\n}\n#list-menu{\n    font-weight: normal;\n}\n.hideSideBar{\n    margin-left: 0px !important;\n    /* display: -webkit-box !important; */\n}\n.showSidebar{\n    margin-left: 265px !important;\n}\n#loggedUser{\n    padding-right: 20px;\n    font-size: 16px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGFzaGJvYXJkL2Rhc2hib2FyZC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksK0RBQStEO0NBQ2xFO0FBQ0Q7SUFDSSx5QkFBeUI7SUFDekIsZUFBZTtDQUNsQjtBQUNEO0lBQ0ksb0JBQW9CO0NBQ3ZCO0FBQ0Q7SUFDSSw0QkFBNEI7SUFDNUIsc0NBQXNDO0NBQ3pDO0FBQ0Q7SUFDSSw4QkFBOEI7Q0FDakM7QUFDRDtJQUNJLG9CQUFvQjtJQUNwQixnQkFBZ0I7Q0FDbkIiLCJmaWxlIjoic3JjL2FwcC9kYXNoYm9hcmQvZGFzaGJvYXJkLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYm94U2hhZG93e1xuICAgIGJveC1zaGFkb3c6IDAgMHB4IDAgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjI1KSBpbnNldCAhaW1wb3J0YW50O1xufVxuI2N1c3RvbS1jb250YWluZXJ7XG4gICAgcGFkZGluZzogMnB4IDI1cHggMCAyMHB4O1xuICAgIGhlaWdodDogOC42cmVtO1xufVxuI2xpc3QtbWVudXtcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xufVxuLmhpZGVTaWRlQmFye1xuICAgIG1hcmdpbi1sZWZ0OiAwcHggIWltcG9ydGFudDtcbiAgICAvKiBkaXNwbGF5OiAtd2Via2l0LWJveCAhaW1wb3J0YW50OyAqL1xufVxuLnNob3dTaWRlYmFye1xuICAgIG1hcmdpbi1sZWZ0OiAyNjVweCAhaW1wb3J0YW50O1xufVxuI2xvZ2dlZFVzZXJ7XG4gICAgcGFkZGluZy1yaWdodDogMjBweDtcbiAgICBmb250LXNpemU6IDE2cHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.html":
/*!****************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container id=\"sidebar\">\n\t<mat-nav-list> <a mat-list-item\n\t\t[routerLinkActive]=\"'active'\" [routerLink]=\"['/dashboard']\"> <mat-icon\n\t\t\tclass=\"sidenav-icon\">dashboard</mat-icon>Sales\n\t</a> <a mat-list-item [routerLinkActive]=\"'active'\"\n\t\t[routerLink]=\"['/charts']\"> <mat-icon class=\"sidenav-icon\">bar_chart</mat-icon>\n\t\tInventory\n\t</a>\n\t<div class=\"nested-menu\">\n\t\t<a mat-list-item (click)=\"addExpandClass('pages')\"> <mat-icon\n\t\t\t\tclass=\"sidenav-icon\">keyboard_arrow_right</mat-icon> Purchase\n\t\t</a>\n\t\t<ul class=\"submenu\" [class.expand]=\"showMenu === 'pages'\">\n\t\t\t<li><a mat-list-item [routerLinkActive]=\"'active'\"\n\t\t\t\t(click)=\"loadComponent('purchasehist')\">Purchase Acknowledgement</a></li>\n\t\t</ul>\n\t\t<ul class=\"nested submenu\" [class.expand]=\"showMenu === 'pages'\">\n\t\t\t<li><a mat-list-item [routerLinkActive]=\"'active'\"\n\t\t\t\t(click)=\"loadComponent('purchasehist')\">Purchase Entry</a></li>\n\t\t</ul>\n\t\t<ul class=\"nested submenu\" [class.expand]=\"showMenu === 'pages'\">\n\t\t\t<li><a mat-list-item [routerLinkActive]=\"'active'\"\n\t\t\t\t(click)=\"loadComponent('purchasehist')\"> Purchase History </a></li>\n\t\t</ul>\n\t</div>\n\t<a mat-list-item [routerLinkActive]=\"'active'\" [routerLink]=\"['/usermanage']\" (click)=\"loadComponent('usermanage')\"> <mat-icon\n\t\t\tclass=\"sidenav-icon\">how_to_reg</mat-icon> People Management\n\t</a>\n\t</mat-nav-list>\n</mat-sidenav-container>\n\n<mat-toolbar color=\"primary\" class=\"fix-nav\">\n<button type=\"button\" mat-icon-button class=\"visible-md\"\n\t(click)=\"toggleSidebar()\">\n\t<mat-icon aria-label=\"Side nav toggle icon\">menu</mat-icon>\n</button>\n<div class=\"nav-brand\">Sakthi Groups</div>\n<span class=\"nav-spacer\"></span>\n\n<button class=\"hidden-sm\" mat-icon-button [matMenuTriggerFor]=\"profile\">\n\t<mat-icon>account_circle</mat-icon>\n</button>\n<mat-menu #profile=\"matMenu\">\n<button mat-menu-item>\n\t<mat-icon>person</mat-icon>\n\t<span>Profile</span>\n</button>\n<button mat-menu-item>\n\t<mat-icon>inbox</mat-icon>\n\t<span>Inbox</span>\n</button>\n<button mat-menu-item>\n\t<mat-icon>settings</mat-icon>\n\t<span>Settings</span>\n</button>\n</mat-menu>\n<button mat-icon-button (click)=\"onLoggedout()\">\n\t<mat-icon>exit_to_app</mat-icon>\n</button>\n</mat-toolbar>\n<div class=\"main-container\">\n    <dynamic-component [componentData]=\"componentData\"></dynamic-component>\n</div>\n<!-- <div style=\"height: 88%;\" class=\"content main-container\">\n\t<div class=\"container\"\n\t\tstyle=\"padding: 0 15px; margin: 0px !important; width: 100%; height: 100%;\">\n\t\t<dynamic-component [componentData]=\"componentData\"></dynamic-component>\n\t</div>\n</div> -->\n"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.ts":
/*!**************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.ts ***!
  \**************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _purchase_history_purchase_history_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../purchase-history/purchase-history.component */ "./src/app/purchase-history/purchase-history.component.ts");
/* harmony import */ var _user_management_user_management_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../user-management/user-management.component */ "./src/app/user-management/user-management.component.ts");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../app.service */ "./src/app/app.service.ts");







var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(router, appService, authentication) {
        var _this = this;
        this.router = router;
        this.appService = appService;
        this.authentication = authentication;
        this.componentData = null;
        this.isManageEntitlementActive = true;
        this.isDownloadPackageActive = false;
        this.isEntitlementAdminActive = false;
        this.isUserManagementActive = false;
        this.isShowSidebar = true;
        this.isAdminDrawerOpened = true;
        this.isLibraryDrawerOpened = true;
        this.userDetails = { userName: "", userRole: "" };
        this.appService.isTokenExpired();
        this.router.events.subscribe(function (val) {
            if (val instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"] && window.innerWidth <= 992 && _this.isToggled()) {
                _this.toggleSidebar();
            }
        });
    }
    DashboardComponent.prototype.ngOnInit = function () {
        console.log(" This is dashboard component");
        this.showMenu = '';
        this.pushRightClass = "push-right";
        this.appService.showLoader();
        if (localStorage.getItem('routePage') === "purchasehist") {
            this.loadComponent('purchasehist');
        }
        else if (localStorage.getItem('routePage') === "usermanage") {
            this.loadComponent('usermanage');
        }
        //this.subscription = this.appService.getChangeEvent( 'dashboard' ).subscribe( item => this.loadComponent( 'manage' ) );
        //this.subscription = this.appService.getChangeEvent( 'usermanage' ).subscribe( item => this.loadComponent( 'userManagement' ) );
    };
    DashboardComponent.prototype.loadComponent = function (component) {
        this.appService.showLoader();
        if (component === 'purchasehist') {
            this.componentData = {
                component: _purchase_history_purchase_history_component__WEBPACK_IMPORTED_MODULE_3__["PurchaseHistoryComponent"], inputs: {}
            };
        }
        else if (component === 'usermanage') {
            this.componentData = {
                component: _user_management_user_management_component__WEBPACK_IMPORTED_MODULE_4__["UserManagementComponent"], inputs: {}
            };
        }
    };
    DashboardComponent.prototype.addExpandClass = function (element) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        }
        else {
            this.showMenu = element;
        }
    };
    DashboardComponent.prototype.changeFocus = function (lnpName) {
        if (lnpName === "manage") {
            this.isManageEntitlementActive = true;
            this.isDownloadPackageActive = false;
            this.isEntitlementAdminActive = false;
            this.isUserManagementActive = false;
        }
        else if (lnpName === "download") {
            this.isManageEntitlementActive = false;
            this.isDownloadPackageActive = true;
            this.isEntitlementAdminActive = false;
            this.isUserManagementActive = false;
        }
        else if (lnpName === "admin") {
            this.isManageEntitlementActive = false;
            this.isDownloadPackageActive = false;
            this.isEntitlementAdminActive = true;
            this.isUserManagementActive = false;
        }
        else if (lnpName === "userManagement") {
            this.isManageEntitlementActive = false;
            this.isDownloadPackageActive = false;
            this.isEntitlementAdminActive = false;
            this.isUserManagementActive = true;
        }
    };
    DashboardComponent.prototype.isToggled = function () {
        var dom = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    };
    DashboardComponent.prototype.toggleSidebar = function () {
        var dom = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    };
    DashboardComponent.prototype.onLoggedout = function () {
        localStorage.removeItem('isLoggedin');
        this.router.navigate(['/login']);
    };
    DashboardComponent.prototype.logout = function () {
        this.appService.showLoader();
        this.authentication.logout();
        location.reload();
    };
    DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'lca-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.css */ "./src/app/dashboard/dashboard.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _app_service__WEBPACK_IMPORTED_MODULE_6__["AppService"], _services_authentication_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/dynamic-component/dashboard-dynamic-component.ts":
/*!******************************************************************!*\
  !*** ./src/app/dynamic-component/dashboard-dynamic-component.ts ***!
  \******************************************************************/
/*! exports provided: DashboardDynamicComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardDynamicComponent", function() { return DashboardDynamicComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _purchase_history_purchase_history_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../purchase-history/purchase-history.component */ "./src/app/purchase-history/purchase-history.component.ts");
/* harmony import */ var _user_management_user_management_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../user-management/user-management.component */ "./src/app/user-management/user-management.component.ts");




var DashboardDynamicComponent = /** @class */ (function () {
    function DashboardDynamicComponent(resolver) {
        this.resolver = resolver;
        this.currentComponent = null;
    }
    Object.defineProperty(DashboardDynamicComponent.prototype, "componentData", {
        set: function (data) {
            if (!data) {
                return;
            }
            // Inputs need to be in the following format to be resolved properly
            var inputProviders = Object.keys(data.inputs).map(function (inputName) { return { provide: inputName, useValue: data.inputs[inputName] }; });
            var resolvedInputs = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ReflectiveInjector"].resolve(inputProviders);
            // We create an injector out of the data we want to pass down and this components injector
            var injector = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ReflectiveInjector"].fromResolvedProviders(resolvedInputs, this.dynamicComponentContainer.parentInjector);
            // We create a factory out of the component we want to create
            var factory = this.resolver.resolveComponentFactory(data.component);
            // We create the component using the factory and the injector
            var component = factory.create(injector);
            // We insert the component into the dom container
            this.dynamicComponentContainer.insert(component.hostView);
            // We can destroy the old component is we like by calling destroy
            if (this.currentComponent) {
                this.currentComponent.destroy();
            }
            this.currentComponent = component;
        },
        enumerable: true,
        configurable: true
    });
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('dynamicComponentContainer', { read: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"] }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"])
    ], DashboardDynamicComponent.prototype, "dynamicComponentContainer", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object])
    ], DashboardDynamicComponent.prototype, "componentData", null);
    DashboardDynamicComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'dynamic-component',
            entryComponents: [
                _purchase_history_purchase_history_component__WEBPACK_IMPORTED_MODULE_2__["PurchaseHistoryComponent"],
                _user_management_user_management_component__WEBPACK_IMPORTED_MODULE_3__["UserManagementComponent"]
            ],
            template: "\n    <div #dynamicComponentContainer></div>\n  "
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ComponentFactoryResolver"]])
    ], DashboardDynamicComponent);
    return DashboardDynamicComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"login-page\">\n\t<div class=\"content\">\n\t\t<form [formGroup]=\"loginForm\" class=\"login-form\" fxFlex\n\t\t\t(ngSubmit)=\"login($event)\">\n\t\t\t<div class=\"text-center\">\n\t\t\t\t<h2 class=\"app-name\">Sakthi Groups</h2>\n\t\t\t</div>\n\t\t\t<div fxFlex fxlayout=\"row\" fxlayout.lt-md=\"column\">\n\t\t\t\t<div fxFlexFill>\n\t\t\t\t\t<mat-form-field class=\"w-100\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<div class=\"form-group__text\">\n\t\t\t\t\t\t\t<input id=\"username\" matInput placeholder=\"User Name\"\n\t\t\t\t\t\t\t\tformControlName=\"username\" name=\"username\" autocomplete=\"off\" >\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t</mat-form-field>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div fxFlex fxLayout=\"row\" fxLayout.lt-md=\"column\">\n\t\t\t\t<div fxFlexFill>\n\t\t\t\t\t<mat-form-field class=\"w-100\">\n\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<div class=\"form-group__text\">\n\t\t\t\t\t\t\t<input matInput type=\"password\" placeholder=\"Password\"\n\t\t\t\t\t\t\t\tformControlName=\"password\" name=\"password\">\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t</mat-form-field>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div *ngIf=\"appService.showLoading\" align=\"center\">\n\t\t\t\t<mat-progress-spinner class=\"example-margin\" align=\"center\"\n\t\t\t\t\tdiameter=30 color=\"primary\" mode=\"indeterminate\" value=\"100\">\n\t\t\t\t</mat-progress-spinner>\n\t\t\t</div>\n\t\t\t<div *ngIf=\"!isAuthenticate\" class=\"help-block text-danger\"\n\t\t\t\tstyle=\"text-align: center; font-size: 15px;\">\n\t\t\t\t<span class=\"icon-error\"></span> <span>Wrong Username or\n\t\t\t\t\tpassword. Try again</span>\n\t\t\t</div>\n\t\t\t<br> <br>\n\n\t\t\t<div fxFlex fxLayout=\"row\" fxLayout.lt-md=\"column\">\n\t\t\t\t<div fxFlexFill>\n\t\t\t\t\t<button mat-raised-button color=\"primary\" class=\"w-100\"\n\t\t\t\t\t\t(click)=\"login($event)\">Login</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t</form>\n\t</div>\n</div>\n"

/***/ }),

/***/ "./src/app/login/login.component.scss":
/*!********************************************!*\
  !*** ./src/app/login/login.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".login-page {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  height: 100%;\n  position: relative; }\n  .login-page .content {\n    z-index: 1;\n    display: flex;\n    align-items: center;\n    justify-content: center; }\n  .login-page .content .app-name {\n      margin-top: 0px;\n      padding-bottom: 10px;\n      font-size: 32px; }\n  .login-page .content .login-form {\n      padding: 40px;\n      background: #fff;\n      width: 500px;\n      box-shadow: 0 0 10px #ddd; }\n  .login-page .content .login-form input:-webkit-autofill {\n        -webkit-box-shadow: 0 0 0 30px white inset; }\n  .login-page:after {\n    content: '';\n    background: #fff;\n    position: absolute;\n    top: 0;\n    left: 0;\n    bottom: 50%;\n    right: 0; }\n  .login-page:before {\n    content: '';\n    background: #3f51b5;\n    position: absolute;\n    top: 50%;\n    left: 0;\n    bottom: 0;\n    right: 0; }\n  .text-center {\n  text-align: center; }\n  .w-100 {\n  width: 100%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4vRDpcXFdvcmtzcGFjZVxcU2FrdGhpUG9ydGFsL3NyY1xcYXBwXFxsb2dpblxcbG9naW4uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxjQUFhO0VBQ2Isb0JBQW1CO0VBQ25CLHdCQUF1QjtFQUN2QixhQUFZO0VBQ1osbUJBQWtCLEVBd0NyQjtFQTdDRDtJQU9RLFdBQVU7SUFDVixjQUFhO0lBQ2Isb0JBQW1CO0lBQ25CLHdCQUF1QixFQWUxQjtFQXpCTDtNQVlZLGdCQUFlO01BQ2YscUJBQW9CO01BQ3BCLGdCQUFlLEVBQ2xCO0VBZlQ7TUFpQlksY0FBYTtNQUNiLGlCQUFnQjtNQUNoQixhQUFZO01BQ1osMEJBQXlCLEVBSTVCO0VBeEJUO1FBc0JnQiwyQ0FBMEMsRUFDN0M7RUF2QmI7SUE0QlEsWUFBVztJQUNYLGlCQUFnQjtJQUNoQixtQkFBa0I7SUFDbEIsT0FBTTtJQUNOLFFBQU87SUFDUCxZQUFXO0lBQ1gsU0FBUSxFQUNYO0VBbkNMO0lBcUNRLFlBQVc7SUFDWCxvQkFBbUI7SUFDbkIsbUJBQWtCO0lBQ2xCLFNBQVE7SUFDUixRQUFPO0lBQ1AsVUFBUztJQUNULFNBQVEsRUFDWDtFQUVMO0VBQ0ksbUJBQWtCLEVBQ3JCO0VBQ0Q7RUFDSSxZQUFXLEVBQ2QiLCJmaWxlIjoic3JjL2FwcC9sb2dpbi9sb2dpbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5sb2dpbi1wYWdlIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAuY29udGVudCB7XG4gICAgICAgIHotaW5kZXg6IDE7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICAuYXBwLW5hbWUge1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogMHB4O1xuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDEwcHg7XG4gICAgICAgICAgICBmb250LXNpemU6IDMycHg7XG4gICAgICAgIH1cbiAgICAgICAgLmxvZ2luLWZvcm0ge1xuICAgICAgICAgICAgcGFkZGluZzogNDBweDtcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgICAgICAgICB3aWR0aDogNTAwcHg7XG4gICAgICAgICAgICBib3gtc2hhZG93OiAwIDAgMTBweCAjZGRkO1xuICAgICAgICAgICAgaW5wdXQ6LXdlYmtpdC1hdXRvZmlsbCB7XG4gICAgICAgICAgICAgICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDAgMCAzMHB4IHdoaXRlIGluc2V0O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgJjphZnRlciB7XG4gICAgICAgIGNvbnRlbnQ6ICcnO1xuICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHRvcDogMDtcbiAgICAgICAgbGVmdDogMDtcbiAgICAgICAgYm90dG9tOiA1MCU7XG4gICAgICAgIHJpZ2h0OiAwO1xuICAgIH1cbiAgICAmOmJlZm9yZSB7XG4gICAgICAgIGNvbnRlbnQ6ICcnO1xuICAgICAgICBiYWNrZ3JvdW5kOiAjM2Y1MWI1O1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHRvcDogNTAlO1xuICAgICAgICBsZWZ0OiAwO1xuICAgICAgICBib3R0b206IDA7XG4gICAgICAgIHJpZ2h0OiAwO1xuICAgIH1cbn1cbi50ZXh0LWNlbnRlciB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLnctMTAwIHtcbiAgICB3aWR0aDogMTAwJTtcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services */ "./src/app/services/index.ts");
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../app.service */ "./src/app/app.service.ts");






var LoginComponent = /** @class */ (function () {
    function LoginComponent(route, router, authenticationService, appService, formBuilder) {
        this.route = route;
        this.router = router;
        this.authenticationService = authenticationService;
        this.appService = appService;
        this.formBuilder = formBuilder;
        this.model = {};
        console.log("This is Login component");
        this.isAuthenticate = true;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        this.loginForm = this.formBuilder.group({
            username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
        });
    };
    LoginComponent.prototype.ngAfterViewInit = function () {
        document.getElementById("username").focus();
        this.appService.hideLoader();
    };
    LoginComponent.prototype.login = function (w) {
        var _this = this;
        this.appService.showLoader();
        var username = this.loginForm.value.username;
        var password = this.loginForm.value.password;
        if (username !== "" && password !== "") {
            this.authenticationService.login(username, password).subscribe(function (data) {
                _this.authenticationService.setSession(data);
                _this.isAuthenticate = true;
                _this.router.navigate([_this.returnUrl]);
            }, function (error) {
                _this.isAuthenticate = false;
                _this.appService.hideLoader();
                console.error(error);
            }, function () {
                _this.appService.hideLoader();
            });
        }
    };
    LoginComponent.prototype.removeMessage = function () {
        this.isAuthenticate = true;
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/login/login.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"], _app_service__WEBPACK_IMPORTED_MODULE_5__["AppService"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/purchase-acknowledgement/purchase-acknowledgement.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/purchase-acknowledgement/purchase-acknowledgement.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  purchase-acknowledgement works!\n</p>\n"

/***/ }),

/***/ "./src/app/purchase-acknowledgement/purchase-acknowledgement.component.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/purchase-acknowledgement/purchase-acknowledgement.component.scss ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3B1cmNoYXNlLWFja25vd2xlZGdlbWVudC9wdXJjaGFzZS1hY2tub3dsZWRnZW1lbnQuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/purchase-acknowledgement/purchase-acknowledgement.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/purchase-acknowledgement/purchase-acknowledgement.component.ts ***!
  \********************************************************************************/
/*! exports provided: PurchaseAcknowledgementComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PurchaseAcknowledgementComponent", function() { return PurchaseAcknowledgementComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PurchaseAcknowledgementComponent = /** @class */ (function () {
    function PurchaseAcknowledgementComponent() {
    }
    PurchaseAcknowledgementComponent.prototype.ngOnInit = function () {
    };
    PurchaseAcknowledgementComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-purchase-acknowledgement',
            template: __webpack_require__(/*! ./purchase-acknowledgement.component.html */ "./src/app/purchase-acknowledgement/purchase-acknowledgement.component.html"),
            styles: [__webpack_require__(/*! ./purchase-acknowledgement.component.scss */ "./src/app/purchase-acknowledgement/purchase-acknowledgement.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PurchaseAcknowledgementComponent);
    return PurchaseAcknowledgementComponent;
}());



/***/ }),

/***/ "./src/app/purchase-entry/purchase-entry.component.html":
/*!**************************************************************!*\
  !*** ./src/app/purchase-entry/purchase-entry.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  purchase-entry works!\n</p>\n"

/***/ }),

/***/ "./src/app/purchase-entry/purchase-entry.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/purchase-entry/purchase-entry.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3B1cmNoYXNlLWVudHJ5L3B1cmNoYXNlLWVudHJ5LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/purchase-entry/purchase-entry.component.ts":
/*!************************************************************!*\
  !*** ./src/app/purchase-entry/purchase-entry.component.ts ***!
  \************************************************************/
/*! exports provided: PurchaseEntryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PurchaseEntryComponent", function() { return PurchaseEntryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PurchaseEntryComponent = /** @class */ (function () {
    function PurchaseEntryComponent() {
    }
    PurchaseEntryComponent.prototype.ngOnInit = function () {
    };
    PurchaseEntryComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-purchase-entry',
            template: __webpack_require__(/*! ./purchase-entry.component.html */ "./src/app/purchase-entry/purchase-entry.component.html"),
            styles: [__webpack_require__(/*! ./purchase-entry.component.scss */ "./src/app/purchase-entry/purchase-entry.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PurchaseEntryComponent);
    return PurchaseEntryComponent;
}());



/***/ }),

/***/ "./src/app/purchase-history/purchase-history.component.html":
/*!******************************************************************!*\
  !*** ./src/app/purchase-history/purchase-history.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"padding-top: 10px; height: 96%;\">\n\t<div fxFlex>\n\t\t<mat-tab-group> <mat-tab\n\t\t\tlabel=\"Add/Update Purchase History\">\n\t\t<form [formGroup]=\"addPurchaseForm\" class=\"tp-form\">\n\t\t\t<br />\n\t\t\t<div formArrayName=\"addPurchaseHistory\">\n\t\t\t<!-- loop throught units -->\n\t\t\t\t<div *ngFor=\"let unit of addPurchaseForm['controls'].addPurchaseHistory['controls']; let i = index \">\n\t\t\t\t<!-- row divider show for every nex row exclude if first row -->\n\t\t\t\t<div *ngIf=\"addPurchaseForm['controls'].addPurchaseHistory['controls'].length > 1 && i > 0\" ><hr></div>\n\t\t\t\t\t<div [formGroupName]=\"i\">\n\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t<mat-form-field class=\"tp-full-width\">\n\t\t\t\t\t\t\t<input matInput name=\"productName\" formControlName=\"productName\" type=\"text\" placeholder=\"Product Name\" aria-label=\"Number\" [matAutocomplete]=\"prodName\">\n\t\t\t\t\t\t\t<mat-autocomplete #prodName=\"matAutocomplete\" [displayWith]=\"displayProdName\" (optionSelected)='getProductCategory($event.option.value, i)'>\n\t\t\t\t\t\t\t\t<mat-option *ngFor=\"let name of filteredProdName | async\" [value]=\"name\"> {{name}} </mat-option>\n\t\t\t\t\t\t\t</mat-autocomplete>\n\t\t\t\t\t\t</mat-form-field>\n\t\t\t\t\t\t<mat-form-field class=\"tp-full-category\">\n\t\t\t\t\t\t\t<input matInput name=\"productCategory\" type=\"text\" formControlName=\"productCategory\" placeholder=\"Product Category\" [matAutocomplete]=\"prodType\">\n\t\t\t\t\t\t\t<mat-autocomplete #prodType=\"matAutocomplete\" [displayWith]=\"displayProdType\">\n\t\t\t\t\t\t\t\t<mat-option *ngFor=\"let productType of filteredProdType | async\" [value]=\"productType\"> {{productType}} </mat-option>\n\t\t\t\t\t\t\t</mat-autocomplete>\n\t\t\t\t\t\t</mat-form-field>\n\t\t\t\t\t\t<mat-form-field class=\"tp-full-date\">\n\t\t\t\t\t\t\t<input matInput [matDatepicker]=\"picker\" formControlName=\"purchaseDate\" autocomplete=\"off\" placeholder=\"Choose a date\">\n\t\t\t\t\t\t\t<mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n\t\t\t\t\t\t\t<mat-datepicker [startAt]=\"currentDate\" #picker></mat-datepicker>\n\t\t\t\t\t\t</mat-form-field>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<mat-form-field class=\"tp-full-small\">\n\t\t\t\t\t\t\t<input matInput placeholder=\"Price\" formControlName=\"piecePrice\" autocomplete=\"off\" value=\"\">\n\t\t\t\t\t\t\t<span matSuffix>Piece</span>\n\t\t\t\t\t\t</mat-form-field>\n\t\t\t\t\t\t<mat-form-field class=\"tp-full-small\">\n\t\t\t\t\t\t\t<input matInput placeholder=\"Price\" formControlName=\"kgPrice\" autocomplete=\"off\" value=\"\">\n\t\t\t\t\t\t\t<span matSuffix>KG</span>\n\t\t\t\t\t\t</mat-form-field>\n\t\t\t\t\t\t<mat-form-field class=\"tp-full-small\">\n\t\t\t\t\t\t\t<input matInput placeholder=\"Price\" formControlName=\"dozenPrice\" autocomplete=\"off\" value=\"\">\n\t\t\t\t\t\t\t<span matSuffix>Dozen</span>\n\t\t\t\t\t\t</mat-form-field>\n\t\t\t\t\t\t<mat-form-field class=\"tp-full-small\">\n\t\t\t\t\t\t\t<input matInput placeholder=\"Price\" formControlName=\"litrePrice\" autocomplete=\"off\" value=\"\">\n\t\t\t\t\t\t\t<span matSuffix>Litre</span>\n\t\t\t\t\t\t</mat-form-field>\n\t\t\t\t\t\t<mat-form-field class=\"tp-full-small\">\n\t\t\t\t\t\t\t<input matInput placeholder=\"Price\" formControlName=\"boxPrice\" autocomplete=\"off\" value=\"\">\n\t\t\t\t\t\t\t<span matSuffix>Box</span>\n\t\t\t\t\t\t</mat-form-field>\n\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<!--<button mat-icon-button color=\"accent\" aria-label=\"Example icon-button with a heart icon\" *ngIf=\"addPurchaseForm['controls'].addPurchaseHistory['controls'].length > 1\" (click)=\"removePurchaseEntry(i)\">\n\t\t\t\t\t\t<mat-icon>block</mat-icon>\n\t\t\t\t    </button>-->\n\t\t\t\t\t<button mat-stroked-button color=\"accent\" *ngIf=\"addPurchaseForm['controls'].addPurchaseHistory['controls'].length > 1\" (click)=\"removePurchaseEntry(i)\">Remove Entry</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<br/>\n\t\t\t<!--<button mat-icon-button color=\"primary\" (click)=\"addPurchaseEntry()\">\n\t\t\t\t<mat-icon>add_circle</mat-icon>\n\t\t\t</button>-->\n\t\t\t<button mat-stroked-button color=\"primary\" (click)=\"addPurchaseEntry()\">New Entry</button>\n\t\t\t<div *ngIf=\"appService.showLoading\" align=\"center\">\n\t\t\t\t<mat-progress-spinner class=\"example-margin\" align=\"center\"\n\t\t\t\t\tdiameter=50 color=\"primary\" mode=\"indeterminate\" value=\"100\">\n\t\t\t\t</mat-progress-spinner>\n\t\t\t\t<br/>\n\t\t\t</div>\n\t\t\t\n\t\t\t<div align=\"center\">\n\t\t\t\t<button mat-raised-button color=\"primary\" [disabled]=\"!addPurchaseForm.valid\" (click)=\"savePurchase(addPurchaseForm.value)\">Submit</button>\n\t\t\t</div>\n\t\t\t<br/>\n\t\t</form>\n\t\t\n\t\t</mat-tab> <mat-tab label=\"View Purchase History\"> <mat-form-field>\n\t\t<input matInput (keyup)=\"applyFilter($event.target.value)\"\n\t\t\tplaceholder=\"Search Product\"> </mat-form-field>\n\n\t\t<div class=\"mat-elevation-z8\">\n\t\t\t<table mat-table [dataSource]=\"dataSource\" matSort>\n\n\t\t\t\t<!-- Product Name Column -->\n\t\t\t\t<ng-container matColumnDef=\"productName\">\n\t\t\t\t<th mat-header-cell *matHeaderCellDef mat-sort-header>Product\n\t\t\t\t\tName</th>\n\t\t\t\t<td mat-cell *matCellDef=\"let row\">{{row.productName}}</td>\n\t\t\t\t</ng-container>\n\n\t\t\t\t<!-- Product Category Column -->\n\t\t\t\t<ng-container matColumnDef=\"productCategory\">\n\t\t\t\t<th mat-header-cell *matHeaderCellDef mat-sort-header>Product\n\t\t\t\t\tCategory</th>\n\t\t\t\t<td mat-cell *matCellDef=\"let row\">{{row.productCategory}}</td>\n\t\t\t\t</ng-container>\n\n\t\t\t\t<!-- Purchase Price History Column -->\n\t\t\t\t<ng-container matColumnDef=\"purPriceHistory\">\n\t\t\t\t<th mat-header-cell *matHeaderCellDef mat-sort-header>Purchase\n\t\t\t\t\tHistory</th>\n\t\t\t\t<td mat-cell *matCellDef=\"let row\"><mat-expansion-panel>\n\t\t\t\t\t<mat-expansion-panel-header> <mat-panel-title>\n\t\t\t\t\tPurchase Price </mat-panel-title> <mat-panel-title> Quantity </mat-panel-title> </mat-expansion-panel-header>\n\t\t\t\t\t<div *ngFor=\"let item of row.purPriceHistory\">\n\t\t\t\t\t\tPurchase Date: {{item.purchaseDate | date: 'dd/MM/yyyy'}}\n\t\t\t\t\t\t<div *ngFor=\"let pricelist of item.purPriceList\">\n\t\t\t\t\t\t\t<mat-chip-list> <mat-chip color=\"primary\"\n\t\t\t\t\t\t\t\tselected>{{pricelist.netRate}}</mat-chip> <mat-chip>{{pricelist.quantity}}</mat-chip>\n\t\t\t\t\t\t\t</mat-chip-list>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t</mat-expansion-panel></td>\n\t\t\t\t</ng-container>\n\n\t\t\t\t<tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n\t\t\t\t<tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n\t\t\t</table>\n\n\t\t\t<mat-paginator [pageSize]=\"10\" [pageSizeOptions]=\"[5, 10, 25, 100]\"></mat-paginator>\n\t\t</div>\n\t\t</mat-tab> </mat-tab-group>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/purchase-history/purchase-history.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/purchase-history/purchase-history.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n  width: 100%; }\n\n.mat-form-field {\n  font-size: 14px;\n  width: 50%;\n  align: center; }\n\ntd,\nth {\n  width: 25%; }\n\n.tp-full-width {\n  width: 23%;\n  padding: 5px; }\n\n.tp-full-category {\n  width: 15%;\n  padding: 5px; }\n\n.tp-full-small {\n  width: 8%;\n  padding: 5px; }\n\n.tp-full-date {\n  width: 12%;\n  padding: 5px; }\n\n.tp-full-select {\n  width: 8%;\n  padding: 5px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHVyY2hhc2UtaGlzdG9yeS9EOlxcV29ya3NwYWNlXFxTYWt0aGlQb3J0YWwvc3JjXFxhcHBcXHB1cmNoYXNlLWhpc3RvcnlcXHB1cmNoYXNlLWhpc3RvcnkuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFXLEVBQ2Q7O0FBRUQ7RUFDSSxnQkFBZTtFQUNmLFdBQVU7RUFDVixjQUFhLEVBQ2hCOztBQUVEOztFQUVJLFdBQVUsRUFDYjs7QUFFRDtFQUNDLFdBQVU7RUFDVixhQUFZLEVBQ1o7O0FBRUQ7RUFDQyxXQUFVO0VBQ1YsYUFBWSxFQUNaOztBQUVEO0VBQ0MsVUFBUztFQUNULGFBQVksRUFDWjs7QUFFRDtFQUNDLFdBQVU7RUFDVixhQUFZLEVBQ1o7O0FBRUQ7RUFDQyxVQUFTO0VBQ1QsYUFBWSxFQUNaIiwiZmlsZSI6InNyYy9hcHAvcHVyY2hhc2UtaGlzdG9yeS9wdXJjaGFzZS1oaXN0b3J5LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5tYXQtZm9ybS1maWVsZCB7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICB3aWR0aDogNTAlO1xyXG4gICAgYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxudGQsXHJcbnRoIHtcclxuICAgIHdpZHRoOiAyNSU7XHJcbn1cclxuXHJcbi50cC1mdWxsLXdpZHRoIHtcclxuXHR3aWR0aDogMjMlO1xyXG5cdHBhZGRpbmc6IDVweDtcclxufVxyXG5cclxuLnRwLWZ1bGwtY2F0ZWdvcnkge1xyXG5cdHdpZHRoOiAxNSU7XHJcblx0cGFkZGluZzogNXB4O1xyXG59XHJcblxyXG4udHAtZnVsbC1zbWFsbCB7XHJcblx0d2lkdGg6IDglO1xyXG5cdHBhZGRpbmc6IDVweDtcclxufVxyXG5cclxuLnRwLWZ1bGwtZGF0ZSB7XHJcblx0d2lkdGg6IDEyJTtcclxuXHRwYWRkaW5nOiA1cHg7XHJcbn1cclxuXHJcbi50cC1mdWxsLXNlbGVjdCB7XHJcblx0d2lkdGg6IDglO1xyXG5cdHBhZGRpbmc6IDVweDtcclxufVxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/purchase-history/purchase-history.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/purchase-history/purchase-history.component.ts ***!
  \****************************************************************/
/*! exports provided: PurchaseHistoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PurchaseHistoryComponent", function() { return PurchaseHistoryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var src_app_app_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/app.service */ "./src/app/app.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");







var PurchaseHistoryComponent = /** @class */ (function () {
    function PurchaseHistoryComponent(appService, formBuilder, snackBar, changeDetectorRefs) {
        this.appService = appService;
        this.formBuilder = formBuilder;
        this.snackBar = snackBar;
        this.changeDetectorRefs = changeDetectorRefs;
        this.productNames = [];
        this.productCategory = [];
        this.displayedColumns = ['productName', 'productCategory', 'purPriceHistory'];
        this.priceList = [];
        this.purPriceHistory = [];
        this.purchaseHistory = [];
        this.prodNameCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]();
        this.prodTypeCtrl = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]();
        this.formData = { productName: "", productCategory: "", purchaseDate: "", priceList: [] };
        console.log("This is Purchase History Component");
        this.currentDate = new Date((new Date().getTime()));
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        this.loadAddPurHistTab();
        this.addPurchaseForm = this.formBuilder.group({
            addPurchaseHistory: this.formBuilder.array([
                this.getArrayObject(null)
            ])
        });
        //this.autoCompleteChanges( 0 );
    }
    PurchaseHistoryComponent.prototype.ngOnInit = function () {
        console.log("This is ngOnInit");
        localStorage.setItem('routePage', 'purchasehist');
        this.purchaseHistList = [];
        this.getStartupData();
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.autoCompleteChanges(0);
    };
    PurchaseHistoryComponent.prototype.autoCompleteChanges = function (index) {
        var _this = this;
        var control = this.addPurchaseForm.controls['addPurchaseHistory'];
        this.filteredProdName = control.at(index).get('productName').valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["startWith"])(''), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (value) { return _this.filterNameAutoComplete(value); }));
        this.filteredProdType = control.at(index).get('productCategory').valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["startWith"])(''), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (value) { return _this.filterTypeAutoComplete(value); }));
    };
    PurchaseHistoryComponent.prototype.filterNameAutoComplete = function (searchValue) {
        return this.productNames.filter(function (value) {
            return value.toLowerCase().includes(searchValue ? searchValue.toLowerCase() : searchValue);
        });
    };
    PurchaseHistoryComponent.prototype.filterTypeAutoComplete = function (searchValue) {
        return this.productCategory.filter(function (value) {
            return value.toLowerCase().includes(searchValue ? searchValue.toLowerCase() : searchValue);
        });
    };
    PurchaseHistoryComponent.prototype.getArrayObject = function (defaultDate) {
        return this.formBuilder.group({
            productName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            productCategory: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            purchaseDate: [defaultDate ? defaultDate : this.currentDate, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            piecePrice: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"]],
            kgPrice: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"]],
            dozenPrice: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"]],
            litrePrice: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"]],
            boxPrice: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"]]
        });
    };
    PurchaseHistoryComponent.prototype.getProductCategory = function (productName, index) {
        var _this = this;
        var control = this.addPurchaseForm.controls['addPurchaseHistory'];
        this.appService.getProductCategory(productName).subscribe(function (data) {
            control.at(index).get('productCategory').setValue(data[0]);
        }, function (error) {
            console.error(error);
            _this.appService.hideLoader();
        }, function () {
            _this.appService.hideLoader();
        });
    };
    PurchaseHistoryComponent.prototype.formReset = function () {
        var control = this.addPurchaseForm.controls['addPurchaseHistory'];
        while (control.length !== 1) {
            control.removeAt(0);
        }
        this.addPurchaseForm.reset({
            productName: '',
            productCategory: '',
            purchaseDate: '',
            piecePrice: '',
            kgPrice: '',
            dozenPrice: '',
            litrePrice: '',
            boxPrice: ''
        });
        control.at(0).get('purchaseDate').setValue(this.currentDate);
        this.refreshPurchaseDataStore();
    };
    PurchaseHistoryComponent.prototype.savePurchase = function (formValue) {
        var _this = this;
        this.appService.showLoader();
        this.purchaseHistory = [];
        formValue.addPurchaseHistory.forEach(function (element) {
            _this.priceList = [];
            _this.purPriceHistory = [];
            if (element.piecePrice != "" && element.piecePrice != null) {
                console.log("element.piecePrice: " + element.piecePrice);
                var piecePrice = {
                    netRate: element.piecePrice,
                    quantity: 'Piece'
                };
                _this.priceList.push(piecePrice);
                console.log("priceList: " + JSON.stringify(_this.priceList));
            }
            if (element.kgPrice != "" && element.kgPrice != null) {
                console.log("element.kgPrice: " + element.kgPrice);
                var kgPrice = {
                    netRate: element.kgPrice,
                    quantity: 'KG'
                };
                _this.priceList.push(kgPrice);
                console.log("priceList: " + JSON.stringify(_this.priceList));
            }
            if (element.dozenPrice != "" && element.dozenPrice != null) {
                console.log("element.dozenPrice: " + element.dozenPrice);
                var dozenPrice = {
                    netRate: element.dozenPrice,
                    quantity: 'Dozen'
                };
                _this.priceList.push(dozenPrice);
                console.log("priceList: " + JSON.stringify(_this.priceList));
            }
            if (element.litrePrice != "" && element.litrePrice != null) {
                console.log("element.litrePrice: " + element.litrePrice);
                var litrePrice = {
                    netRate: element.litrePrice,
                    quantity: 'Litre'
                };
                _this.priceList.push(litrePrice);
                console.log("priceList: " + JSON.stringify(_this.priceList));
            }
            if (element.boxPrice != "" && element.boxPrice != null) {
                console.log("element.boxPrice: " + element.boxPrice);
                var boxPrice = {
                    netRate: element.boxPrice,
                    quantity: 'Box'
                };
                _this.priceList.push(boxPrice);
                console.log("priceList: " + JSON.stringify(_this.priceList));
            }
            var purchasePriceList = {
                purchaseDate: element.purchaseDate,
                purPriceList: _this.priceList
            };
            _this.purPriceHistory.push(purchasePriceList);
            var purHistory = {
                productName: element.productName,
                productCategory: element.productCategory,
                purPriceHistory: _this.purPriceHistory
            };
            _this.purchaseHistory.push(purHistory);
        });
        console.log("this.formData: " + JSON.stringify(this.purchaseHistory));
        this.appService.savePurchaseRecord(this.purchaseHistory).subscribe(function (data) {
            console.log("Post response data: " + JSON.stringify(data));
            if (data) {
                _this.formReset();
                _this.loadAddPurHistTab();
                _this.showSnackBar("Successfully updated the data");
            }
        }, function (error) {
            console.error(error);
            _this.showSnackBar("Failed to updated the data");
        }, function () {
            _this.appService.hideLoader();
        });
    };
    PurchaseHistoryComponent.prototype.showSnackBar = function (message) {
        this.snackBar.open(message, 'Close', {
            duration: 3000,
        });
    };
    PurchaseHistoryComponent.prototype.applyFilter = function (filterValue) {
        filterValue = filterValue.trim();
        filterValue = filterValue.toLowerCase();
        this.dataSource.filter = filterValue;
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    };
    PurchaseHistoryComponent.prototype.addPurchaseEntry = function () {
        var control = this.addPurchaseForm.controls['addPurchaseHistory'];
        control.push(this.getArrayObject(control.at(control.length - 1).get('purchaseDate').value));
        this.autoCompleteChanges(control.length - 1);
        this.addPurchaseForm.controls.userTechnologies.updateValueAndValidity();
    };
    PurchaseHistoryComponent.prototype.removePurchaseEntry = function (i) {
        var control = this.addPurchaseForm.controls['addPurchaseHistory'];
        control.removeAt(i);
        this.autoCompleteChanges(i - 1);
    };
    PurchaseHistoryComponent.prototype.displayProdName = function (value) {
        return value ? value : undefined;
    };
    PurchaseHistoryComponent.prototype.displayProdType = function (value) {
        return value ? value : undefined;
    };
    PurchaseHistoryComponent.prototype.loadAddPurHistTab = function () {
        var _this = this;
        this.appService.getProductNames().subscribe(function (data) {
            _this.productNames = data;
        }, function (error) {
            console.error(error);
            _this.appService.hideLoader();
        }, function () {
            _this.appService.hideLoader();
        });
        this.appService.getProductCategory(null).subscribe(function (data) {
            _this.productCategory = data;
        }, function (error) {
            console.error(error);
            _this.appService.hideLoader();
        }, function () {
            _this.appService.hideLoader();
        });
    };
    PurchaseHistoryComponent.prototype.getStartupData = function () {
        this.refreshPurchaseDataStore();
    };
    PurchaseHistoryComponent.prototype.refreshPurchaseDataStore = function () {
        var _this = this;
        this.appService.getPurchaseHistoryDetails().subscribe(function (data) {
            _this.dataSource.data = data;
            _this.changeDetectorRefs.detectChanges();
        }, function (error) {
            console.error(error);
            _this.appService.hideLoader();
        }, function () {
            _this.appService.hideLoader();
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginator"])
    ], PurchaseHistoryComponent.prototype, "paginator", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSort"])
    ], PurchaseHistoryComponent.prototype, "sort", void 0);
    PurchaseHistoryComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-purchase-history',
            template: __webpack_require__(/*! ./purchase-history.component.html */ "./src/app/purchase-history/purchase-history.component.html"),
            styles: [__webpack_require__(/*! ./purchase-history.component.scss */ "./src/app/purchase-history/purchase-history.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_app_service__WEBPACK_IMPORTED_MODULE_3__["AppService"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]])
    ], PurchaseHistoryComponent);
    return PurchaseHistoryComponent;
}());



/***/ }),

/***/ "./src/app/services/auth-interceptor.service.ts":
/*!******************************************************!*\
  !*** ./src/app/services/auth-interceptor.service.ts ***!
  \******************************************************/
/*! exports provided: HttpsRequestInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpsRequestInterceptor", function() { return HttpsRequestInterceptor; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HttpsRequestInterceptor = /** @class */ (function () {
    function HttpsRequestInterceptor() {
    }
    HttpsRequestInterceptor.prototype.intercept = function (request, next) {
        var idToken = localStorage.getItem("id_token");
        if (idToken) {
            var cloned = request.clone({
                headers: request.headers.set("Authorization", "Bearer " + idToken)
            });
            return next.handle(cloned);
        }
        else {
            return next.handle(request);
        }
    };
    HttpsRequestInterceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], HttpsRequestInterceptor);
    return HttpsRequestInterceptor;
}());



/***/ }),

/***/ "./src/app/services/authentication.service.ts":
/*!****************************************************!*\
  !*** ./src/app/services/authentication.service.ts ***!
  \****************************************************/
/*! exports provided: TOKEN_NAME, AuthenticationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TOKEN_NAME", function() { return TOKEN_NAME; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationService", function() { return AuthenticationService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _util_AppConstants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../util/AppConstants */ "./src/app/util/AppConstants.ts");






//import 'rxjs/add/operator/map';

var TOKEN_NAME = 'id_token';
var AuthenticationService = /** @class */ (function () {
    function AuthenticationService(http, router) {
        this.http = http;
        this.router = router;
        this.baseRestUrl = _util_AppConstants__WEBPACK_IMPORTED_MODULE_5__["BaseRestURL"];
    }
    AuthenticationService.prototype.ngOnInit = function () {
    };
    AuthenticationService.prototype.login = function (username, password) {
        var response;
        this.userId = username;
        var body = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]().set('username', username).set('password', password);
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers.set('Content-Type', 'application/x-www-form-urlencoded');
        var url = this.baseRestUrl + "auth/login";
        console.log("Request made from here");
        return this.http.post(url, body, { responseType: 'text' });
    };
    AuthenticationService.prototype.refreshToken = function () {
        var url = this.baseRestUrl + "auth/refreshToken";
        return this.http.get(url);
    };
    AuthenticationService.prototype.setSession = function (authResult) {
        var expiresAt = moment__WEBPACK_IMPORTED_MODULE_4__().add("14400", 'second');
        localStorage.setItem("id_token", authResult);
        localStorage.setItem("expires_at", JSON.stringify(expiresAt.valueOf()));
        localStorage.setItem("logged_user", this.userId);
    };
    AuthenticationService.prototype.logout = function () {
        localStorage.removeItem("id_token");
        localStorage.removeItem("expires_at");
        localStorage.removeItem("logged_user");
        this.router.navigate(['']);
    };
    AuthenticationService.prototype.isLoggedIn = function () {
        return moment__WEBPACK_IMPORTED_MODULE_4__().isBefore(this.getExpiration());
    };
    AuthenticationService.prototype.isLoggedOut = function () {
        return !this.isLoggedIn();
    };
    AuthenticationService.prototype.getExpiration = function () {
        var expiration = localStorage.getItem("expires_at");
        var expiresAt = JSON.parse(expiration);
        return moment__WEBPACK_IMPORTED_MODULE_4__(expiresAt);
    };
    AuthenticationService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], AuthenticationService);
    return AuthenticationService;
}());



/***/ }),

/***/ "./src/app/services/authguard.service.ts":
/*!***********************************************!*\
  !*** ./src/app/services/authguard.service.ts ***!
  \***********************************************/
/*! exports provided: AuthguardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthguardService", function() { return AuthguardService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var AuthguardService = /** @class */ (function () {
    function AuthguardService(router) {
        this.router = router;
    }
    AuthguardService.prototype.canActivate = function () {
        if (localStorage.getItem('id_token')) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    };
    AuthguardService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AuthguardService);
    return AuthguardService;
}());



/***/ }),

/***/ "./src/app/services/index.ts":
/*!***********************************!*\
  !*** ./src/app/services/index.ts ***!
  \***********************************/
/*! exports provided: TOKEN_NAME, AuthenticationService, AuthguardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _authentication_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TOKEN_NAME", function() { return _authentication_service__WEBPACK_IMPORTED_MODULE_0__["TOKEN_NAME"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AuthenticationService", function() { return _authentication_service__WEBPACK_IMPORTED_MODULE_0__["AuthenticationService"]; });

/* harmony import */ var _authguard_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./authguard.service */ "./src/app/services/authguard.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AuthguardService", function() { return _authguard_service__WEBPACK_IMPORTED_MODULE_1__["AuthguardService"]; });





/***/ }),

/***/ "./src/app/supplier-master/supplier-master.component.html":
/*!****************************************************************!*\
  !*** ./src/app/supplier-master/supplier-master.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  supplier-master works!\n</p>\n"

/***/ }),

/***/ "./src/app/supplier-master/supplier-master.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/supplier-master/supplier-master.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3N1cHBsaWVyLW1hc3Rlci9zdXBwbGllci1tYXN0ZXIuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/supplier-master/supplier-master.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/supplier-master/supplier-master.component.ts ***!
  \**************************************************************/
/*! exports provided: SupplierMasterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupplierMasterComponent", function() { return SupplierMasterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SupplierMasterComponent = /** @class */ (function () {
    function SupplierMasterComponent() {
    }
    SupplierMasterComponent.prototype.ngOnInit = function () {
    };
    SupplierMasterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-supplier-master',
            template: __webpack_require__(/*! ./supplier-master.component.html */ "./src/app/supplier-master/supplier-master.component.html"),
            styles: [__webpack_require__(/*! ./supplier-master.component.scss */ "./src/app/supplier-master/supplier-master.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SupplierMasterComponent);
    return SupplierMasterComponent;
}());



/***/ }),

/***/ "./src/app/user-management/customer-management/customer-management.component.html":
/*!****************************************************************************************!*\
  !*** ./src/app/user-management/customer-management/customer-management.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br/>\n<div class=\"example-button-row\">\n\t<button mat-stroked-button color=\"primary\">Add Customer</button>\n\t<button mat-stroked-button color=\"primary\">Edit Customer</button>\n\t<button mat-stroked-button color=\"primary\">Delete Customer</button>\n</div>"

/***/ }),

/***/ "./src/app/user-management/customer-management/customer-management.component.scss":
/*!****************************************************************************************!*\
  !*** ./src/app/user-management/customer-management/customer-management.component.scss ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-button-row button,\n.example-button-row a {\n  margin-right: 8px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci1tYW5hZ2VtZW50L2N1c3RvbWVyLW1hbmFnZW1lbnQvRDpcXFdvcmtzcGFjZVxcU2FrdGhpUG9ydGFsL3NyY1xcYXBwXFx1c2VyLW1hbmFnZW1lbnRcXGN1c3RvbWVyLW1hbmFnZW1lbnRcXGN1c3RvbWVyLW1hbmFnZW1lbnQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0VBRUUsa0JBQWlCLEVBQ2xCIiwiZmlsZSI6InNyYy9hcHAvdXNlci1tYW5hZ2VtZW50L2N1c3RvbWVyLW1hbmFnZW1lbnQvY3VzdG9tZXItbWFuYWdlbWVudC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5leGFtcGxlLWJ1dHRvbi1yb3cgYnV0dG9uLFxyXG4uZXhhbXBsZS1idXR0b24tcm93IGEge1xyXG4gIG1hcmdpbi1yaWdodDogOHB4O1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/user-management/customer-management/customer-management.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/user-management/customer-management/customer-management.component.ts ***!
  \**************************************************************************************/
/*! exports provided: CustomerManagementComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomerManagementComponent", function() { return CustomerManagementComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var CustomerManagementComponent = /** @class */ (function () {
    function CustomerManagementComponent() {
    }
    CustomerManagementComponent.prototype.ngOnInit = function () {
    };
    CustomerManagementComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-customer-management',
            template: __webpack_require__(/*! ./customer-management.component.html */ "./src/app/user-management/customer-management/customer-management.component.html"),
            styles: [__webpack_require__(/*! ./customer-management.component.scss */ "./src/app/user-management/customer-management/customer-management.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CustomerManagementComponent);
    return CustomerManagementComponent;
}());



/***/ }),

/***/ "./src/app/user-management/login-user-management/login-user-management.component.html":
/*!********************************************************************************************!*\
  !*** ./src/app/user-management/login-user-management/login-user-management.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<br/>\n<div class=\"example-button-row\">\n\t<button mat-stroked-button color=\"primary\">Add User</button>\n\t<button mat-stroked-button color=\"primary\" [disabled]=\"!selection.hasValue() || selection.selected.length > 1\" (click)=\"editUser()\">Edit User</button>\n\t<button mat-stroked-button color=\"primary\" [disabled]=\"!selection.hasValue()\" (click)=\"deleteUser()\">Delete User</button>\n\t<br/>\n\t<br/>\n\t<div class=\"mat-elevation-z8 docs-example-viewer-body\">\n\t\t\t<table mat-table [dataSource]=\"dataSource\" matSort>\n\t\t\t\t\n\t\t\t\t<ng-container matColumnDef=\"select\">\n\t\t\t    <th mat-header-cell *matHeaderCellDef>\n\t\t\t      <mat-checkbox (change)=\"$event ? masterToggle() : null\"\n\t\t\t                    [checked]=\"selection.hasValue() && isAllSelected()\"\n\t\t\t                    [indeterminate]=\"selection.hasValue() && !isAllSelected()\"\n\t\t\t                    [aria-label]=\"checkboxLabel()\">\n\t\t\t      </mat-checkbox>\n\t\t\t    </th>\n\t\t\t    <td mat-cell *matCellDef=\"let row\">\n\t\t\t      <mat-checkbox (click)=\"$event.stopPropagation()\"\n\t\t\t                    (change)=\"$event ? selection.toggle(row) : null\"\n\t\t\t                    [checked]=\"selection.isSelected(row)\"\n\t\t\t                    [aria-label]=\"checkboxLabel(row)\">\n\t\t\t      </mat-checkbox>\n\t\t\t    </td>\n\t\t\t  </ng-container>\n\t\t\t\t<!-- Product Name Column -->\n\t\t\t\t<ng-container matColumnDef=\"userName\">\n\t\t\t\t<th mat-header-cell *matHeaderCellDef mat-sort-header>User\n\t\t\t\t\tName</th>\n\t\t\t\t<td mat-cell *matCellDef=\"let row\">{{row.userName}}</td>\n\t\t\t\t</ng-container>\n\n\t\t\t\t<!-- Product Category Column -->\n\t\t\t\t<ng-container matColumnDef=\"userRole\">\n\t\t\t\t<th mat-header-cell *matHeaderCellDef mat-sort-header>User Role</th>\n\t\t\t\t<td mat-cell *matCellDef=\"let row\">{{row.userRole}}</td>\n\t\t\t\t</ng-container>\n\n\t\t\t\t<tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\n\t\t\t\t<tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\n\t\t\t</table>\n\n\t\t\t<mat-paginator [pageSize]=\"10\" [pageSizeOptions]=\"[5, 10, 25, 100]\"></mat-paginator>\n\t\t</div>\n</div>"

/***/ }),

/***/ "./src/app/user-management/login-user-management/login-user-management.component.scss":
/*!********************************************************************************************!*\
  !*** ./src/app/user-management/login-user-management/login-user-management.component.scss ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-button-row button,\n.example-button-row a {\n  margin-right: 8px; }\n\ntable {\n  align: center;\n  width: 100%; }\n\n.mat-form-field {\n  font-size: 14px;\n  width: 50%;\n  align: center; }\n\ntd,\nth {\n  width: 15%; }\n\n.mat-column-select {\n  overflow: initial; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci1tYW5hZ2VtZW50L2xvZ2luLXVzZXItbWFuYWdlbWVudC9EOlxcV29ya3NwYWNlXFxTYWt0aGlQb3J0YWwvc3JjXFxhcHBcXHVzZXItbWFuYWdlbWVudFxcbG9naW4tdXNlci1tYW5hZ2VtZW50XFxsb2dpbi11c2VyLW1hbmFnZW1lbnQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0VBRUUsa0JBQWlCLEVBQ2xCOztBQUVEO0VBQ0MsY0FBYTtFQUNWLFlBQVcsRUFDZDs7QUFFRDtFQUNJLGdCQUFlO0VBQ2YsV0FBVTtFQUNWLGNBQWEsRUFDaEI7O0FBRUQ7O0VBRUksV0FBVSxFQUNiOztBQUVEO0VBQ0Usa0JBQWlCLEVBQ2xCIiwiZmlsZSI6InNyYy9hcHAvdXNlci1tYW5hZ2VtZW50L2xvZ2luLXVzZXItbWFuYWdlbWVudC9sb2dpbi11c2VyLW1hbmFnZW1lbnQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXhhbXBsZS1idXR0b24tcm93IGJ1dHRvbixcclxuLmV4YW1wbGUtYnV0dG9uLXJvdyBhIHtcclxuICBtYXJnaW4tcmlnaHQ6IDhweDtcclxufVxyXG5cclxudGFibGUge1xyXG5cdGFsaWduOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLm1hdC1mb3JtLWZpZWxkIHtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgICBhbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG50ZCxcclxudGgge1xyXG4gICAgd2lkdGg6IDE1JTtcclxufVxyXG5cclxuLm1hdC1jb2x1bW4tc2VsZWN0IHtcclxuICBvdmVyZmxvdzogaW5pdGlhbDtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/user-management/login-user-management/login-user-management.component.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/user-management/login-user-management/login-user-management.component.ts ***!
  \******************************************************************************************/
/*! exports provided: LoginUserManagementComponent, DeleteUserDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginUserManagementComponent", function() { return LoginUserManagementComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeleteUserDialog", function() { return DeleteUserDialog; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/app.service */ "./src/app/app.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/cdk/collections */ "./node_modules/@angular/cdk/esm5/collections.es5.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");








var defaultDialogConfig = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogConfig"]();
var USER_DATA = [{ id: '12345', userName: 'bala', password: '12345', userRole: 'admin' },
    { id: '12346', userName: 'bala2', password: '123452', userRole: 'admin' }];
var LoginUserManagementComponent = /** @class */ (function () {
    function LoginUserManagementComponent(appService, formBuilder, changeDetectorRefs, dialog, doc) {
        this.appService = appService;
        this.formBuilder = formBuilder;
        this.changeDetectorRefs = changeDetectorRefs;
        this.dialog = dialog;
        this.displayedColumns = ['select', 'userName', 'userRole'];
        this.selection = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__["SelectionModel"](true, []);
        this.config = {
            disableClose: false,
            panelClass: 'custom-overlay-pane-class',
            hasBackdrop: true,
            backdropClass: '',
            width: '',
            height: '',
            minWidth: '',
            minHeight: '',
            maxWidth: defaultDialogConfig.maxWidth,
            maxHeight: '',
            position: {
                top: '',
                bottom: '',
                left: '',
                right: ''
            },
            data: {
                message: 'Jazzy jazz jazz'
            }
        };
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"](USER_DATA);
        console.log("This is User Management Component");
    }
    ;
    LoginUserManagementComponent.prototype.ngOnInit = function () {
    };
    LoginUserManagementComponent.prototype.isAllSelected = function () {
        var numSelected = this.selection.selected.length;
        var numRows = this.dataSource.data.length;
        return numSelected === numRows;
    };
    LoginUserManagementComponent.prototype.editUser = function () {
        console.log("this.selection: " + JSON.stringify(this.selection.selected));
    };
    LoginUserManagementComponent.prototype.deleteUser = function () {
        console.log("this.selection: " + JSON.stringify(this.selection.selected));
        this.dialogRef = this.dialog.open(DeleteUserDialog, this.config);
    };
    LoginUserManagementComponent.prototype.masterToggle = function () {
        var _this = this;
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(function (row) { return _this.selection.select(row); });
    };
    LoginUserManagementComponent.prototype.checkboxLabel = function (row) {
        if (!row) {
            return (this.isAllSelected() ? 'select' : 'deselect') + " all";
        }
        return (this.selection.isSelected(row) ? 'deselect' : 'select') + " row " + (row.userRole + 1);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], LoginUserManagementComponent.prototype, "paginator", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"])
    ], LoginUserManagementComponent.prototype, "sort", void 0);
    LoginUserManagementComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login-user-management',
            template: __webpack_require__(/*! ./login-user-management.component.html */ "./src/app/user-management/login-user-management/login-user-management.component.html"),
            styles: [__webpack_require__(/*! ./login-user-management.component.scss */ "./src/app/user-management/login-user-management/login-user-management.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](4, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_common__WEBPACK_IMPORTED_MODULE_6__["DOCUMENT"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"], Object])
    ], LoginUserManagementComponent);
    return LoginUserManagementComponent;
}());

var DeleteUserDialog = /** @class */ (function () {
    function DeleteUserDialog(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        this._dimesionToggle = false;
    }
    DeleteUserDialog.prototype.togglePosition = function () {
        this._dimesionToggle = !this._dimesionToggle;
        if (this._dimesionToggle) {
            this.dialogRef
                .updateSize('500px', '500px')
                .updatePosition({ top: '25px', left: '25px' });
        }
        else {
            this.dialogRef
                .updateSize()
                .updatePosition();
        }
    };
    DeleteUserDialog = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'delete-user',
            template: "\n    <mat-dialog-content>\n<p>\n  Do you want to delete the user?\n</p>\n</mat-dialog-content>\n<mat-dialog-actions class=\"example-button-row\" align=\"center\">\n  <button mat-stroked-button color=\"primary\" (click)=\"onYesClick()\" tabindex=\"1\">Yes</button>\n  <button mat-stroked-button mat-dialog-close tabindex=\"-1\">No</button>\n</mat-dialog-actions>",
            styles: [__webpack_require__(/*! ./login-user-management.component.scss */ "./src/app/user-management/login-user-management/login-user-management.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"], Object])
    ], DeleteUserDialog);
    return DeleteUserDialog;
}());



/***/ }),

/***/ "./src/app/user-management/user-management.component.html":
/*!****************************************************************!*\
  !*** ./src/app/user-management/user-management.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"padding-top: 10px; height: 96%;\">\n\t<div fxFlex>\n\t\t<mat-tab-group>\n\t\t\t<mat-tab label=\"Customer Management\">\n\t\t\t\t<app-customer-management></app-customer-management>\n\t\t\t</mat-tab>\n\t\t\t<mat-tab label=\"User Management\">\n\t\t\t\t<app-login-user-management></app-login-user-management>\n\t\t\t</mat-tab>\n\t\t</mat-tab-group>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/user-management/user-management.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/user-management/user-management.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-button-row button,\n.example-button-row a {\n  margin-right: 8px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci1tYW5hZ2VtZW50L0Q6XFxXb3Jrc3BhY2VcXFNha3RoaVBvcnRhbC9zcmNcXGFwcFxcdXNlci1tYW5hZ2VtZW50XFx1c2VyLW1hbmFnZW1lbnQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0VBRUUsa0JBQWlCLEVBQ2xCIiwiZmlsZSI6InNyYy9hcHAvdXNlci1tYW5hZ2VtZW50L3VzZXItbWFuYWdlbWVudC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5leGFtcGxlLWJ1dHRvbi1yb3cgYnV0dG9uLFxyXG4uZXhhbXBsZS1idXR0b24tcm93IGEge1xyXG4gIG1hcmdpbi1yaWdodDogOHB4O1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/user-management/user-management.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/user-management/user-management.component.ts ***!
  \**************************************************************/
/*! exports provided: UserManagementComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserManagementComponent", function() { return UserManagementComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/app.service */ "./src/app/app.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");





var UserManagementComponent = /** @class */ (function () {
    function UserManagementComponent(appService, formBuilder, changeDetectorRefs) {
        this.appService = appService;
        this.formBuilder = formBuilder;
        this.changeDetectorRefs = changeDetectorRefs;
    }
    UserManagementComponent.prototype.ngOnInit = function () {
        console.log("This is UserManagementComponent ngOnInit");
        localStorage.setItem('routePage', 'usermanage');
    };
    UserManagementComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-user-management',
            template: __webpack_require__(/*! ./user-management.component.html */ "./src/app/user-management/user-management.component.html"),
            styles: [__webpack_require__(/*! ./user-management.component.scss */ "./src/app/user-management/user-management.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_app_service__WEBPACK_IMPORTED_MODULE_2__["AppService"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]])
    ], UserManagementComponent);
    return UserManagementComponent;
}());



/***/ }),

/***/ "./src/app/util/AppConstants.ts":
/*!**************************************!*\
  !*** ./src/app/util/AppConstants.ts ***!
  \**************************************/
/*! exports provided: Port, BaseRestIP, BaseRestURL */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Port", function() { return Port; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseRestIP", function() { return BaseRestIP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseRestURL", function() { return BaseRestURL; });
var Port = "10000";
var BaseRestIP = "localhost";
var BaseRestURL = "http://" + BaseRestIP + ":" + Port + "/SakthiPortal/";


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\Workspace\SakthiPortal\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map