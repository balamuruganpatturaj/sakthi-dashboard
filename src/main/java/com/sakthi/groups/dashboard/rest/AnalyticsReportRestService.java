package com.sakthi.groups.dashboard.rest;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sakthi.groups.dashboard.model.CustomerAnalyticsPayLoad;
import com.sakthi.groups.dashboard.model.CustomerAnalyticsReport;
import com.sakthi.groups.dashboard.service.AnalyticsReportService;
import com.sakthi.groups.migration.portal.model.BillAnalyticsData;
import com.sakthi.groups.migration.portal.model.BillSearchCriteria;
import com.sakthi.groups.migration.portal.model.BillTrendData;
import com.sakthi.groups.migration.portal.model.ProductSupplierWiseData;
import com.sakthi.groups.migration.portal.model.SalesPurchaseTrendData;
import com.sakthi.groups.migration.portal.model.TopSellingTrendData;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/rest/reports")
public class AnalyticsReportRestService {

	@Autowired
	private AnalyticsReportService analyticsReportService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@ApiOperation(value = "API to fetch sale trend data from GoFrugal", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved the information"),
			@ApiResponse(code = 500, message = "Exception occurred while fetching") })
	@PostMapping(path = "/getsalestrend", produces = "application/json")
	public ResponseEntity<?> getSalesTrend(@RequestBody BillSearchCriteria searchCriteria, HttpServletRequest request) {
		BillTrendData billTrendData = analyticsReportService.getSalesTrendData(searchCriteria);
		return new ResponseEntity<>(billTrendData, HttpStatus.OK);
	}

	@ApiOperation(value = "API to fetch top selling trend data from GoFrugal", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved the information"),
			@ApiResponse(code = 500, message = "Exception occurred while fetching") })
	@PostMapping(path = "/getproducttrend", produces = "application/json")
	public ResponseEntity<?> getProductTrend(@RequestBody BillSearchCriteria searchCriteria,
			HttpServletRequest request) {
		List<TopSellingTrendData> topSellingTrendData = analyticsReportService.getTopSellingProduct(searchCriteria);
		return new ResponseEntity<>(topSellingTrendData, HttpStatus.OK);
	}

	@ApiOperation(value = "API to fetch sale trend data from GoFrugal", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved the information"),
			@ApiResponse(code = 500, message = "Exception occurred while fetching") })
	@PostMapping(path = "/getbillanalyticsdata", produces = "application/json")
	public ResponseEntity<?> getBillAnalyticsData(@RequestBody BillSearchCriteria searchCriteria,
			HttpServletRequest request, @RequestParam("isSaveSearch") boolean isSaveSearch) {
		List<BillAnalyticsData> billAnalyticsData = analyticsReportService.getBillAnalyticsData(searchCriteria,
				isSaveSearch);
		return new ResponseEntity<>(billAnalyticsData, HttpStatus.OK);
	}

	@ApiOperation(value = "API to check whether Customer Analytics is already stored or not", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the purchase history details"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = { "/checkCustomerAnalytics" }, produces = "application/json")
	public ResponseEntity<?> checkCustomerAnalytics(@RequestBody BillSearchCriteria searchCriteria,
			HttpServletRequest request, @RequestParam("isSaveSearch") boolean isSaveSearch) {
		boolean returnStatus = analyticsReportService.checkCustomerAnalytics(searchCriteria, isSaveSearch);
		return new ResponseEntity<>(returnStatus, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the saved customer anlaytics data", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the purchase history details"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = { "/getSavedCustomerAnalytics" }, produces = "application/json")
	public ResponseEntity<?> getSavedCustomerAnalytics(HttpServletRequest request) {
		List<CustomerAnalyticsReport> customerAnalyticsReportList = analyticsReportService.getSavedCustomerAnalytics();
		return new ResponseEntity<>(customerAnalyticsReportList, HttpStatus.OK);
	}

	@ApiOperation(value = "API to update the Customer Analytics based on changes made", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the purchase history details"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = { "/updateCustomerAnalytics" }, produces = "application/json")
	public ResponseEntity<?> updateCustomerAnalytics(@RequestBody CustomerAnalyticsReport customerAnalyticsReport,
			HttpServletRequest request) {
		boolean returnStatus = analyticsReportService.updateCustomerAnalytics(customerAnalyticsReport);
		return new ResponseEntity<>(returnStatus, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the saved customer anlaytics data", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the purchase history details"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = { "/getCustomerAnalyticsList" }, produces = "application/json")
	public ResponseEntity<?> getCustomerAnalyticsList(@RequestBody BillSearchCriteria searchCriteria,
			HttpServletRequest request) {
		List<BillAnalyticsData> analyticsDataList = analyticsReportService.getCustomerAnalyticsList(searchCriteria);
		return new ResponseEntity<>(analyticsDataList, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the product names availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = "/getGoFrugalProductNames", produces = "application/json")
	public ResponseEntity<?> getFilteredProductNames(@RequestParam("productName") String productName,
			HttpServletRequest request) {
		try {
			productName = URLDecoder.decode(productName, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("Unsupported Encoding {}", e.getMessage());
		}
		logger.info("productName: " + productName);
		List<String> productNames = analyticsReportService.getFilteredProductNames(productName);
		return new ResponseEntity<>(productNames, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get customer product trend data", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = "/getProdcutPurchaseTrend", produces = "application/json")
	public ResponseEntity<?> getPurchaseTrendData(@RequestBody CustomerAnalyticsPayLoad payLoad,
			HttpServletRequest request) {
		List<SalesPurchaseTrendData> sellingTrendData = analyticsReportService.getPurchaseTrend(payLoad.getItemName(),
				payLoad.getSearchParam(), payLoad.getSearchStartDate(), payLoad.getSearchEndDate());
		return new ResponseEntity<>(sellingTrendData, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the gross trend data", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = "/getProductSalesTrend", produces = "application/json")
	public ResponseEntity<?> getSalesTrendData(@RequestBody CustomerAnalyticsPayLoad payLoad,
			HttpServletRequest request) {
		List<SalesPurchaseTrendData> grossTrendData = analyticsReportService.getSalesTrend(payLoad.getItemName(),
				payLoad.getSearchParam(), payLoad.getSearchStartDate(), payLoad.getSearchEndDate());
		return new ResponseEntity<>(grossTrendData, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get supplied item trend data", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = "/getSuppliedItemTrend", produces = "application/json")
	public ResponseEntity<?> getSuppliedItemTrend(@RequestBody CustomerAnalyticsPayLoad payLoad,
			HttpServletRequest request) {
		List<ProductSupplierWiseData> suppliedItemTrendData = analyticsReportService.getProductSuppliedData(
				payLoad.getSupplierCode(), payLoad.getSearchParam(), payLoad.getSearchStartDate(),
				payLoad.getSearchEndDate());
		return new ResponseEntity<>(suppliedItemTrendData, HttpStatus.OK);
	}
}
