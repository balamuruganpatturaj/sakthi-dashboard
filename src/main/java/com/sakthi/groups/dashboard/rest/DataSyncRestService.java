package com.sakthi.groups.dashboard.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sakthi.groups.dashboard.service.DataSyncService;
import com.sakthi.groups.migration.portal.model.BillInfo;
import com.sakthi.groups.migration.portal.model.BillItemDetails;
import com.sakthi.groups.migration.portal.model.BillSearchCriteria;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/rest/datasync")
public class DataSyncRestService {

	@Autowired
	private DataSyncService dataSyncService;

	@ApiOperation(value = "API to sync the supplier data from GoFrugal to Sakthi Portal", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Synced the data successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while syncing data") })
	@GetMapping(path = "/syncsuppliers", produces = "application/json")
	public ResponseEntity<?> syncSuppliers(HttpServletRequest request) {
		boolean syncStatus = dataSyncService.syncSuppliers(false);
		return new ResponseEntity<>(syncStatus, HttpStatus.OK);
	}

	@ApiOperation(value = "API to sync the customer data from GoFrugal to Sakthi Portal", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Synced the data successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while syncing data") })
	@GetMapping(path = "/synccustomers", produces = "application/json")
	public ResponseEntity<?> syncCustomers(HttpServletRequest request) {
		boolean syncStatus = dataSyncService.syncCustomers(false);
		return new ResponseEntity<>(syncStatus, HttpStatus.OK);
	}
	
	@ApiOperation(value = "API to sync the purchase entry data from GoFrugal to Sakthi Portal", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Synced the data successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while syncing data") })
	@GetMapping(path = "/syncpurchaseentry", produces = "application/json")
	public ResponseEntity<?> syncPurchaseEntry(HttpServletRequest request) {
		boolean syncStatus = dataSyncService.syncPurchaseEntry();
		return new ResponseEntity<>(syncStatus, HttpStatus.OK);
	}

	@ApiOperation(value = "API to sync the supplier data from GoFrugal to Sakthi Portal", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Synced the data successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while syncing data") })
	@GetMapping(path = "/syncproducts", produces = "application/json")
	public ResponseEntity<?> syncProducts(HttpServletRequest request) {
		boolean syncStatus = dataSyncService.syncProducts(false);
		return new ResponseEntity<>(syncStatus, HttpStatus.OK);
	}

	@ApiOperation(value = "API to fetch the bill details from GoFrugal", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved the information"),
			@ApiResponse(code = 500, message = "Exception occurred while fetching") })
	@PostMapping(path = "/getBillInfo", produces = "application/json")
	public ResponseEntity<?> getBillInfo(@RequestBody BillSearchCriteria searchCriteria, HttpServletRequest request) {
		List<BillInfo> billInfoList = dataSyncService.getBillInfoList(searchCriteria);
		return new ResponseEntity<>(billInfoList, HttpStatus.OK);
	}

	@ApiOperation(value = "API to fetch the bill details from GoFrugal", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved the information"),
			@ApiResponse(code = 500, message = "Exception occurred while fetching") })
	@GetMapping(path = "/getBillItemDetails", produces = "application/json")
	public ResponseEntity<?> getBillItemDetails(@RequestParam("billNo") Integer billNo,
			@RequestParam("isQuotationBill") boolean isQuotationBill, HttpServletRequest request) {
		List<BillItemDetails> billInfoList = dataSyncService.getBillDetailsList(billNo, isQuotationBill);
		return new ResponseEntity<>(billInfoList, HttpStatus.OK);
	}

	@ApiOperation(value = "API to fetch the single bill info from GoFrugal", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved the information"),
			@ApiResponse(code = 500, message = "Exception occurred while fetching") })
	@GetMapping(path = "/getSingleBillInfo", produces = "application/json")
	public ResponseEntity<?> getBillInfo(@RequestParam("billNo") Integer billNo,
			@RequestParam("isQuotationBill") boolean isQuotationBill, HttpServletRequest request) {
		BillInfo billInfo = dataSyncService.getSingleBillInfo(billNo, isQuotationBill);
		return new ResponseEntity<>(billInfo, HttpStatus.OK);
	}

	@ApiOperation(value = "API to fetch the single bill info from GoFrugal", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved the information"),
			@ApiResponse(code = 500, message = "Exception occurred while fetching") })
	@GetMapping(path = "/getBillAmount", produces = "application/json")
	public ResponseEntity<?> getBillAmount(@RequestParam("billNo") Integer billNo,
			@RequestParam("phoneNo") String phoneNo, HttpServletRequest request) {
		Double billAmount = dataSyncService.getBillAmount(billNo, phoneNo);
		return new ResponseEntity<>(billAmount, HttpStatus.OK);
	}

	@ApiOperation(value = "API to fetch the single bill info from GoFrugal", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved the information"),
			@ApiResponse(code = 500, message = "Exception occurred while fetching") })
	@GetMapping(path = "/getPBillNumber", produces = "application/json")
	public ResponseEntity<?> getPBillNumber(@RequestParam("billNo") Integer billNo,
			@RequestParam("phoneNo") String phoneNo, HttpServletRequest request) {
		Integer pBillNo = dataSyncService.getPBillNumber(billNo, phoneNo);
		return new ResponseEntity<>(pBillNo, HttpStatus.OK);
	}

	@ApiOperation(value = "API to fetch the availble counter from GoFrugal DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved the information"),
			@ApiResponse(code = 500, message = "Exception occurred while fetching") })
	@GetMapping(path = "/getCounterList", produces = "application/json")
	public ResponseEntity<?> getCounterList(HttpServletRequest request) {
		List<String> counterList = dataSyncService.getCounterList();
		return new ResponseEntity<>(counterList, HttpStatus.OK);
	}
}
