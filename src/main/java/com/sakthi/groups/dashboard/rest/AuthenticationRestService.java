package com.sakthi.groups.dashboard.rest;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sakthi.groups.dashboard.service.DashboardService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/auth")
public class AuthenticationRestService {
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private DashboardService dashboardService;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@ApiOperation(value = "Authenticate LCA dashboard with CEC user id and password", consumes="application/x-www-form-urlencoded", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully authenticated and have access"),
			@ApiResponse(code = 401, message = "You are not authorized to view LCA dashboard"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 500, message = "Exception occurred while authenticating") })
	@PostMapping("/login")
	public ResponseEntity<?> authenticateUser(@RequestParam("username") String userName,
			@RequestParam("password") String password) {
		Authentication authentication = null;
		String token = "";
		try {
			authentication = this.authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(userName, password));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Failed to authenticate with user {}", userName);
			return new ResponseEntity<>(token, HttpStatus.UNAUTHORIZED);
		}
		SecurityContextHolder.getContext().setAuthentication(authentication);
		token = dashboardService.generateHMACToken(userName, authentication.getAuthorities());
		return new ResponseEntity<>(token, HttpStatus.OK);
	}
	
	@ApiOperation(value = "Refresh JWT token with existing one in request header", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully refreshed the token"),
			@ApiResponse(code = 500, message = "Exception occurred while refreshing JWT token") })
	@RequestMapping(value = "/refreshToken", method = RequestMethod.GET)
	public ResponseEntity<?> refreshToken(HttpServletRequest request) {
		String token = dashboardService.refreshJwtToken(request);
		return new ResponseEntity<>(token, HttpStatus.OK);
	}
}
