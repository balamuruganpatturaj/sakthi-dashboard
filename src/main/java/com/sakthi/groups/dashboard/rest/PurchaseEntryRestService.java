package com.sakthi.groups.dashboard.rest;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sakthi.groups.dashboard.service.PurchaseBillEntryService;
import com.sakthi.groups.migration.portal.model.PurchaseBillEntry;
import com.sakthi.groups.migration.portal.model.PurchaseBillItemDetails;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/rest/purchaseEntry")
public class PurchaseEntryRestService {

	@Autowired
	private PurchaseBillEntryService billEntryService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@ApiOperation(value = "API to get purchase entry made in GoFrugal appplication", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = { "/getBillEntry", "/getBillEntry/{supplierCode}" }, produces = "application/json")
	public ResponseEntity<?> getPurchaseBillEntry(@PathVariable("supplierCode") Optional<Integer> suppCodeOptional,
			HttpServletRequest request) {
		Integer supplierCode = suppCodeOptional.isPresent() ? suppCodeOptional.get() : null;
		logger.debug("Supplier code: {}", supplierCode);
		List<PurchaseBillEntry> billEntryList = billEntryService.getBillEntry(supplierCode);
		return new ResponseEntity<>(billEntryList, HttpStatus.OK);
	}
	
	@ApiOperation(value = "API to get purchase entry made in GoFrugal appplication", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = { "/getBillItemEntry", "/getBillItemEntry/{productCode}" }, produces = "application/json")
	public ResponseEntity<?> getPurchaseBillItemEntry(@PathVariable("productCode") Optional<Integer> productCodeOptional,
			HttpServletRequest request) {
		Integer productCode = productCodeOptional.isPresent() ? productCodeOptional.get() : null;
		logger.debug("Product code: {}", productCode);
		List<PurchaseBillItemDetails> billItemDetailList = billEntryService.getBillItemEntry(productCode);
		return new ResponseEntity<>(billItemDetailList, HttpStatus.OK);
	}
	
	@ApiOperation(value = "API to get purchase entry made in GoFrugal appplication", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = { "/getGrnItemEntry", "/getGrnItemEntry/{grnRefNo}" }, produces = "application/json")
	public ResponseEntity<?> getGrnBillItemEntry(@PathVariable("grnRefNo") Optional<Integer> grnRefNoOptional,
			HttpServletRequest request) {
		Integer grnRefNo = grnRefNoOptional.isPresent() ? grnRefNoOptional.get() : null;
		logger.debug("GRN MRC NO: {}", grnRefNo);
		List<PurchaseBillItemDetails> billItemDetailList = billEntryService.getGrnBillItemEntry(grnRefNo);
		return new ResponseEntity<>(billItemDetailList, HttpStatus.OK);
	}
}
