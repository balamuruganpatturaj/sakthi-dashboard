package com.sakthi.groups.dashboard.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sakthi.groups.dashboard.bean.SupplierNames;
import com.sakthi.groups.dashboard.model.InvoiceDetails;
import com.sakthi.groups.dashboard.model.SupplierMaster;
import com.sakthi.groups.dashboard.service.PurchaseAckService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/rest/purchaseack")
public class PurchaseAckRestService {

	@Autowired
	private PurchaseAckService purchaseAckService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@ApiOperation(value = "API to get the Supplier names availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = "/getSupplierNames", produces = "application/json")
	public ResponseEntity<?> getSupplierNames(HttpServletRequest request) {
		logger.info("Supplier Name {}", request.getRemoteUser());
		List<SupplierNames> supplierNames = purchaseAckService.getSupplierNames();
		return new ResponseEntity<>(supplierNames, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the Supplier Details availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = "/getSupplierDetails", produces = "application/json")
	public ResponseEntity<?> getSupplierDetails(HttpServletRequest request) {
		logger.info("Supplier Details {}", request.getRemoteUser());
		List<SupplierMaster> supplierDetailList = purchaseAckService.getSupplierDetails();
		return new ResponseEntity<>(supplierDetailList, HttpStatus.OK);
	}

	@ApiOperation(value = "API to store new Supplier in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully stored the details"),
			@ApiResponse(code = 500, message = "Exception occurred while storing details") })
	@PostMapping(path = "/createSupplier", produces = "application/json")
	public ResponseEntity<?> createSupplier(@RequestBody SupplierMaster supplierDetails, HttpServletRequest request) {
		logger.info("Supplier Details {}", request.getRemoteUser());
		boolean returnStatus = purchaseAckService.createNewSupplier(supplierDetails, request.getRemoteUser());
		return new ResponseEntity<>(returnStatus, HttpStatus.OK);
	}

	@ApiOperation(value = "API to edit Supplier in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully stored the details"),
			@ApiResponse(code = 500, message = "Exception occurred while storing details") })
	@PostMapping(path = "/editSupplier", produces = "application/json")
	public ResponseEntity<?> editSupplier(@RequestBody SupplierMaster supplierDetails, HttpServletRequest request) {
		logger.info("Supplier Details {}", request.getRemoteUser());
		boolean returnStatus = purchaseAckService.editSupplier(supplierDetails, request.getRemoteUser());
		return new ResponseEntity<>(returnStatus, HttpStatus.OK);
	}

	@ApiOperation(value = "API to store Invoice details in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully stored the details"),
			@ApiResponse(code = 500, message = "Exception occurred while storing details") })
	@PostMapping(path = "/saveInvoiceDetails", produces = "application/json")
	public ResponseEntity<?> saveInvoiceDetails(@RequestBody List<InvoiceDetails> invoiceDetails,
			HttpServletRequest request) {
		logger.info("Supplier Details {}", request.getRemoteUser());
		boolean returnStatus = purchaseAckService.saveInvoiceDetails(invoiceDetails);
		return new ResponseEntity<>(returnStatus, HttpStatus.OK);
	}

	@ApiOperation(value = "API to check Invoice details in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved the details"),
			@ApiResponse(code = 500, message = "Exception occurred while storing details") })
	@PostMapping(path = "/checkInvoiceDetails", produces = "application/json")
	public ResponseEntity<?> checkInvoiceDetails(@RequestBody InvoiceDetails invoiceDetails,
			HttpServletRequest request) {
		logger.info("Supplier Details {}", request.getRemoteUser());
		boolean returnStatus = purchaseAckService.checkInvoiceAvailable(invoiceDetails.getSupplierName(),
				invoiceDetails.getInvoiceNo(), invoiceDetails.getInvoiceDate());
		return new ResponseEntity<>(returnStatus, HttpStatus.OK);
	}
}
