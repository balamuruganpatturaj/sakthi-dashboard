package com.sakthi.groups.dashboard.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sakthi.groups.dashboard.service.SalesMgmtService;
import com.sakthi.groups.migration.portal.model.ProductMaster;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/rest/sales")
public class SalesManagementRestService {

	@Autowired
	private SalesMgmtService salesService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@ApiOperation(value = "API to get the product names availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = "/getProductDetail", produces = "application/json")
	public ResponseEntity<?> getProductDetails(@RequestParam("productSearch") String productSearch,
			HttpServletRequest request) {
		logger.info("Invoking getProductDetails()");
		List<ProductMaster> productDetails = salesService.getProductDetails(productSearch);
		return new ResponseEntity<>(productDetails, HttpStatus.OK);
	}
}
