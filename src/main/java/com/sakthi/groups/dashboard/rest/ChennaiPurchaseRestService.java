package com.sakthi.groups.dashboard.rest;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sakthi.groups.dashboard.bean.ChennaiProductInfo;
import com.sakthi.groups.dashboard.model.ChennaiPurchase;
import com.sakthi.groups.dashboard.service.ChennaiPurchaseService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/rest/chennaipurchase")
public class ChennaiPurchaseRestService {

	@Autowired
	private ChennaiPurchaseService chennaiPurchaseService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@ApiOperation(value = "API to get the product names availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = "/getFilteredProductNames", produces = "application/json")
	public ResponseEntity<?> getFilteredProductNames(@RequestParam("productName") String productName,
			HttpServletRequest request) {
		Set<String> productNames = chennaiPurchaseService.getFilteredProductNames(productName);
		return new ResponseEntity<>(productNames, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the product info availabe in DB using EanCode", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = { "/getEanCodeProductInfo" }, produces = "application/json")
	public ResponseEntity<?> getEanCodeProductInfo(@RequestParam("eanCode") String eanCode,
			HttpServletRequest request) {
		try {
			eanCode = URLDecoder.decode(eanCode, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("Unsupported Encoding {}", e.getMessage());
		}
		ChennaiProductInfo productInfo = chennaiPurchaseService.getEanCodeProductInfo(eanCode);
		return new ResponseEntity<>(productInfo, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the product info availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = { "/getProductInfo" }, produces = "application/json")
	public ResponseEntity<?> getProductInfo(@RequestParam("productName") String productName,
			HttpServletRequest request) {
		try {
			productName = URLDecoder.decode(productName, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("Unsupported Encoding {}", e.getMessage());
		}
		ChennaiProductInfo productInfo = chennaiPurchaseService.getProductInfo(productName);
		return new ResponseEntity<>(productInfo, HttpStatus.OK);
	}

	@ApiOperation(value = "API to add or update purchase history details", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Added or Updated purchase history details"),
			@ApiResponse(code = 500, message = "Exception occurred while Adding/Updating details") })
	@PostMapping(path = "/saveChennaiPurchase", produces = "application/json")
	public ResponseEntity<?> saveChennaiPurchase(@RequestBody List<ChennaiPurchase> chennaiPurchaseList,
			HttpServletRequest request) {
		boolean purHistoryUpdResponse = chennaiPurchaseService.saveChennaiPurchase(chennaiPurchaseList);
		return new ResponseEntity<>(purHistoryUpdResponse, HttpStatus.OK);
	}

	@ApiOperation(value = "API to add or update purchase history details", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Added or Updated purchase history details"),
			@ApiResponse(code = 500, message = "Exception occurred while Adding/Updating details") })
	@PostMapping(path = "/updateChennaiPurchase", produces = "application/json")
	public ResponseEntity<?> updateChennaiPurchase(@RequestBody List<ChennaiPurchase> chennaiPurchaseList,
			HttpServletRequest request) {
		boolean purHistoryUpdResponse = chennaiPurchaseService.updateChennaiPurchase(chennaiPurchaseList);
		return new ResponseEntity<>(purHistoryUpdResponse, HttpStatus.OK);
	}

	@ApiOperation(value = "API to delete product entry", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully deleted the product in DB"),
			@ApiResponse(code = 500, message = "Exception occurred while deleting the product") })
	@PostMapping(path = "/deleteProduct", produces = "application/json")
	public ResponseEntity<?> deleteEntry(@RequestBody List<ChennaiPurchase> chennaiPurchaseList,
			HttpServletRequest request) {
		boolean purHistoryUpdResponse = chennaiPurchaseService.deleteProduct(chennaiPurchaseList);
		return new ResponseEntity<>(purHistoryUpdResponse, HttpStatus.OK);
	}

	@ApiOperation(value = "API to add or update purchase history details", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Added or Updated purchase history details"),
			@ApiResponse(code = 500, message = "Exception occurred while Adding/Updating details") })
	@PostMapping(path = "/checkChennaiPurchase", produces = "application/json")
	public ResponseEntity<?> checkChennaiPurchase(@RequestBody ChennaiPurchase chennaiPurchase,
			HttpServletRequest request) {
		boolean isChennaiPurchaseAvailable = chennaiPurchaseService.isPurchaseAlreadyAdded(chennaiPurchase);
		return new ResponseEntity<>(isChennaiPurchaseAvailable, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the purchase history details", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the purchase history details"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = { "/getChennaiPurchase", "/getChennaiPurchase/{productName}" }, produces = "application/json")
	public ResponseEntity<?> getPurchaseHistoryData(@RequestParam("dateSlotValue") String dateSlotValue,
			@PathVariable("productName") Optional<String> productName, HttpServletRequest request) {
		logger.info("productName: " + productName);
		String prodName = productName.isPresent() ? productName.get() : null;
		List<ChennaiPurchase> chennaiPurchaseList = chennaiPurchaseService.getChennaiPurchase(prodName, dateSlotValue);
		return new ResponseEntity<>(chennaiPurchaseList, HttpStatus.OK);
	}
}
