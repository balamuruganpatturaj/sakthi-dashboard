package com.sakthi.groups.dashboard.rest;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.sakthi.groups.dashboard.service.RodeoDataVerifyService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/rest/rodeodata")
public class RodeoDataCheckRestService {

	@Autowired
	private RodeoDataVerifyService rodeoDataService;

	@ApiOperation(value = "API to fetch Rodeo missing product details from GoFrugal", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully fetched the data from GoFrugal"),
			@ApiResponse(code = 500, message = "Exception occurred while fetching details") })
	@PostMapping(path = "/checkMissingProducts", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = "application/vnd.ms-excel")
	public ResponseEntity<?> checkMissingProducts(
			@RequestParam(value = "rodeoExcelFile", required = true) MultipartFile rodeoExcelFile,
			@RequestParam(value = "daysToCheck", required = true) Integer daysToCheck,
			@RequestParam(value = "chooseItemFrom", required = true) String chooseItemFrom) {
		String fileName = "";
		InputStream excelStream = null;
		byte[] excelBytesArray = null;
		try {
			excelStream = rodeoExcelFile.getInputStream();
			excelBytesArray = rodeoDataService.getMissingProducts(excelStream, daysToCheck, chooseItemFrom);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (excelStream != null) {
				try {
					excelStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		if ("Purchase".equalsIgnoreCase(chooseItemFrom)) {
			fileName = "Purchase_Missing_Items.xlsx";
		} else if ("Repackage".equalsIgnoreCase(chooseItemFrom)) {
			fileName = "Repack_Missing_Items.xlsx";
		} else {
			fileName = "Sales_Missing_Items.xlsx";
		}
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/vnd.ms-excel");
		headers.set("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
		return new ResponseEntity<>(excelBytesArray, headers, HttpStatus.OK);
	}
}
