package com.sakthi.groups.dashboard.rest;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sakthi.groups.dashboard.bean.ProductInfo;
import com.sakthi.groups.dashboard.bean.PurchaseHistoryData;
import com.sakthi.groups.dashboard.model.LatestPurchaseHistory;
import com.sakthi.groups.dashboard.model.PurchaseFieldChange;
import com.sakthi.groups.dashboard.model.PurchaseHistory;
import com.sakthi.groups.dashboard.model.chart.GoogleChartModel;
import com.sakthi.groups.dashboard.service.PurchaseHistoryService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/rest/purchase")
public class PurchaseHistoryRestService {

	@Autowired
	private PurchaseHistoryService purHistoryService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@ApiOperation(value = "API to get the product names availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = "/getProductNames", produces = "application/json")
	public ResponseEntity<?> getProductNames(HttpServletRequest request) {
		List<String> productNames = purHistoryService.getProductNames();
		return new ResponseEntity<>(productNames, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the product names availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = "/getFilteredProductNames", produces = "application/json")
	public ResponseEntity<?> getFilteredProductNames(@RequestParam("productName") String productName,
			HttpServletRequest request) {
		try {
			productName = URLDecoder.decode(productName, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("Unsupported Encoding {}", e.getMessage());
		}
		logger.info("getFilteredProductNames productName: " + productName);
		List<String> productNames = purHistoryService.getFilteredProductNames(productName);
		return new ResponseEntity<>(productNames, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the product category availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = { "/getProductCategory", "/getProductCategory/{productName}" }, produces = "application/json")
	public ResponseEntity<?> getProductCategory(@PathVariable("productName") Optional<String> productName,
			HttpServletRequest request) {
		String prodName = productName.isPresent() ? productName.get() : null;
		Set<String> productCategory = purHistoryService.getProductCategory(prodName);
		return new ResponseEntity<>(productCategory, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the product info availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = { "/getProductInfo" }, produces = "application/json")
	public ResponseEntity<?> getProductInfo(@RequestParam("productName") String productName,
			HttpServletRequest request) {
		try {
			productName = URLDecoder.decode(productName, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("Unsupported Encoding {}", e.getMessage());
		}
		ProductInfo productInfo = purHistoryService.getProductInfo(productName);
		return new ResponseEntity<>(productInfo, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the purchase history details", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the purchase history details"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = { "/getPurchaseHistory" }, produces = "application/json")
	public ResponseEntity<?> getPurchaseHistory(@RequestParam("productName") String productName,
			HttpServletRequest request) {
		try {
			productName = URLDecoder.decode(productName, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("Unsupported Encoding {}", e.getMessage());
		}
		logger.info("getPurchaseHistory productName: " + productName);
		List<PurchaseHistory> purHistory = purHistoryService.getPurchaseHistory(productName);
		return new ResponseEntity<>(purHistory, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the purchase history details", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the purchase history details"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = { "/getPurchaseHistoryData" }, produces = "application/json")
	public ResponseEntity<?> getPurchaseHistoryData(@RequestParam("productName") String productName,
			HttpServletRequest request) {
		try {
			productName = URLDecoder.decode(productName, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("Unsupported Encoding {}", e.getMessage());
		}
		logger.info("getPurchaseHistoryData productName: " + productName);
		List<PurchaseHistoryData> purHistory = purHistoryService.getPurchaseHistoryData(productName);
		return new ResponseEntity<>(purHistory, HttpStatus.OK);
	}

	@ApiOperation(value = "API to add or update purchase history details", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Added or Updated purchase history details"),
			@ApiResponse(code = 500, message = "Exception occurred while Adding/Updating details") })
	@PostMapping(path = "/addUpdatePurchase", produces = "application/json")
	public ResponseEntity<?> addUpdatePurchase(@RequestBody List<PurchaseHistory> purchaseHistList,
			HttpServletRequest request) {
		boolean purHistoryUpdResponse = purHistoryService.addOrUpdatePurchaseHistory(purchaseHistList);
		return new ResponseEntity<>(purHistoryUpdResponse, HttpStatus.OK);
	}

	@ApiOperation(value = "API to check whether purchase already entered for that date", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Added or Updated purchase history details"),
			@ApiResponse(code = 500, message = "Exception occurred while Adding/Updating details") })
	@PostMapping(path = "/checkPurchaseExists", produces = "application/json")
	public ResponseEntity<?> checkPurchaseExists(@RequestBody PurchaseHistory purchaseHist,
			HttpServletRequest request) {
		boolean isPurchaseAvailable = purHistoryService.checkPurchaseHistory(purchaseHist);
		return new ResponseEntity<>(isPurchaseAvailable, HttpStatus.OK);
	}

	@ApiOperation(value = "API to update purchase history Caches", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = "/updatePurchaseCache", produces = "application/json")
	public ResponseEntity<?> updatePurchaseCaches(HttpServletRequest request) {
		purHistoryService.updatePurchaseCache();
		return new ResponseEntity<>(true, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the purchase history details", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the purchase history details"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = { "/getLatestPurchase" }, produces = "application/json")
	public ResponseEntity<?> getLatestPurchase(@RequestParam("productName") String productName,
			HttpServletRequest request) {
		try {
			productName = URLDecoder.decode(productName, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("Unsupported Encoding {}", e.getMessage());
		}
		logger.info("getLatestPurchase productName: " + productName);
		PurchaseHistory purHistory = purHistoryService.getLatestPurchaseHistory(productName);
		return new ResponseEntity<>(purHistory, HttpStatus.OK);
	}

	@ApiOperation(value = "API to update product & last purchase entry", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Product & last purchase updated successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while Adding/Updating details") })
	@PostMapping(path = "/updateProductDetail", produces = "application/json")
	public ResponseEntity<?> updateProduct(@RequestBody List<PurchaseHistory> purchaseHist,
			HttpServletRequest request) {
		boolean purHistoryUpdResponse = purHistoryService.updateProductDetials(purchaseHist.get(0));
		return new ResponseEntity<>(purHistoryUpdResponse, HttpStatus.OK);
	}

	@ApiOperation(value = "API to delete product entry", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully deleted the product in DB"),
			@ApiResponse(code = 500, message = "Exception occurred while deleting the product") })
	@PostMapping(path = "/deleteProduct", produces = "application/json")
	public ResponseEntity<?> deleteEntry(@RequestBody List<PurchaseHistory> purchaseHist, HttpServletRequest request) {
		boolean purHistoryUpdResponse = purHistoryService.deleteProduct(purchaseHist);
		return new ResponseEntity<>(purHistoryUpdResponse, HttpStatus.OK);
	}

	@ApiOperation(value = "API to merge two purchase history", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully merged in DB"),
			@ApiResponse(code = 500, message = "Exception occurred while deleting the product") })
	@PostMapping(path = "/mergePurchaseHistory", produces = "application/json")
	public ResponseEntity<?> mergePurchaseHistory(@RequestParam("fromProdName") String fromProdName,
			@RequestParam("toProdName") String toProdName, HttpServletRequest request) {
		boolean mergeResponse = purHistoryService.mergePurchaseHistory(fromProdName, toProdName);
		return new ResponseEntity<>(mergeResponse, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the purchase trend data", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the purchase trend data"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving data") })
	@PostMapping(path = { "/getPurchaseTrend" }, produces = "application/json")
	public ResponseEntity<?> getPurchaseTrend(@RequestParam("productName") String productName,
			HttpServletRequest request) {
		try {
			productName = URLDecoder.decode(productName, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("Unsupported Encoding {}", e.getMessage());
		}
		logger.info("getPurchaseTrend productName: " + productName);
		GoogleChartModel purchaseTrendData = purHistoryService.getPurchaseTrendData(productName);
		return new ResponseEntity<>(purchaseTrendData, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the purchase trend data", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the purchase trend data"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving data") })
	@PostMapping(path = { "/advancePurSearch" }, produces = "application/json")
	public ResponseEntity<?> getPurchaseSearchResult(@RequestBody Map<String, Object> searchParameter,
			HttpServletRequest request) {
		logger.info("searchParameter: {}", searchParameter.toString());
		List<LatestPurchaseHistory> purSearchList = purHistoryService.getPurchaseSearchResult(searchParameter);
		return new ResponseEntity<>(purSearchList, HttpStatus.OK);
	}
	
	@ApiOperation(value = "API to get the purchase field change formula", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the purchase field change formula"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving data") })
	@PostMapping(path = { "/getPurchaseFieldFomula" }, produces = "application/json")
	public ResponseEntity<?> getPurchaseFieldFomula(@RequestParam("productName") String productName,
			HttpServletRequest request) {
		try {
			productName = URLDecoder.decode(productName, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("Unsupported Encoding {}", e.getMessage());
		}
		logger.info("getPurchaseFieldFomula productName: " + productName);
		PurchaseFieldChange purchaseFieldFormula = purHistoryService.getPurchaseFieldFomula(productName);
		return new ResponseEntity<>(purchaseFieldFormula, HttpStatus.OK);
	}
	
	@ApiOperation(value = "API to save the purchase field change formula", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully saved purchase field change formula"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving data") })
	@PostMapping(path = { "/savePurchaseFieldFomula" }, produces = "application/json")
	public ResponseEntity<?> savePurchaseFieldFomula(@RequestBody PurchaseFieldChange purchaseFieldFormula,
			HttpServletRequest request) {
		logger.info("purchaseFieldFormula: " + purchaseFieldFormula);
		boolean saveResponse = purHistoryService.savePurchaseFieldFomula(purchaseFieldFormula);
		return new ResponseEntity<>(saveResponse, HttpStatus.OK);
	}
	
	@ApiOperation(value = "API to migrate purchase formula", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the purchase trend data"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving data") })
	@GetMapping(path = { "/migratePurchseFormula" }, produces = "application/json")
	public ResponseEntity<?> migratePurchaseFormula(
			HttpServletRequest request) {
		purHistoryService.migratePurchaseFormula();
		return new ResponseEntity<>("Success", HttpStatus.OK);
	}
}
