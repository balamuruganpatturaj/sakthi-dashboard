package com.sakthi.groups.dashboard.rest;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.sakthi.groups.dashboard.bean.SettlementReport;
import com.sakthi.groups.dashboard.bean.UpdateSettlementData;
import com.sakthi.groups.dashboard.model.EdcPosDetail;
import com.sakthi.groups.dashboard.model.ReversalReport;
import com.sakthi.groups.dashboard.model.SettlementDetail;
import com.sakthi.groups.dashboard.service.SettlementMgmtService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/rest/settlement")
public class SettlementMgmtRestService {

	@Autowired
	private SettlementMgmtService settlementService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@ApiOperation(value = "API to get the Supplier names availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = "/getavailablepos", produces = "application/json")
	public ResponseEntity<?> getAvailablePOS(@RequestParam("isactive") boolean isActive, HttpServletRequest request) {
		logger.debug("SettlementMgmtService remote login user {}", request.getRemoteUser());
		List<EdcPosDetail> availablePos = settlementService.getAvailablePOS(isActive);
		return new ResponseEntity<>(availablePos, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the Supplier names availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = "/addnewposmachine", produces = "application/json")
	public ResponseEntity<?> addNewPosDetail(@RequestBody EdcPosDetail edcPosDetail, HttpServletRequest request) {
		logger.debug("SettlementMgmtService remote login user {}", request.getRemoteUser());
		String creationStatus = settlementService.createNewPOSEntry(edcPosDetail);
		return new ResponseEntity<>("{ \"response\": \"" + creationStatus + "\"}", HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the Supplier names availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = "/editposmachine", produces = "application/json")
	public ResponseEntity<?> editPosDetail(@RequestBody EdcPosDetail edcPosDetail, HttpServletRequest request) {
		logger.debug("SettlementMgmtService remote login user {}", request.getRemoteUser());
		String editStatus = settlementService.editPOSEntry(edcPosDetail);
		return new ResponseEntity<>("{ \"response\": \"" + editStatus + "\"}", HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the Supplier names availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = "/checksettlementstatus", produces = "application/json")
	public ResponseEntity<?> checkSettlementStatus(@RequestBody SettlementDetail settlementDetail,
			HttpServletRequest request) {
		logger.debug("SettlementMgmtService remote login user {}", request.getRemoteUser());
		boolean settlementStatus = settlementService.checkSettlementStatus(settlementDetail);
		return new ResponseEntity<>(settlementStatus, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the Supplier names availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = "/savesettlement", produces = "application/json")
	public ResponseEntity<?> saveSettlement(@RequestBody SettlementDetail settlementDetail,
			HttpServletRequest request) {
		logger.debug("SettlementMgmtService remote login user {}", request.getRemoteUser());
		boolean status = settlementService.saveDailySettlement(settlementDetail);
		return new ResponseEntity<>(status, HttpStatus.OK);
	}
	
	@ApiOperation(value = "API to get the Supplier names availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = "/editsettlement", produces = "application/json")
	public ResponseEntity<?> editSettlement(@RequestBody SettlementDetail settlementDetail,
			HttpServletRequest request) {
		logger.debug("SettlementMgmtService remote login user {}", request.getRemoteUser());
		boolean status = settlementService.editDailySettlement(settlementDetail);
		return new ResponseEntity<>(status, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the Supplier names availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = "/getsettlementreport", produces = "application/json")
	public ResponseEntity<?> getSettlementReport(HttpServletRequest request) {
		logger.debug("SettlementMgmtService remote login user {}", request.getRemoteUser());
		List<SettlementReport> settlementReport = settlementService.getSettlementReport();
		return new ResponseEntity<>(settlementReport, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the Supplier names availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = "/updatesettlement", produces = "application/json")
	public ResponseEntity<?> updateSettlement(@RequestBody UpdateSettlementData updateSettlementData,
			HttpServletRequest request) {
		logger.debug("SettlementMgmtService remote login user {}", request.getRemoteUser());
		boolean status = settlementService.updateDailySettlement(updateSettlementData);
		return new ResponseEntity<>(status, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the Supplier names availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = "/getreversalreport", produces = "application/json")
	public ResponseEntity<?> getReversalReport(HttpServletRequest request) {
		logger.debug("SettlementMgmtService remote login user {}", request.getRemoteUser());
		List<ReversalReport> reversalReport = settlementService.getReversalReport();
		return new ResponseEntity<>(reversalReport, HttpStatus.OK);
	}

	@ApiOperation(value = "API to create new Employee in portal", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Added new customer details"),
			@ApiResponse(code = 500, message = "Exception occurred while Adding/Updating details") })
	@PostMapping(path = "/refundreversal", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = "application/json")
	public ResponseEntity<?> updateReversalRefund(
			@RequestParam(value = "letterProof", required = true) MultipartFile letterProof,
			@RequestParam(value = "letterProofName", required = true) String letterProofName,
			@RequestParam(value = "identityProof", required = true) MultipartFile identityProof,
			@RequestParam(value = "identityProofName", required = true) String identityProofName,
			@RequestParam(value = "tidNo", required = true) BigInteger tidNo,
			@RequestParam(value = "assignedParty", required = true) String assignedParty,
			@RequestParam(value = "cardType", required = true) String cardType,
			@RequestParam(value = "cardNumber", required = true) String cardNumber,
			@RequestParam(value = "reversalAmount", required = true) BigDecimal reversalAmount,
			@RequestParam(value = "amountRevered", required = true) boolean amountRevered,
			@RequestParam(value = "amountRefunded", required = true) boolean amountRefunded,
			@RequestParam(value = "txnDate", required = true) Date txnDate, HttpServletRequest request) {
		ReversalReport reversalReport = new ReversalReport();
		try {
			Binary letterProofBinary = null;
			if (letterProof != null) {
				letterProofBinary = new Binary(BsonBinarySubType.BINARY, letterProof.getBytes());
			}

			Binary identityProofBinary = null;
			if (identityProof != null) {
				identityProofBinary = new Binary(BsonBinarySubType.BINARY, identityProof.getBytes());
			}

			reversalReport = new ReversalReport(tidNo, assignedParty, txnDate, cardType, cardNumber, reversalAmount,
					amountRevered, amountRefunded, letterProofBinary, letterProofName, identityProofBinary,
					identityProofName);

		} catch (IOException e) {
			e.printStackTrace();
		}
		boolean status = settlementService.updateReversalRefund(reversalReport);
		return new ResponseEntity<>(status, HttpStatus.OK);
	}

}
