package com.sakthi.groups.dashboard.rest;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.sakthi.groups.dashboard.model.Company;
import com.sakthi.groups.dashboard.service.CompanyService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/rest/company")
public class CompanyMgmtRestService {

	@Autowired
	private CompanyService companyService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@ApiOperation(value = "API to get the registered company availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = "/getcompany", produces = "application/json")
	public ResponseEntity<?> getCompany(@RequestParam("isactive") boolean isActive, HttpServletRequest request) {
		logger.debug("CompanyMgmtRestService remote login user {}", request.getRemoteUser());
		List<Company> availablePos = companyService.getAvailableCompany(isActive);
		return new ResponseEntity<>(availablePos, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the registered company availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = "/getcompanydetail", produces = "application/json")
	public ResponseEntity<?> getCompanyDetail(@RequestParam("isactive") boolean isActive,
			@RequestParam("companyName") String companyName, HttpServletRequest request) {
		logger.debug("CompanyMgmtRestService remote login user {}", request.getRemoteUser());
		Company company = companyService.getAvailableCompanyDetail(isActive, companyName);
		return new ResponseEntity<>(company, HttpStatus.OK);
	}

	@ApiOperation(value = "API to create new Employee in portal", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Added new customer details"),
			@ApiResponse(code = 500, message = "Exception occurred while Adding/Updating details") })
	@PostMapping(path = "/createcompany", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = "application/json")
	public ResponseEntity<?> createCompany(@RequestParam(value = "gstFile", required = false) MultipartFile gstFile,
			@RequestParam(value = "gstFileName", required = false) String gstFileName,
			@RequestParam(value = "panFile", required = false) MultipartFile panFile,
			@RequestParam(value = "panFileName", required = false) String panFileName,
			@RequestParam(value = "companyName", required = true) String companyName,
			@RequestParam(value = "address", required = true) String address,
			@RequestParam(value = "location", required = true) String location,
			@RequestParam(value = "area", required = true) String area,
			@RequestParam(value = "pincode", required = true) Integer pincode,
			@RequestParam(value = "contactNo", required = true) String contactNo,
			@RequestParam(value = "gstNo", required = true) String gstNo,
			@RequestParam(value = "panNo", required = true) String panNo,
			@RequestParam(value = "gstType", required = true) String gstType,
			@RequestParam(value = "status", required = true) boolean status, HttpServletRequest request) {
		Company company = new Company();
		try {
			Binary gstFileBinary = null;
			if (gstFile != null) {
				gstFileBinary = new Binary(BsonBinarySubType.BINARY, gstFile.getBytes());
			}

			Binary panFileBinary = null;
			if (panFile != null) {
				panFileBinary = new Binary(BsonBinarySubType.BINARY, panFile.getBytes());
			}
			List<String> contactList = Arrays.asList(contactNo.split(","));
			company = new Company(companyName, address, location, area, pincode, contactList, status, gstNo, panNo,
					gstType, gstFileBinary, gstFileName, panFileBinary, panFileName);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String creationStatus = companyService.createNewCompany(company);
		return new ResponseEntity<>("{ \"response\": \"" + creationStatus + "\"}", HttpStatus.OK);
	}

	@ApiOperation(value = "API to edit the created company in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = "/editcompany", produces = "application/json")
	public ResponseEntity<?> editCompany(@RequestBody Company company, HttpServletRequest request) {
		logger.debug("CompanyMgmtRestService remote login user {}", request.getRemoteUser());
		String editStatus = companyService.editCompany(company);
		return new ResponseEntity<>("{ \"response\": \"" + editStatus + "\"}", HttpStatus.OK);
	}

}
