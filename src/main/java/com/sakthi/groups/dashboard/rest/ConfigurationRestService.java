package com.sakthi.groups.dashboard.rest;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sakthi.groups.dashboard.service.ConfigurationService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/rest/propsmanage")
public class ConfigurationRestService {

	@Autowired
	private ConfigurationService configService;
	
	@ApiOperation(value = "API to get the configuration availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = "/getConfigDetails", produces = "application/json")
	public ResponseEntity<?> getConfiguration(HttpServletRequest request) {
		Map<String, String> configuration = configService.getConfigDetails();
		return new ResponseEntity<>(configuration, HttpStatus.OK);
	}
}
