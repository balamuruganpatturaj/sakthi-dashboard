package com.sakthi.groups.dashboard.rest;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.sakthi.groups.dashboard.bean.OfferInputDetail;
import com.sakthi.groups.dashboard.model.CustomerAnalyticsPayLoad;
import com.sakthi.groups.dashboard.model.CustomerMaster;
import com.sakthi.groups.dashboard.model.EmployeeMaster;
import com.sakthi.groups.dashboard.model.GiftDetails;
import com.sakthi.groups.dashboard.model.OfferDetails;
import com.sakthi.groups.dashboard.service.PeopleMgmtService;
import com.sakthi.groups.migration.portal.model.BillAnalyticsData;
import com.sakthi.groups.migration.portal.model.TopGrossTrendData;
import com.sakthi.groups.migration.portal.model.TopSellingTrendData;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/rest/pplmanage")
public class PeopleManagementRestService {

	@Autowired
	private PeopleMgmtService pplMgmtService;

	@ApiOperation(value = "API to get the Customer Details availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = "/getCustomerDetails", produces = "application/json")
	public ResponseEntity<?> getCustomerDetails(HttpServletRequest request) {
		List<CustomerMaster> customerMaster = pplMgmtService.getCustomerDetails();
		return new ResponseEntity<>(customerMaster, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the Customer names availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = "/getCustomerNames", produces = "application/json")
	public ResponseEntity<?> getCustomerNames(HttpServletRequest request) {
		List<String> customerNameList = pplMgmtService.getCustomerNames();
		return new ResponseEntity<>(customerNameList, HttpStatus.OK);
	}

	@ApiOperation(value = "API to create new Customer in portal", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Added new customer details"),
			@ApiResponse(code = 500, message = "Exception occurred while Adding/Updating details") })
	@PostMapping(path = "/createCustomer", produces = "application/json")
	public ResponseEntity<?> createNewCustomer(@RequestBody CustomerMaster customer, HttpServletRequest request) {
		boolean createResponse = pplMgmtService.saveNewCustomer(customer, request.getRemoteUser());
		return new ResponseEntity<>(createResponse, HttpStatus.OK);
	}

	@ApiOperation(value = "API to edit Customer in portal", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully updated customer details"),
			@ApiResponse(code = 500, message = "Exception occurred while Adding/Updating details") })
	@PostMapping(path = "/editCustomer", produces = "application/json")
	public ResponseEntity<?> editCustomer(@RequestBody CustomerMaster customer, HttpServletRequest request) {
		boolean editResponse = pplMgmtService.editCustomer(customer, request.getRemoteUser());
		return new ResponseEntity<>(editResponse, HttpStatus.OK);
	}

	@ApiOperation(value = "API to add Customer offer in portal", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully updated customer details"),
			@ApiResponse(code = 500, message = "Exception occurred while Adding/Updating details") })
	@PostMapping(path = "/addCustomerOffer", produces = "application/json")
	public ResponseEntity<?> addCustomerOffer(@RequestBody OfferDetails offerDetails, HttpServletRequest request) {
		boolean offerResponse = pplMgmtService.addCustomerOffer(offerDetails);
		return new ResponseEntity<>(offerResponse, HttpStatus.OK);
	}

	@ApiOperation(value = "API to add Customer offer in portal", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully updated customer details"),
			@ApiResponse(code = 500, message = "Exception occurred while Adding/Updating details") })
	@PostMapping(path = "/checkCustomerOffer", produces = "application/json")
	public ResponseEntity<?> checkCustomerOffer(@RequestBody OfferDetails offerDetails, HttpServletRequest request) {
		String checkResponse = pplMgmtService.checkCustomerOffer(offerDetails);
		return new ResponseEntity<>("{ \"response\": \"" + checkResponse + "\"}", HttpStatus.OK);
	}

	@ApiOperation(value = "API to get issued Customer Gift offer in portal", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully added gift details"),
			@ApiResponse(code = 500, message = "Exception occurred while Adding/Updating details") })
	@GetMapping(path = "/getIssuedOffers", produces = "application/json")
	public ResponseEntity<?> getGiftOfferDetails(HttpServletRequest request) {
		List<GiftDetails> issuedOfferList = pplMgmtService.getIssuedOffers();
		return new ResponseEntity<>(issuedOfferList, HttpStatus.OK);
	}

	@ApiOperation(value = "API to add Customer Gift offer in portal", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully added gift details"),
			@ApiResponse(code = 500, message = "Exception occurred while Adding/Updating details") })
	@PostMapping(path = "/addGiftOffer", produces = "application/json")
	public ResponseEntity<?> addCustomerGiftOffer(@RequestBody OfferInputDetail offerInputDetail,
			HttpServletRequest request) {
		boolean offerResponse = pplMgmtService.addGiftOffer(offerInputDetail);
		return new ResponseEntity<>(offerResponse, HttpStatus.OK);
	}

	@ApiOperation(value = "API to check  Customer offer in portal", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully updated customer details"),
			@ApiResponse(code = 500, message = "Exception occurred while Adding/Updating details") })
	@PostMapping(path = "/checkGiftOffer", produces = "application/json")
	public ResponseEntity<?> checkCustomerGiftOffer(@RequestBody OfferInputDetail offerInputDetail,
			HttpServletRequest request) {
		String checkResponse = pplMgmtService.checkGiftOffer(offerInputDetail);
		return new ResponseEntity<>("{ \"response\": \"" + checkResponse + "\"}", HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the employee Details availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = "/getEmployeeDetails", produces = "application/json")
	public ResponseEntity<?> getEmployeeDetails(HttpServletRequest request) {
		List<EmployeeMaster> employeeDetails = pplMgmtService.getEmployeeDetails();
		return new ResponseEntity<>(employeeDetails, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the employee Details availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = "/getAddressProof")
	public ResponseEntity<?> getAddressProof(@RequestParam(value = "phoneNumber") BigInteger phoneNumber,
			HttpServletRequest request) {
		Binary addressProof = pplMgmtService.getAddressProof(phoneNumber);
		return new ResponseEntity<>(addressProof, HttpStatus.OK);
	}

	@ApiOperation(value = "API to create new Employee in portal", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Added new customer details"),
			@ApiResponse(code = 500, message = "Exception occurred while Adding/Updating details") })
	@PostMapping(path = "/createEmployee", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = "application/json")
	public ResponseEntity<?> createEmployee(
			@RequestParam(value = "addressProof", required = false) MultipartFile addressProof,
			@RequestParam(value = "fileName", required = true) String fileName,
			@RequestParam(value = "unModifiedPhNo", required = false) BigInteger unModifiedPhNo,
			@RequestParam(value = "employeeName", required = true) String employeeName,
			@RequestParam(value = "phoneNumber", required = true) BigInteger phoneNumber,
			@RequestParam(value = "permanentAddress", required = true) String permanentAddress,
			@RequestParam(value = "presentAddress", required = true) String presentAddress,
			@RequestParam(value = "joiningDate", required = false) Date joiningDate,
			@RequestParam(value = "emailId", required = false) String emailId, HttpServletRequest request) {
		EmployeeMaster employeeData = new EmployeeMaster();
		try {
			Binary addressProofBinary = null;
			if (addressProof != null) {
				addressProofBinary = new Binary(BsonBinarySubType.BINARY, addressProof.getBytes());
			}
			employeeData = new EmployeeMaster("", employeeName, phoneNumber, presentAddress, permanentAddress,
					joiningDate, emailId, fileName, addressProofBinary);
		} catch (IOException e) {
			e.printStackTrace();
		}
		boolean createResponse = pplMgmtService.saveNewEmployee(employeeData, unModifiedPhNo, request.getRemoteUser());
		return new ResponseEntity<>(createResponse, HttpStatus.OK);
	}

	@ApiOperation(value = "API to edit Employee in portal", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully updated customer details"),
			@ApiResponse(code = 500, message = "Exception occurred while Adding/Updating details") })
	@PostMapping(path = "/editEmployee", produces = "application/json")
	public ResponseEntity<?> editEmployee(@RequestBody EmployeeMaster employee, HttpServletRequest request) {
		boolean editResponse = pplMgmtService.editEmployee(employee, request.getRemoteUser());
		return new ResponseEntity<>(editResponse, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the phone number availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = "/getFilteredCustomer", produces = "application/json")
	public ResponseEntity<?> getFilteredCustomer(@RequestParam("searchParam") String searchParam,
			HttpServletRequest request) {
		if (searchParam.matches("[0-9].*")) {
			Set<BigInteger> phoneNumbers = pplMgmtService.getCustomerPhoneNum(searchParam);
			return new ResponseEntity<>(phoneNumbers, HttpStatus.OK);
		} else if (searchParam.matches("[s S][0-9].*")) {
			Set<String> customerNames = pplMgmtService.getCustomerId(searchParam);
			return new ResponseEntity<>(customerNames, HttpStatus.OK);
		} else {
			Set<String> customerNames = pplMgmtService.getCustomerId(searchParam);
			return new ResponseEntity<>(customerNames, HttpStatus.OK);
		}
	}

	@ApiOperation(value = "API to get the phone number availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = "/getCustomerInfo", produces = "application/json")
	public ResponseEntity<?> getCustomerInfo(@RequestParam("searchParam") String searchParam,
			HttpServletRequest request) {
		CustomerMaster customerDetail = pplMgmtService.getCustomerDetail(searchParam);
		return new ResponseEntity<>(customerDetail, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the customer analytics", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = "/getCustomerAnalytics", produces = "application/json")
	public ResponseEntity<?> getCustomerAnalytics(@RequestBody CustomerAnalyticsPayLoad payLoad,
			HttpServletRequest request) {
		BillAnalyticsData anaylticsData = pplMgmtService.getCustomerAnalyticsData(payLoad.getCustomerCode(),
				payLoad.getSearchParam(), payLoad.getSearchStartDate(), payLoad.getSearchEndDate());
		return new ResponseEntity<>(anaylticsData, HttpStatus.OK);
	}
	
	@ApiOperation(value = "API to get customer product trend data", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = "/getCustomerTrendData", produces = "application/json")
	public ResponseEntity<?> getCustomerTrendData(@RequestBody CustomerAnalyticsPayLoad payLoad,
			HttpServletRequest request) {
		List<TopSellingTrendData> sellingTrendData = pplMgmtService.getCustomerTrendData(payLoad.getCustomerCode(),
				payLoad.getSearchParam(), payLoad.getSearchStartDate(), payLoad.getSearchEndDate());
		return new ResponseEntity<>(sellingTrendData, HttpStatus.OK);
	}
	
	@ApiOperation(value = "API to get the gross trend data", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = "/getCustomerGrossTrendData", produces = "application/json")
	public ResponseEntity<?> getCustomerGrossTrendData(@RequestBody CustomerAnalyticsPayLoad payLoad,
			HttpServletRequest request) {
		List<TopGrossTrendData> grossTrendData = pplMgmtService.getCustomerGrossTrendData(payLoad.getCustomerCode(),
				payLoad.getSearchParam(), payLoad.getSearchStartDate(), payLoad.getSearchEndDate());
		return new ResponseEntity<>(grossTrendData, HttpStatus.OK);
	}
}
