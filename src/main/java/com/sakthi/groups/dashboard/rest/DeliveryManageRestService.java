package com.sakthi.groups.dashboard.rest;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sakthi.groups.dashboard.model.BataDeliveryData;
import com.sakthi.groups.dashboard.model.BatchDeliveryDetails;
import com.sakthi.groups.dashboard.service.DeliveryManageService;
import com.sakthi.groups.migration.portal.model.DeliveryOrderDetail;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/rest/deliverymanage")
public class DeliveryManageRestService {

	@Autowired
	private DeliveryManageService deliveryService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@ApiOperation(value = "API to get the delivery order availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = "/getDeliveryOrderDetail", produces = "application/json")
	public ResponseEntity<?> getdeliveryOrderDetails(HttpServletRequest request) {
		List<DeliveryOrderDetail> deliveryOrderDetail = deliveryService.getdeliveryOrders();
		return new ResponseEntity<>(deliveryOrderDetail, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the prending delivery order availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = "/getPendingOrderDetail", produces = "application/json")
	public ResponseEntity<?> getPendingOrderDetails(HttpServletRequest request) {
		logger.info("Invoking getPendingOrderDetails");
		List<DeliveryOrderDetail> deliveryOrderDetail = deliveryService.getPendingdeliveryOrders();
		return new ResponseEntity<>(deliveryOrderDetail, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the phone number availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = "/getFilteredPhoneNumbers", produces = "application/json")
	public ResponseEntity<?> getFilteredPhoneNumbers(@RequestParam("phoneNumber") String phoneNumber,
			HttpServletRequest request) {
		Set<String> phoneNumbers = deliveryService.getPhoneNumbers(phoneNumber);
		return new ResponseEntity<>(phoneNumbers, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the phone number availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = "/getPhoneNumbers", produces = "application/json")
	public ResponseEntity<?> getPhoneNumbers(HttpServletRequest request) {
		Set<String> phoneNumbers = deliveryService.getPhoneNumbers();
		return new ResponseEntity<>(phoneNumbers, HttpStatus.OK);
	}

	@ApiOperation(value = "API to add or update delivery entry details", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Added or Updated delivery entry"),
			@ApiResponse(code = 500, message = "Exception occurred while Adding/Updating details") })
	@PostMapping(path = "/addDeliveryOrder", produces = "application/json")
	public ResponseEntity<?> addDeliveryOrder(@RequestBody List<DeliveryOrderDetail> deliveryOrderList,
			HttpServletRequest request) {
		boolean deliveryOrderResponse = deliveryService.addDeliveryOrderDetail(deliveryOrderList);
		return new ResponseEntity<>(deliveryOrderResponse, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the customer info availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@PostMapping(path = { "/getCustomerInfo" }, produces = "application/json")
	public ResponseEntity<?> getCustomerInfo(@RequestParam("phoneNumber") String phoneNumber,
			HttpServletRequest request) {
		try {
			phoneNumber = URLDecoder.decode(phoneNumber, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("Unsupported Encoding {}", e.getMessage());
		}
		HashMap<String, String> customerInfo = deliveryService.getCustomerInfo(phoneNumber);
		return new ResponseEntity<>(customerInfo, HttpStatus.OK);
	}

	@ApiOperation(value = "API to check delivery order in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved the details"),
			@ApiResponse(code = 500, message = "Exception occurred while storing details") })
	@PostMapping(path = "/checkDeliveryOrder", produces = "application/json")
	public ResponseEntity<?> checkDeliveryOrder(@RequestBody DeliveryOrderDetail deliveryOrder,
			HttpServletRequest request) {
		boolean returnStatus = deliveryService.checkDeliveryOrder(deliveryOrder.getPhoneNumber(),
				deliveryOrder.getBillNo());
		return new ResponseEntity<>(returnStatus, HttpStatus.OK);
	}

	@ApiOperation(value = "API to check delivery order in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved the details"),
			@ApiResponse(code = 500, message = "Exception occurred while storing details") })
	@PostMapping(path = "/checkSimillarDeliveryOrder", produces = "application/json")
	public ResponseEntity<?> checkSimilarDeliveryOrder(@RequestBody DeliveryOrderDetail deliveryOrder,
			HttpServletRequest request) {
		String returnStatus = deliveryService.checkSimilarDeliveryOrder(deliveryOrder.getPhoneNumber());
		return new ResponseEntity<>("{ \"response\": \"" + returnStatus + "\"}", HttpStatus.OK);
	}

	@ApiOperation(value = "API to update delivery order details in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully updated the details"),
			@ApiResponse(code = 500, message = "Exception occurred while storing details") })
	@PostMapping(path = "/updateDeliveryOrder", produces = "application/json")
	public ResponseEntity<?> updateDeliveryOrder(@RequestBody DeliveryOrderDetail deliveryOrder,
			HttpServletRequest request) {
		boolean returnStatus = deliveryService.updateDeliveryOrder(deliveryOrder);
		return new ResponseEntity<>(returnStatus, HttpStatus.OK);
	}

	@ApiOperation(value = "API to update delivery order details in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully updated the details"),
			@ApiResponse(code = 500, message = "Exception occurred while storing details") })
	@PostMapping(path = "/updateDeliveryOrderList", produces = "application/json")
	public ResponseEntity<?> updateDeliveryOrderList(@RequestBody List<DeliveryOrderDetail> deliveryOrder,
			HttpServletRequest request) {
		boolean returnStatus = deliveryService.updateDeliveryOrderList(deliveryOrder, null);
		return new ResponseEntity<>(returnStatus, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the batch delivery order availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = "/getBatchOrderDetail", produces = "application/json")
	public ResponseEntity<?> getBatchOrderDetail(HttpServletRequest request) {
		List<BatchDeliveryDetails> batchDeliveryDetail = deliveryService.getBatchdeliveryOrders();
		return new ResponseEntity<>(batchDeliveryDetail, HttpStatus.OK);
	}

	@ApiOperation(value = "API to update delivery order details in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully updated the details"),
			@ApiResponse(code = 500, message = "Exception occurred while storing details") })
	@PostMapping(path = "/batchDeliveryOrder", produces = "application/json")
	public ResponseEntity<?> batchDeliveryOrder(@RequestParam("denominationTotal") Double denominationTotal,
			@RequestBody BataDeliveryData batchDeliveryData, HttpServletRequest request) {
		boolean returnStatus = deliveryService.updatebatchDeliveryOrder(batchDeliveryData.getDeliveryDetail(),
				denominationTotal, batchDeliveryData.getChangesGiven());
		return new ResponseEntity<>(returnStatus, HttpStatus.OK);
	}

	@ApiOperation(value = "API to move the batch status to close", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully updated the details"),
			@ApiResponse(code = 500, message = "Exception occurred while storing details") })
	@PostMapping(path = "/closeDeliveryBatch", produces = "application/json")
	public ResponseEntity<?> closeDeliveryBatch(@RequestParam("batchId") String batchId, HttpServletRequest request) {
		boolean returnStatus = deliveryService.closeDeliveryBatch(UUID.fromString(batchId));
		return new ResponseEntity<>(returnStatus, HttpStatus.OK);
	}

	@ApiOperation(value = "API to get the batch delivery order availabe in DB", response = ResponseEntity.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Retrieved the details successfully"),
			@ApiResponse(code = 500, message = "Exception occurred while retrieving details") })
	@GetMapping(path = "/getBatchDeliveryOrders", produces = "application/json")
	public ResponseEntity<?> getBatchDeliveryOrders(HttpServletRequest request, @RequestParam("batchId") UUID batchId) {
		List<DeliveryOrderDetail> deliveryOrderDetail = deliveryService.getBatchDeliveryOrders(batchId);
		return new ResponseEntity<>(deliveryOrderDetail, HttpStatus.OK);
	}
}
