package com.sakthi.groups.dashboard.model;

public class BatchChangeProvided {

	private Integer denomination;
	private Integer denominationValue;
	private Integer denominationAmount;

	public Integer getDenomination() {
		return denomination;
	}

	public void setDenomination(Integer denomination) {
		this.denomination = denomination;
	}

	public Integer getDenominationValue() {
		return denominationValue;
	}

	public void setDenominationValue(Integer denominationValue) {
		this.denominationValue = denominationValue;
	}

	public Integer getDenominationAmount() {
		return denominationAmount;
	}

	public void setDenominationAmount(Integer denominationAmount) {
		this.denominationAmount = denominationAmount;
	}

}
