package com.sakthi.groups.dashboard.model.chart;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "label", "pattern", "type" })
public class ColModel {

	public ColModel() {
		super();
	}

	public ColModel(String id, String label, String pattern, String type) {
		super();
		this.id = id;
		this.label = label;
		this.pattern = pattern;
		this.type = type;
	}

	@JsonProperty("id")
	private String id;
	@JsonProperty("label")
	private String label;
	@JsonProperty("pattern")
	private String pattern;
	@JsonProperty("type")
	private String type;

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("label")
	public String getLabel() {
		return label;
	}

	@JsonProperty("label")
	public void setLabel(String label) {
		this.label = label;
	}

	@JsonProperty("pattern")
	public String getPattern() {
		return pattern;
	}

	@JsonProperty("pattern")
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}
}
