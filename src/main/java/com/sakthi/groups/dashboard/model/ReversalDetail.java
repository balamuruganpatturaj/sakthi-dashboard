package com.sakthi.groups.dashboard.model;

import java.math.BigDecimal;

public class ReversalDetail {

	private String cardType;
	private String cardNumber;
	private BigDecimal reversalAmount;
	private boolean amountRevered;
	private boolean amountRefunded;

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public BigDecimal getReversalAmount() {
		return reversalAmount;
	}

	public void setReversalAmount(BigDecimal reversalAmount) {
		this.reversalAmount = reversalAmount;
	}

	public boolean isAmountRevered() {
		return amountRevered;
	}

	public void setAmountRevered(boolean amountRevered) {
		this.amountRevered = amountRevered;
	}

	public boolean isAmountRefunded() {
		return amountRefunded;
	}

	public void setAmountRefunded(boolean amountRefunded) {
		this.amountRefunded = amountRefunded;
	}

}
