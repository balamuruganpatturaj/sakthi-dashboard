package com.sakthi.groups.dashboard.model;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "batch_delivery_details")
public class BatchDeliveryDetails {

	public BatchDeliveryDetails(UUID batchNumber, Date deliveryDate, Integer totalOrders,
			Double totalBillAmount, Double changesGiven, Double paidBillAmount, Double totalAmount, String batchStatus,
			Double balanceAmount, String deliveryNotes, String deliveryPerson,
			List<BatchDeliveryOrder> batchDeliveryList, List<BatchChangeProvided> changesProvided) {
		super();
		this.batchNumber = batchNumber;
		this.deliveryDate = deliveryDate;
		this.totalOrders = totalOrders;
		this.totalBillAmount = totalBillAmount;
		this.changesGiven = changesGiven;
		this.paidBillAmount = paidBillAmount;
		this.totalAmount = totalAmount;
		this.batchStatus = batchStatus;
		this.balanceAmount = balanceAmount;
		this.deliveryNotes = deliveryNotes;
		this.deliveryPerson = deliveryPerson;
		this.batchDeliveryList = batchDeliveryList;
		this.changesProvided = changesProvided;
	}

	public BatchDeliveryDetails() {

	}

	@Id
	private ObjectId id;
	@Indexed
	private UUID batchNumber;
	private Date deliveryDate;
	private Integer totalOrders;
	private Double totalBillAmount;
	private Double changesGiven;
	private Double paidBillAmount;
	private Double totalAmount;
	private String batchStatus;
	private Double balanceAmount;
	private String deliveryNotes;
	private String deliveryPerson;
	private List<BatchDeliveryOrder> batchDeliveryList;
	private List<BatchChangeProvided> changesProvided;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public UUID getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(UUID batchNumber) {
		this.batchNumber = batchNumber;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Integer getTotalOrders() {
		return totalOrders;
	}

	public void setTotalOrders(Integer totalOrders) {
		this.totalOrders = totalOrders;
	}

	public Double getTotalBillAmount() {
		return totalBillAmount;
	}

	public void setTotalBillAmount(Double totalBillAmount) {
		this.totalBillAmount = totalBillAmount;
	}

	public Double getChangesGiven() {
		return changesGiven;
	}

	public void setChangesGiven(Double changesGiven) {
		this.changesGiven = changesGiven;
	}

	public Double getPaidBillAmount() {
		return paidBillAmount;
	}

	public void setPaidBillAmount(Double paidBillAmount) {
		this.paidBillAmount = paidBillAmount;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getBatchStatus() {
		return batchStatus;
	}

	public void setBatchStatus(String batchStatus) {
		this.batchStatus = batchStatus;
	}

	public Double getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(Double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public String getDeliveryNotes() {
		return deliveryNotes;
	}

	public void setDeliveryNotes(String deliveryNotes) {
		this.deliveryNotes = deliveryNotes;
	}

	public String getDeliveryPerson() {
		return deliveryPerson;
	}

	public void setDeliveryPerson(String deliveryPerson) {
		this.deliveryPerson = deliveryPerson;
	}

	public List<BatchDeliveryOrder> getBatchDeliveryList() {
		return batchDeliveryList;
	}

	public void setBatchDeliveryList(List<BatchDeliveryOrder> batchDeliveryList) {
		this.batchDeliveryList = batchDeliveryList;
	}

	public List<BatchChangeProvided> getChangesProvided() {
		return changesProvided;
	}

	public void setChangesProvided(List<BatchChangeProvided> changesProvided) {
		this.changesProvided = changesProvided;
	}

}
