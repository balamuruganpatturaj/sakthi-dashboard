package com.sakthi.groups.dashboard.model;

import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PurchasePriceList {

	@Field("net_rate")
	@JsonProperty("netRate")
	private Double netRate;
	
	@Field("quantity")
	@JsonProperty("quantity")
	private String quantity;

	public Double getNetRate() {
		return netRate;
	}

	public void setNetRate(Double netRate) {
		this.netRate = netRate;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
}
