package com.sakthi.groups.dashboard.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "chennai_purchase")
public class ChennaiPurchase {

	@Id
	private ObjectId id;
	private String productName;
	private Date purchaseDate;
	private Double maxRetailPrice;
	private Integer unitPerCase;
	private Integer quantity;
	private String quantityValue;
	private String eancode;
	private Integer itemCode;
	private Double piecePrice;
	private Double kgPrice;
	private Double dozenPrice;
	private Double litrePrice;
	private Double boxPrice;
	private Double saramPrice;
	private Double bundlePrice;
	private Double bagPrice;
	private Double attaiPrice;
	private String remarks;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public Double getMaxRetailPrice() {
		return maxRetailPrice;
	}

	public void setMaxRetailPrice(Double maxRetailPrice) {
		this.maxRetailPrice = maxRetailPrice;
	}

	public Integer getUnitPerCase() {
		return unitPerCase;
	}

	public void setUnitPerCase(Integer unitPerCase) {
		this.unitPerCase = unitPerCase;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getEancode() {
		return eancode;
	}

	public void setEancode(String eancode) {
		this.eancode = eancode;
	}

	public Integer getItemCode() {
		return itemCode;
	}

	public void setItemCode(Integer itemCode) {
		this.itemCode = itemCode;
	}

	public Double getPiecePrice() {
		return piecePrice;
	}

	public void setPiecePrice(Double piecePrice) {
		this.piecePrice = piecePrice;
	}

	public Double getKgPrice() {
		return kgPrice;
	}

	public void setKgPrice(Double kgPrice) {
		this.kgPrice = kgPrice;
	}

	public Double getDozenPrice() {
		return dozenPrice;
	}

	public void setDozenPrice(Double dozenPrice) {
		this.dozenPrice = dozenPrice;
	}

	public Double getLitrePrice() {
		return litrePrice;
	}

	public void setLitrePrice(Double litrePrice) {
		this.litrePrice = litrePrice;
	}

	public Double getBoxPrice() {
		return boxPrice;
	}

	public void setBoxPrice(Double boxPrice) {
		this.boxPrice = boxPrice;
	}

	public Double getSaramPrice() {
		return saramPrice;
	}

	public void setSaramPrice(Double saramPrice) {
		this.saramPrice = saramPrice;
	}

	public Double getBundlePrice() {
		return bundlePrice;
	}

	public void setBundlePrice(Double bundlePrice) {
		this.bundlePrice = bundlePrice;
	}

	public Double getBagPrice() {
		return bagPrice;
	}

	public void setBagPrice(Double bagPrice) {
		this.bagPrice = bagPrice;
	}

	public Double getAttaiPrice() {
		return attaiPrice;
	}

	public void setAttaiPrice(Double attaiPrice) {
		this.attaiPrice = attaiPrice;
	}

	public String getQuantityValue() {
		return quantityValue;
	}

	public void setQuantityValue(String quantityValue) {
		this.quantityValue = quantityValue;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
