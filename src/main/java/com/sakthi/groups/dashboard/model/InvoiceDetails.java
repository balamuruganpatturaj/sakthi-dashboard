package com.sakthi.groups.dashboard.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "invoice_details")
@CompoundIndexes({
		@CompoundIndex(name = "invoice_unique_idx", unique = true, def = "{'supplierName' : 1, 'invoiceNo' : 1, 'invoiceDate' : 1}") })
public class InvoiceDetails {

	@Id
	private ObjectId id;
	private String supplierName;
	private String invoiceNo;
	private Date invoiceDate;
	private Float invoiceAmt;
	private String invoiceRemarks;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Float getInvoiceAmt() {
		return invoiceAmt;
	}

	public void setInvoiceAmt(Float invoiceAmt) {
		this.invoiceAmt = invoiceAmt;
	}

	public String getInvoiceRemarks() {
		return invoiceRemarks;
	}

	public void setInvoiceRemarks(String invoiceRemarks) {
		this.invoiceRemarks = invoiceRemarks;
	}

}
