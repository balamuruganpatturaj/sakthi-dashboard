package com.sakthi.groups.dashboard.model;

public class GoFrugalPurchaseMapping {

	private Integer itemCode;
	private String mappedField;

	public GoFrugalPurchaseMapping() {
	}

	public Integer getItemCode() {
		return itemCode;
	}

	public void setItemCode(Integer itemCode) {
		this.itemCode = itemCode;
	}

	public String getMappedField() {
		return mappedField;
	}

	public void setMappedField(String mappedField) {
		this.mappedField = mappedField;
	}

}
