package com.sakthi.groups.dashboard.model;

import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "customer_master")
public class CustomerMaster {

	@Id
	private ObjectId id;
	private String customerId;
	@Indexed(unique = true)
	private Integer customerCode;
	private String customerName;
	private String customerAddress;
	private Integer pincode;
	private String customerArea;
	private List<String> customerPhoneNumber;
	private String customerEmailId;
	private boolean creditAllowed;
	private Float creditBalance;
	private Float creditLimit;
	private boolean customerStatus;
	private Float customerDiscount;
	private String customerPriceLevel;
	private boolean loyaltyAllowed;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	private String gstCompanyName;
	private String gstNumber;
	private String panNumber;
	private Integer aadharNumber;
	private List<String> contactPerson;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public Integer getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(Integer customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public Integer getPincode() {
		return pincode;
	}

	public void setPincode(Integer pincode) {
		this.pincode = pincode;
	}

	public String getCustomerArea() {
		return customerArea;
	}

	public void setCustomerArea(String customerArea) {
		this.customerArea = customerArea;
	}

	public List<String> getCustomerPhoneNumber() {
		return customerPhoneNumber;
	}

	public void setCustomerPhoneNumber(List<String> customerPhoneNumber) {
		this.customerPhoneNumber = customerPhoneNumber;
	}

	public String getCustomerEmailId() {
		return customerEmailId;
	}

	public void setCustomerEmailId(String customerEmailId) {
		this.customerEmailId = customerEmailId;
	}

	public boolean isCreditAllowed() {
		return creditAllowed;
	}

	public void setCreditAllowed(boolean creditAllowed) {
		this.creditAllowed = creditAllowed;
	}

	public Float getCreditBalance() {
		return creditBalance;
	}

	public void setCreditBalance(Float creditBalance) {
		this.creditBalance = creditBalance;
	}

	public Float getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(Float creditLimit) {
		this.creditLimit = creditLimit;
	}

	public boolean isCustomerStatus() {
		return customerStatus;
	}

	public void setCustomerStatus(boolean customerStatus) {
		this.customerStatus = customerStatus;
	}

	public Float getCustomerDiscount() {
		return customerDiscount;
	}

	public void setCustomerDiscount(Float customerDiscount) {
		this.customerDiscount = customerDiscount;
	}

	public String getCustomerPriceLevel() {
		return customerPriceLevel;
	}

	public void setCustomerPriceLevel(String customerPriceLevel) {
		this.customerPriceLevel = customerPriceLevel;
	}

	public boolean isLoyaltyAllowed() {
		return loyaltyAllowed;
	}

	public void setLoyaltyAllowed(boolean loyaltyAllowed) {
		this.loyaltyAllowed = loyaltyAllowed;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date date) {
		this.createdDate = date;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getGstCompanyName() {
		return gstCompanyName;
	}

	public void setGstCompanyName(String gstCompanyName) {
		this.gstCompanyName = gstCompanyName;
	}

	public String getGstNumber() {
		return gstNumber;
	}

	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public Integer getAadharNumber() {
		return aadharNumber;
	}

	public void setAadharNumber(Integer aadharNumber) {
		this.aadharNumber = aadharNumber;
	}

	public List<String> getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(List<String> contactPerson) {
		this.contactPerson = contactPerson;
	}

}
