package com.sakthi.groups.dashboard.model.chart;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "v", "f" })
public class CModel {

	public CModel() {
		super();
	}

	public CModel(Object v, Object f) {
		super();
		this.v = v;
		this.f = f;
	}

	@JsonProperty("v")
	private Object v;
	@JsonProperty("f")
	private Object f;

	@JsonProperty("v")
	public Object getV() {
		return v;
	}

	@JsonProperty("v")
	public void setV(Object v) {
		this.v = v;
	}

	@JsonProperty("f")
	public Object getF() {
		return f;
	}

	@JsonProperty("f")
	public void setF(Object f) {
		this.f = f;
	}

	@Override
	public String toString() {
		return "CModel [v=" + v + ", f=" + f + "]";
	}

}
