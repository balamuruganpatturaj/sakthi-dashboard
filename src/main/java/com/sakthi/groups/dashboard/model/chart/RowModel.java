package com.sakthi.groups.dashboard.model.chart;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "c" })
public class RowModel {

	public RowModel() {
		super();
	}

	public RowModel(List<CModel> c) {
		super();
		this.c = c;
	}

	@JsonProperty("c")
	private List<CModel> c = null;

	@JsonProperty("c")
	public List<CModel> getC() {
		return c;
	}

	@JsonProperty("c")
	public void setC(List<CModel> c) {
		this.c = c;
	}
}
