package com.sakthi.groups.dashboard.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "latest_purchase_history")
public class LatestPurchaseHistory {

	@Id
	private ObjectId id;
	private String productName;
	private String productCategory;
	private String productAlias;
	private boolean productStatus;
	private Date purchaseDate;
	private Double maxRetailPrice;
	private Double piecePrice;
	private Double kgPrice;
	private Double dozenPrice;
	private Double saramPrice;
	private Double boxPrice;
	private Double bagPrice;
	private Double litrePrice;
	private Double bundlePrice;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public String getProductAlias() {
		return productAlias;
	}

	public void setProductAlias(String productAlias) {
		this.productAlias = productAlias;
	}

	public boolean isProductStatus() {
		return productStatus;
	}

	public void setProductStatus(boolean productStatus) {
		this.productStatus = productStatus;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public Double getMaxRetailPrice() {
		return maxRetailPrice;
	}

	public void setMaxRetailPrice(Double maxRetailPrice) {
		this.maxRetailPrice = maxRetailPrice;
	}

	public Double getPiecePrice() {
		return piecePrice;
	}

	public void setPiecePrice(Double piecePrice) {
		this.piecePrice = piecePrice;
	}

	public Double getKgPrice() {
		return kgPrice;
	}

	public void setKgPrice(Double kgPrice) {
		this.kgPrice = kgPrice;
	}

	public Double getDozenPrice() {
		return dozenPrice;
	}

	public void setDozenPrice(Double dozenPrice) {
		this.dozenPrice = dozenPrice;
	}

	public Double getSaramPrice() {
		return saramPrice;
	}

	public void setSaramPrice(Double saramPrice) {
		this.saramPrice = saramPrice;
	}

	public Double getBoxPrice() {
		return boxPrice;
	}

	public void setBoxPrice(Double boxPrice) {
		this.boxPrice = boxPrice;
	}

	public Double getBagPrice() {
		return bagPrice;
	}

	public void setBagPrice(Double bagPrice) {
		this.bagPrice = bagPrice;
	}

	public Double getLitrePrice() {
		return litrePrice;
	}

	public void setLitrePrice(Double litrePrice) {
		this.litrePrice = litrePrice;
	}

	public Double getBundlePrice() {
		return bundlePrice;
	}

	public void setBundlePrice(Double bundlePrice) {
		this.bundlePrice = bundlePrice;
	}
}
