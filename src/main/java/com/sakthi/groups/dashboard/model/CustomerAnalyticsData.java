package com.sakthi.groups.dashboard.model;

public class CustomerAnalyticsData {

	public CustomerAnalyticsData(Integer noOfBills, Double totalAmount, Double profitPercent, Integer totalQuantity) {
		super();
		this.noOfBills = noOfBills;
		this.totalAmount = totalAmount;
		this.profitPercent = profitPercent;
		this.totalQuantity = totalQuantity;
	}

	private Integer noOfBills;
	private Double totalAmount;
	private Double profitPercent;
	private Integer totalQuantity;

	public CustomerAnalyticsData() {
	}

	public Integer getNoOfBills() {
		return noOfBills;
	}

	public void setNoOfBills(Integer noOfBills) {
		this.noOfBills = noOfBills;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getProfitPercent() {
		return profitPercent;
	}

	public void setProfitPercent(Double profitPercent) {
		this.profitPercent = profitPercent;
	}

	public Integer getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(Integer totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

}
