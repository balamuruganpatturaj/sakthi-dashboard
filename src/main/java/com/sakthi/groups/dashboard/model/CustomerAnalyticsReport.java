package com.sakthi.groups.dashboard.model;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.sakthi.groups.migration.portal.model.BillAnalyticsData;
import com.sakthi.groups.migration.portal.model.BillSearchCriteria;

@Document(collection = "customer_analytics_report")
public class CustomerAnalyticsReport {

	public CustomerAnalyticsReport() {
		super();
	}

	public CustomerAnalyticsReport(BillSearchCriteria searchCriteria, List<BillAnalyticsData> analyticsDataList) {
		super();
		this.searchCriteria = searchCriteria;
		this.analyticsDataList = analyticsDataList;
	}

	@Id
	private ObjectId id;
	private BillSearchCriteria searchCriteria;
	private List<BillAnalyticsData> analyticsDataList;

	public BillSearchCriteria getSearchCriteria() {
		return searchCriteria;
	}

	public void setSearchCriteria(BillSearchCriteria searchCriteria) {
		this.searchCriteria = searchCriteria;
	}

	public List<BillAnalyticsData> getAnalyticsDataList() {
		return analyticsDataList;
	}

	public void setAnalyticsDataList(List<BillAnalyticsData> analyticsDataList) {
		this.analyticsDataList = analyticsDataList;
	}
}
