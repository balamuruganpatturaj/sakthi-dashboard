package com.sakthi.groups.dashboard.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.bson.types.Binary;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "reversal_report")
public class ReversalReport {

	public ReversalReport() {
		super();
	}

	public ReversalReport(BigInteger tidNo, String assignedParty, Date txnDate, String cardType, String cardNumber,
			BigDecimal reversalAmount, boolean amountRevered, boolean amountRefunded) {
		super();
		this.tidNo = tidNo;
		this.assignedParty = assignedParty;
		this.txnDate = txnDate;
		this.cardType = cardType;
		this.cardNumber = cardNumber;
		this.reversalAmount = reversalAmount;
		this.amountRevered = amountRevered;
		this.amountRefunded = amountRefunded;
	}

	public ReversalReport(BigInteger tidNo, String assignedParty, Date txnDate, String cardType,
			String cardNumber, BigDecimal reversalAmount, boolean amountRevered, boolean amountRefunded,
			Binary letterProof, String letterProofName, Binary identityProof, String identityProofName) {
		super();
		this.tidNo = tidNo;
		this.assignedParty = assignedParty;
		this.txnDate = txnDate;
		this.cardType = cardType;
		this.cardNumber = cardNumber;
		this.reversalAmount = reversalAmount;
		this.amountRevered = amountRevered;
		this.amountRefunded = amountRefunded;
		this.letterProof = letterProof;
		this.letterProofName = letterProofName;
		this.identityProof = identityProof;
		this.identityProofName = identityProofName;
	}

	@Id
	private ObjectId id;
	private BigInteger tidNo;
	private String assignedParty;
	private Date txnDate;
	private String cardType;
	private String cardNumber;
	private BigDecimal reversalAmount;
	private boolean amountRevered;
	private boolean amountRefunded;
	private Binary letterProof;
	private String letterProofName;
	private Binary identityProof;
	private String identityProofName;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public BigInteger getTidNo() {
		return tidNo;
	}

	public void setTidNo(BigInteger tidNo) {
		this.tidNo = tidNo;
	}

	public String getAssignedParty() {
		return assignedParty;
	}

	public void setAssignedParty(String assignedParty) {
		this.assignedParty = assignedParty;
	}

	public Date getTxnDate() {
		return txnDate;
	}

	public void setTxnDate(Date txnDate) {
		this.txnDate = txnDate;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public BigDecimal getReversalAmount() {
		return reversalAmount;
	}

	public void setReversalAmount(BigDecimal reversalAmount) {
		this.reversalAmount = reversalAmount;
	}

	public boolean isAmountRevered() {
		return amountRevered;
	}

	public void setAmountRevered(boolean amountRevered) {
		this.amountRevered = amountRevered;
	}

	public boolean isAmountRefunded() {
		return amountRefunded;
	}

	public void setAmountRefunded(boolean amountRefunded) {
		this.amountRefunded = amountRefunded;
	}

	public Binary getLetterProof() {
		return letterProof;
	}

	public void setLetterProof(Binary letterProof) {
		this.letterProof = letterProof;
	}

	public String getLetterProofName() {
		return letterProofName;
	}

	public void setLetterProofName(String letterProofName) {
		this.letterProofName = letterProofName;
	}

	public Binary getIdentityProof() {
		return identityProof;
	}

	public void setIdentityProof(Binary identityProof) {
		this.identityProof = identityProof;
	}

	public String getIdentityProofName() {
		return identityProofName;
	}

	public void setIdentityProofName(String identityProofName) {
		this.identityProofName = identityProofName;
	}

}
