package com.sakthi.groups.dashboard.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.Set;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "settlement_details")
public class SettlementDetail {

	@Id
	private ObjectId id;
	private BigInteger tidNo;
	private String bankName;
	private String assignedParty;
	private Date txnDate;
	private BigInteger totalTxn;
	private BigDecimal txnAmount;
	private BigDecimal totalAmount;
	private BigDecimal settledAmount;
	private BigDecimal differenceAmt;
	private Set<ReversalDetail> addReversal;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public BigInteger getTidNo() {
		return tidNo;
	}

	public void setTidNo(BigInteger tidNo) {
		this.tidNo = tidNo;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAssignedParty() {
		return assignedParty;
	}

	public void setAssignedParty(String assignedParty) {
		this.assignedParty = assignedParty;
	}

	public Date getTxnDate() {
		return txnDate;
	}

	public void setTxnDate(Date txnDate) {
		this.txnDate = txnDate;
	}

	public BigInteger getTotalTxn() {
		return totalTxn;
	}

	public void setTotalTxn(BigInteger totalTxn) {
		this.totalTxn = totalTxn;
	}

	public BigDecimal getTxnAmount() {
		return txnAmount;
	}

	public void setTxnAmount(BigDecimal txnAmount) {
		this.txnAmount = txnAmount;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getSettledAmount() {
		return settledAmount;
	}

	public void setSettledAmount(BigDecimal settledAmount) {
		this.settledAmount = settledAmount;
	}

	public BigDecimal getDifferenceAmt() {
		return differenceAmt;
	}

	public void setDifferenceAmt(BigDecimal differenceAmt) {
		this.differenceAmt = differenceAmt;
	}

	public Set<ReversalDetail> getAddReversal() {
		return addReversal;
	}

	public void setAddReversal(Set<ReversalDetail> addReversal) {
		this.addReversal = addReversal;
	}
}
