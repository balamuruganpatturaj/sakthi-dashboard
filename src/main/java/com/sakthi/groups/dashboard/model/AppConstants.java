package com.sakthi.groups.dashboard.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "application_constant")
public class AppConstants {

	@Id
	private ObjectId id;
	private String appConstants;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getAppConstants() {
		return appConstants;
	}

	public void setAppConstants(String appConstants) {
		this.appConstants = appConstants;
	}
}
