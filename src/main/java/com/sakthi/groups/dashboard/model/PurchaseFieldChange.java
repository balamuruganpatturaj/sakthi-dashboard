package com.sakthi.groups.dashboard.model;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "purchase_field_change_formula")
public class PurchaseFieldChange {

	public PurchaseFieldChange(ObjectId productObjId, List<PurchaseFieldFormula> pieceChangeFormula,
			List<PurchaseFieldFormula> kgChangeFormula, List<PurchaseFieldFormula> dozenChangeFormula,
			List<PurchaseFieldFormula> saramChangeFormula, List<PurchaseFieldFormula> boxChangeFormula,
			List<PurchaseFieldFormula> bagChangeFormula, List<PurchaseFieldFormula> litreChangeFormula,
			List<PurchaseFieldFormula> bundleChangeFormula) {
		super();
		this.productObjId = productObjId;
		this.pieceChangeFormula = pieceChangeFormula;
		this.kgChangeFormula = kgChangeFormula;
		this.dozenChangeFormula = dozenChangeFormula;
		this.saramChangeFormula = saramChangeFormula;
		this.boxChangeFormula = boxChangeFormula;
		this.bagChangeFormula = bagChangeFormula;
		this.litreChangeFormula = litreChangeFormula;
		this.bundleChangeFormula = bundleChangeFormula;
	}

	@Id
	private ObjectId id;
	private ObjectId productObjId;
	private List<PurchaseFieldFormula> pieceChangeFormula;
	private List<PurchaseFieldFormula> kgChangeFormula;
	private List<PurchaseFieldFormula> dozenChangeFormula;
	private List<PurchaseFieldFormula> saramChangeFormula;
	private List<PurchaseFieldFormula> boxChangeFormula;
	private List<PurchaseFieldFormula> bagChangeFormula;
	private List<PurchaseFieldFormula> litreChangeFormula;
	private List<PurchaseFieldFormula> bundleChangeFormula;

	public PurchaseFieldChange() {
	}

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public ObjectId getProductObjId() {
		return productObjId;
	}

	public void setProductObjId(ObjectId productObjId) {
		this.productObjId = productObjId;
	}

	public List<PurchaseFieldFormula> getPieceChangeFormula() {
		return pieceChangeFormula;
	}

	public void setPieceChangeFormula(List<PurchaseFieldFormula> pieceChangeFormula) {
		this.pieceChangeFormula = pieceChangeFormula;
	}

	public List<PurchaseFieldFormula> getKgChangeFormula() {
		return kgChangeFormula;
	}

	public void setKgChangeFormula(List<PurchaseFieldFormula> kgChangeFormula) {
		this.kgChangeFormula = kgChangeFormula;
	}

	public List<PurchaseFieldFormula> getDozenChangeFormula() {
		return dozenChangeFormula;
	}

	public void setDozenChangeFormula(List<PurchaseFieldFormula> dozenChangeFormula) {
		this.dozenChangeFormula = dozenChangeFormula;
	}

	public List<PurchaseFieldFormula> getSaramChangeFormula() {
		return saramChangeFormula;
	}

	public void setSaramChangeFormula(List<PurchaseFieldFormula> saramChangeFormula) {
		this.saramChangeFormula = saramChangeFormula;
	}

	public List<PurchaseFieldFormula> getBoxChangeFormula() {
		return boxChangeFormula;
	}

	public void setBoxChangeFormula(List<PurchaseFieldFormula> boxChangeFormula) {
		this.boxChangeFormula = boxChangeFormula;
	}

	public List<PurchaseFieldFormula> getBagChangeFormula() {
		return bagChangeFormula;
	}

	public void setBagChangeFormula(List<PurchaseFieldFormula> bagChangeFormula) {
		this.bagChangeFormula = bagChangeFormula;
	}

	public List<PurchaseFieldFormula> getLitreChangeFormula() {
		return litreChangeFormula;
	}

	public void setLitreChangeFormula(List<PurchaseFieldFormula> litreChangeFormula) {
		this.litreChangeFormula = litreChangeFormula;
	}

	public List<PurchaseFieldFormula> getBundleChangeFormula() {
		return bundleChangeFormula;
	}

	public void setBundleChangeFormula(List<PurchaseFieldFormula> bundleChangeFormula) {
		this.bundleChangeFormula = bundleChangeFormula;
	}

}
