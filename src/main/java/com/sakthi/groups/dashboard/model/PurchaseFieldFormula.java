package com.sakthi.groups.dashboard.model;

public class PurchaseFieldFormula {

	public PurchaseFieldFormula(String fieldToChange, String operator, Double quantity) {
		super();
		this.fieldToChange = fieldToChange;
		this.operator = operator;
		this.quantity = quantity;
	}

	private String fieldToChange;
	private String operator;
	private Double quantity;

	public PurchaseFieldFormula() {
	}

	public String getFieldToChange() {
		return fieldToChange;
	}

	public void setFieldToChange(String fieldToChange) {
		this.fieldToChange = fieldToChange;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

}
