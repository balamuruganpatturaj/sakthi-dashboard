package com.sakthi.groups.dashboard.model;

import java.math.BigInteger;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "edc_pos_detail")
public class EdcPosDetail {

	@Id
	private ObjectId id;
	@Indexed(unique = true)
	private BigInteger tidNo;
	private String bankName;
	private String bankBranch;
	private String ifscCode;
	private String midNo;
	private String assignedParty;
	private boolean status;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public BigInteger getTidNo() {
		return tidNo;
	}

	public void setTidNo(BigInteger tidNo) {
		this.tidNo = tidNo;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankBranch() {
		return bankBranch;
	}

	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getMidNo() {
		return midNo;
	}

	public void setMidNo(String midNo) {
		this.midNo = midNo;
	}

	public String getAssignedParty() {
		return assignedParty;
	}

	public void setAssignedParty(String assignedParty) {
		this.assignedParty = assignedParty;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

}
