package com.sakthi.groups.dashboard.model.chart;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "cols", "rows" })
public class GoogleChartModel {

	public GoogleChartModel() {
		super();
	}

	public GoogleChartModel(List<ColModel> cols, List<RowModel> rows) {
		super();
		this.cols = cols;
		this.rows = rows;
	}

	@JsonProperty("cols")
	private List<ColModel> cols = null;
	@JsonProperty("rows")
	private List<RowModel> rows = null;

	@JsonProperty("cols")
	public List<ColModel> getCols() {
		return cols;
	}

	@JsonProperty("cols")
	public void setCols(List<ColModel> cols) {
		this.cols = cols;
	}

	@JsonProperty("rows")
	public List<RowModel> getRows() {
		return rows;
	}

	@JsonProperty("rows")
	public void setRows(List<RowModel> rows) {
		this.rows = rows;
	}
}
