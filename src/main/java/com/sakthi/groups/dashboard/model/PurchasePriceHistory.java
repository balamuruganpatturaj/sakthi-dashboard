package com.sakthi.groups.dashboard.model;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PurchasePriceHistory implements Comparable<PurchasePriceHistory> {

	@Field("purchase_date")
	@JsonProperty("purchaseDate")
	private Date purchaseDate;

	@Field("price_list")
	@JsonProperty("purPriceList")
	private List<PurchasePriceList> purPriceList;

	@Field("max_retail_price")
	@JsonProperty("maxRetailPrice")
	private Double maxRetailPrice;

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public List<PurchasePriceList> getPurPriceList() {
		return purPriceList;
	}

	public void setPurPriceList(List<PurchasePriceList> purPriceList) {
		this.purPriceList = purPriceList;
	}

	public Double getMaxRetailPrice() {
		return maxRetailPrice;
	}

	public void setMaxRetailPrice(Double maxRetailPrice) {
		this.maxRetailPrice = maxRetailPrice;
	}

	@Override
	public int compareTo(PurchasePriceHistory hist) {
		return hist.purchaseDate.compareTo(this.purchaseDate);
	}
}
