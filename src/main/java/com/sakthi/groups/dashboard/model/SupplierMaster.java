package com.sakthi.groups.dashboard.model;

import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "supplier_master")
public class SupplierMaster {

	@Id
	private ObjectId id;
	private Integer supplierCode;
	@Indexed(unique = true)
	private String supplierName;
	private String supplierAddress;
	private Integer pincode;
	private List<String> phoneNo;
	private String emailId;
	private boolean status;
	private String locality;
	private List<String> contactPerson;
	private Float tradeDiscount;
	private String createdBy;
	private Date createdTime;
	private String updatedBy;
	private Date updatedTime;
	private String supplierGstNo;
	private String supplierPanNo;
	private String gstRegisterType;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public Integer getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierCode(Integer supplierCode) {
		this.supplierCode = supplierCode;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getSupplierAddress() {
		return supplierAddress;
	}

	public void setSupplierAddress(String supplierAddress) {
		this.supplierAddress = supplierAddress;
	}

	public Integer getPincode() {
		return pincode;
	}

	public void setPincode(Integer pincode) {
		this.pincode = pincode;
	}

	public List<String> getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(List<String> phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public List<String> getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(List<String> contactPerson) {
		this.contactPerson = contactPerson;
	}

	public Float getTradeDiscount() {
		return tradeDiscount;
	}

	public void setTradeDiscount(Float tradeDiscount) {
		this.tradeDiscount = tradeDiscount;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}

	public String getSupplierGstNo() {
		return supplierGstNo;
	}

	public void setSupplierGstNo(String supplierGstNo) {
		this.supplierGstNo = supplierGstNo;
	}

	public String getSupplierPanNo() {
		return supplierPanNo;
	}

	public void setSupplierPanNo(String supplierPanNo) {
		this.supplierPanNo = supplierPanNo;
	}

	public String getGstRegisterType() {
		return gstRegisterType;
	}

	public void setGstRegisterType(String gstRegisterType) {
		this.gstRegisterType = gstRegisterType;
	}
}
