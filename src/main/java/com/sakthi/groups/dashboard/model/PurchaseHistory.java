package com.sakthi.groups.dashboard.model;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonProperty;

@Document(collection = "purchase_history")
public class PurchaseHistory implements Comparable<PurchaseHistory> {

	@Id
	private ObjectId id;

	@Field("product_name")
	@JsonProperty("productName")
	private String productName;

	@Field("product_category")
	@JsonProperty("productCategory")
	private String productCategory;

	@Field("product_alias")
	@JsonProperty("productAlias")
	private String productAlias;

	@Field("product_status")
	@JsonProperty("productStatus")
	private boolean productStatus;

	@Field("pieceChangeFormula")
	@JsonProperty("pieceChangeFormula")
	private List<PurchaseFieldFormula> pieceChangeFormula;

	@Field("kgChangeFormula")
	@JsonProperty("kgChangeFormula")
	private List<PurchaseFieldFormula> kgChangeFormula;

	@Field("dozenChangeFormula")
	@JsonProperty("dozenChangeFormula")
	private List<PurchaseFieldFormula> dozenChangeFormula;

	@Field("saramChangeFormula")
	@JsonProperty("saramChangeFormula")
	private List<PurchaseFieldFormula> saramChangeFormula;

	@Field("boxChangeFormula")
	@JsonProperty("boxChangeFormula")
	private List<PurchaseFieldFormula> boxChangeFormula;

	@Field("bagChangeFormula")
	@JsonProperty("bagChangeFormula")
	private List<PurchaseFieldFormula> bagChangeFormula;

	@Field("litreChangeFormula")
	@JsonProperty("litreChangeFormula")
	private List<PurchaseFieldFormula> litreChangeFormula;

	@Field("bundleChangeFormula")
	@JsonProperty("bundleChangeFormula")
	private List<PurchaseFieldFormula> bundleChangeFormula;

	@Field("goFrugalMapping")
	@JsonProperty("goFrugalMapping")
	private List<GoFrugalPurchaseMapping> goFrugalMapping;

	@Field("purchase_history")
	@JsonProperty("purPriceHistory")
	private List<PurchasePriceHistory> purPriceHistory;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public List<PurchasePriceHistory> getPurPriceHistory() {
		return purPriceHistory;
	}

	public void setPurPriceHistory(List<PurchasePriceHistory> purPriceHistory) {
		this.purPriceHistory = purPriceHistory;
	}

	public String getProductAlias() {
		return productAlias;
	}

	public void setProductAlias(String productAlias) {
		this.productAlias = productAlias;
	}

	public boolean isProductStatus() {
		return productStatus;
	}

	public void setProductStatus(boolean productStatus) {
		this.productStatus = productStatus;
	}

	public List<PurchaseFieldFormula> getPieceChangeFormula() {
		return pieceChangeFormula;
	}

	public void setPieceChangeFormula(List<PurchaseFieldFormula> pieceChangeFormula) {
		this.pieceChangeFormula = pieceChangeFormula;
	}

	public List<PurchaseFieldFormula> getKgChangeFormula() {
		return kgChangeFormula;
	}

	public void setKgChangeFormula(List<PurchaseFieldFormula> kgChangeFormula) {
		this.kgChangeFormula = kgChangeFormula;
	}

	public List<PurchaseFieldFormula> getDozenChangeFormula() {
		return dozenChangeFormula;
	}

	public void setDozenChangeFormula(List<PurchaseFieldFormula> dozenChangeFormula) {
		this.dozenChangeFormula = dozenChangeFormula;
	}

	public List<PurchaseFieldFormula> getSaramChangeFormula() {
		return saramChangeFormula;
	}

	public void setSaramChangeFormula(List<PurchaseFieldFormula> saramChangeFormula) {
		this.saramChangeFormula = saramChangeFormula;
	}

	public List<PurchaseFieldFormula> getBoxChangeFormula() {
		return boxChangeFormula;
	}

	public void setBoxChangeFormula(List<PurchaseFieldFormula> boxChangeFormula) {
		this.boxChangeFormula = boxChangeFormula;
	}

	public List<PurchaseFieldFormula> getBagChangeFormula() {
		return bagChangeFormula;
	}

	public void setBagChangeFormula(List<PurchaseFieldFormula> bagChangeFormula) {
		this.bagChangeFormula = bagChangeFormula;
	}

	public List<PurchaseFieldFormula> getLitreChangeFormula() {
		return litreChangeFormula;
	}

	public void setLitreChangeFormula(List<PurchaseFieldFormula> litreChangeFormula) {
		this.litreChangeFormula = litreChangeFormula;
	}

	public List<PurchaseFieldFormula> getBundleChangeFormula() {
		return bundleChangeFormula;
	}

	public void setBundleChangeFormula(List<PurchaseFieldFormula> bundleChangeFormula) {
		this.bundleChangeFormula = bundleChangeFormula;
	}

	public List<GoFrugalPurchaseMapping> getGoFrugalMapping() {
		return goFrugalMapping;
	}

	public void setGoFrugalMapping(List<GoFrugalPurchaseMapping> goFrugalMapping) {
		this.goFrugalMapping = goFrugalMapping;
	}

	@Override
	public int compareTo(PurchaseHistory o) {
		return this.productName.compareTo(o.productName);
	}
}
