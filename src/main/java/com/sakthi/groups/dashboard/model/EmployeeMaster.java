package com.sakthi.groups.dashboard.model;

import java.math.BigInteger;
import java.util.Date;

import org.bson.types.Binary;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "employee_master")
public class EmployeeMaster {

	@Id
	private ObjectId id;
	private String employeeId;
	private String employeeName;
	private BigInteger phoneNumber;
	private String presentAddress;
	private String permanentAddress;
	private Date joiningDate;
	private String emailId;
	private String proofFileName;
	private Binary addressProof;

	public EmployeeMaster() {
		
	}

	public EmployeeMaster(String employeeId, String employeeName, BigInteger phoneNumber, String presentAddress,
			String permanentAddress, Date joiningDate, String emailId, String proofFileName, Binary addressProof) {
		super();
		this.employeeId = employeeId;
		this.employeeName = employeeName;
		this.phoneNumber = phoneNumber;
		this.presentAddress = presentAddress;
		this.permanentAddress = permanentAddress;
		this.joiningDate = joiningDate;
		this.emailId = emailId;
		this.proofFileName = proofFileName;
		this.addressProof = addressProof;
	}

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public BigInteger getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(BigInteger phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPresentAddress() {
		return presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getPermanentAddress() {
		return permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public Date getJoiningDate() {
		return joiningDate;
	}

	public void setJoiningDate(Date joiningDate) {
		this.joiningDate = joiningDate;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getProofFileName() {
		return proofFileName;
	}

	public void setProofFileName(String proofFileName) {
		this.proofFileName = proofFileName;
	}

	public Binary getAddressProof() {
		return addressProof;
	}

	public void setAddressProof(Binary addressProof) {
		this.addressProof = addressProof;
	}

}
