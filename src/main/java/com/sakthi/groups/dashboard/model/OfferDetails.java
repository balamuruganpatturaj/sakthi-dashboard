package com.sakthi.groups.dashboard.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "offer_details")
public class OfferDetails {

	@Id
	private ObjectId id;
	private ObjectId customerObjId;
	private String billNo;
	private Date purchaseDate;
	private String offerType;
	private int offerQuantity;
	private String offerDetails;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public ObjectId getCustomerObjId() {
		return customerObjId;
	}

	public void setCustomerObjId(ObjectId customerObjId) {
		this.customerObjId = customerObjId;
	}

	public String getBillNo() {
		return billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getOfferType() {
		return offerType;
	}

	public void setOfferType(String offerType) {
		this.offerType = offerType;
	}

	public int getOfferQuantity() {
		return offerQuantity;
	}

	public void setOfferQuantity(int offerQuantity) {
		this.offerQuantity = offerQuantity;
	}

	public String getOfferDetails() {
		return offerDetails;
	}

	public void setOfferDetails(String offerDetails) {
		this.offerDetails = offerDetails;
	}

}
