package com.sakthi.groups.dashboard.model;

import java.util.List;

import org.bson.types.Binary;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "company")
public class Company {

	public Company(String companyName, String address, String location, String area, Integer pincode,
			List<String> contactNo, boolean status, String gstNo, String panNo, String gstType, Binary gstFile,
			String gstFileName, Binary panFile, String panFileName) {
		super();
		this.companyName = companyName;
		this.address = address;
		this.location = location;
		this.area = area;
		this.pincode = pincode;
		this.contactNo = contactNo;
		this.status = status;
		this.gstNo = gstNo;
		this.panNo = panNo;
		this.gstType = gstType;
		this.gstFile = gstFile;
		this.gstFileName = gstFileName;
		this.panFile = panFile;
		this.panFileName = panFileName;
	}

	public Company() {
	}

	@Id
	private ObjectId id;
	private String companyName;
	private String address;
	private String location;
	private String area;
	private Integer pincode;
	private List<String> contactNo;
	private boolean status;
	private String gstNo;
	private String panNo;
	private String gstType;
	private Binary gstFile;
	private String gstFileName;
	private Binary panFile;
	private String panFileName;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public Integer getPincode() {
		return pincode;
	}

	public void setPincode(Integer pincode) {
		this.pincode = pincode;
	}

	public List<String> getContactNo() {
		return contactNo;
	}

	public void setContactNo(List<String> contactNo) {
		this.contactNo = contactNo;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getGstNo() {
		return gstNo;
	}

	public void setGstNo(String gstNo) {
		this.gstNo = gstNo;
	}

	public String getPanNo() {
		return panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public String getGstType() {
		return gstType;
	}

	public void setGstType(String gstType) {
		this.gstType = gstType;
	}

	public Binary getGstFile() {
		return gstFile;
	}

	public void setGstFile(Binary gstFile) {
		this.gstFile = gstFile;
	}

	public String getGstFileName() {
		return gstFileName;
	}

	public void setGstFileName(String gstFileName) {
		this.gstFileName = gstFileName;
	}

	public Binary getPanFile() {
		return panFile;
	}

	public void setPanFile(Binary panFile) {
		this.panFile = panFile;
	}

	public String getPanFileName() {
		return panFileName;
	}

	public void setPanFileName(String panFileName) {
		this.panFileName = panFileName;
	}
}
