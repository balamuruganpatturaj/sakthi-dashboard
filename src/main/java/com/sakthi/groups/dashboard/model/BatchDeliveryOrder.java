package com.sakthi.groups.dashboard.model;

import java.util.Date;

public class BatchDeliveryOrder {

	public BatchDeliveryOrder(String phoneNumber, Date orderPlacedDate, Integer billNo, Double billAmount, boolean delivered) {
		super();
		this.phoneNumber = phoneNumber;
		this.orderPlacedDate = orderPlacedDate;
		this.billNo = billNo;
		this.billAmount = billAmount;
		this.delivered = delivered;
	}

	public BatchDeliveryOrder() {
	}

	private String phoneNumber;
	private Date orderPlacedDate;
	private Integer billNo;
	private Double billAmount;
	private boolean delivered;

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Date getOrderPlacedDate() {
		return orderPlacedDate;
	}

	public void setOrderPlacedDate(Date orderPlacedDate) {
		this.orderPlacedDate = orderPlacedDate;
	}

	public Integer getBillNo() {
		return billNo;
	}

	public void setBillNo(Integer billNo) {
		this.billNo = billNo;
	}

	public Double getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(Double billAmount) {
		this.billAmount = billAmount;
	}

	public boolean isDelivered() {
		return delivered;
	}

	public void setDelivered(boolean delivered) {
		this.delivered = delivered;
	}

}
