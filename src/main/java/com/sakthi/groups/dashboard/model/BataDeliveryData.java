package com.sakthi.groups.dashboard.model;

import java.util.List;

import com.sakthi.groups.migration.portal.model.DeliveryOrderDetail;

public class BataDeliveryData {

	private List<DeliveryOrderDetail> deliveryDetail;
	private List<BatchChangeProvided> changesGiven;

	public List<DeliveryOrderDetail> getDeliveryDetail() {
		return deliveryDetail;
	}

	public void setDeliveryDetail(List<DeliveryOrderDetail> deliveryDetail) {
		this.deliveryDetail = deliveryDetail;
	}

	public List<BatchChangeProvided> getChangesGiven() {
		return changesGiven;
	}

	public void setChangesGiven(List<BatchChangeProvided> changesGiven) {
		this.changesGiven = changesGiven;
	}
}
