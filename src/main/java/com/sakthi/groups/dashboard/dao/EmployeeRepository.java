package com.sakthi.groups.dashboard.dao;

import java.math.BigInteger;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.sakthi.groups.dashboard.model.EmployeeMaster;

public interface EmployeeRepository extends MongoRepository<EmployeeMaster, ObjectId> {

	public EmployeeMaster findByPhoneNumber(BigInteger phoneNumber);

	public EmployeeMaster findByEmployeeName(String employeeName);

	public EmployeeMaster findByEmployeeNameAndPhoneNumber(String employeeName, BigInteger phoneNumber);

}
