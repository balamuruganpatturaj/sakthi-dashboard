package com.sakthi.groups.dashboard.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.sakthi.groups.dashboard.model.ReversalReport;

public interface ReversalReportRepository extends MongoRepository<ReversalReport, ObjectId> {

	ReversalReport findByTidNoAndTxnDateAndReversalAmount(BigInteger tidNo, Date txnDate, BigDecimal reversalAmount);

}
