package com.sakthi.groups.dashboard.dao;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.sakthi.groups.dashboard.model.SupplierMaster;

public interface SupplierRepository extends MongoRepository<SupplierMaster, ObjectId> {

	SupplierMaster findBySupplierCode(Integer supplierCode);

}
