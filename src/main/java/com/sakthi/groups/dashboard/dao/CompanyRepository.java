package com.sakthi.groups.dashboard.dao;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.sakthi.groups.dashboard.model.Company;

public interface CompanyRepository extends MongoRepository<Company, ObjectId> {

	Company findByGstNo(String gstNo);
	
	Company findByStatusAndCompanyName(boolean isActive, String companyName);

	List<Company> findByStatus(boolean isActive);
	
	

}
