package com.sakthi.groups.dashboard.dao;

import java.util.List;
import java.util.UUID;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.sakthi.groups.dashboard.model.BatchDeliveryDetails;

public interface BatchDeliveryRepository extends MongoRepository<BatchDeliveryDetails, ObjectId> {

	BatchDeliveryDetails findByBatchNumber(UUID batchNumber);

	List<BatchDeliveryDetails> findByBatchStatusOrderByDeliveryDateDesc(String batchStatus);

}
