package com.sakthi.groups.dashboard.dao;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.sakthi.groups.dashboard.model.InvoiceDetails;

public interface InvoiceDetailsRepository extends MongoRepository<InvoiceDetails, ObjectId> {

	public InvoiceDetails findBySupplierNameAndInvoiceNoAndInvoiceDate(String supplierName, String invoiceNo,
			Date invoiceDate);
}
