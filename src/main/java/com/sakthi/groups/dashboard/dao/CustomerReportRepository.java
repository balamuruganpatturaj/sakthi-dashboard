package com.sakthi.groups.dashboard.dao;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.sakthi.groups.dashboard.model.CustomerAnalyticsReport;
import com.sakthi.groups.migration.portal.model.BillSearchCriteria;

public interface CustomerReportRepository extends MongoRepository<CustomerAnalyticsReport, ObjectId> {

	CustomerAnalyticsReport findBySearchCriteria(BillSearchCriteria searchCriteria);

}
