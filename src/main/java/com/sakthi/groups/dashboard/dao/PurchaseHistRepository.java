package com.sakthi.groups.dashboard.dao;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.sakthi.groups.dashboard.model.PurchaseHistory;

public interface PurchaseHistRepository extends MongoRepository<PurchaseHistory, ObjectId> {

	PurchaseHistory findByProductName(String productName);
}
