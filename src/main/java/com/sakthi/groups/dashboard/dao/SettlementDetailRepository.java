package com.sakthi.groups.dashboard.dao;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.sakthi.groups.dashboard.model.SettlementDetail;

public interface SettlementDetailRepository extends MongoRepository<SettlementDetail, ObjectId> {

	List<SettlementDetail> findByTidNoAndTxnDate(BigInteger tidNo, Date txnDate);

	List<SettlementDetail> findByBankNameAndAssignedPartyAndTxnDate(String bankName, String assignedParty,
			Date txnDate);

}
