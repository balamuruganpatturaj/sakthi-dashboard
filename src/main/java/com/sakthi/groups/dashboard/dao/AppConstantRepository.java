package com.sakthi.groups.dashboard.dao;

import org.bson.types.ObjectId;
import org.springframework.data.repository.CrudRepository;

import com.sakthi.groups.dashboard.model.AppConstants;

public interface AppConstantRepository extends CrudRepository<AppConstants, ObjectId> {

}
