package com.sakthi.groups.dashboard.dao;

import java.math.BigInteger;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.sakthi.groups.dashboard.model.EdcPosDetail;

public interface EdcPosDetailRepository extends MongoRepository<EdcPosDetail, ObjectId> {

	EdcPosDetail findByTidNo(BigInteger tidNo);
	
	List<EdcPosDetail> findByStatus(boolean status);

}
