package com.sakthi.groups.dashboard.dao;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.sakthi.groups.dashboard.model.LatestPurchaseHistory;

public interface LatestPurchaseHistRepository extends MongoRepository<LatestPurchaseHistory, ObjectId> {

	LatestPurchaseHistory findByProductName(String productName);

}
