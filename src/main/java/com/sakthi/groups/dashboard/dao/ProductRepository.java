package com.sakthi.groups.dashboard.dao;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.sakthi.groups.migration.portal.model.ProductMaster;

public interface ProductRepository extends MongoRepository<ProductMaster, ObjectId> {

	List<ProductMaster> findByProductAliasIgnoreCase(String productAlias);

	ProductMaster findByProductCode(Integer productCode);
	
	ProductMaster findByProductNameIgnoreCase(String productName);

}
