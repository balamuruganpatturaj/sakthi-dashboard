package com.sakthi.groups.dashboard.dao;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.sakthi.groups.dashboard.model.CustomerMaster;

public interface CustomerRepository extends MongoRepository<CustomerMaster, ObjectId> {

	public List<CustomerMaster> findByCustomerPhoneNumberIn(List<String> customerPhoneNumber);

	public CustomerMaster findByCustomerIdIgnoreCase(String customerId);

	public CustomerMaster findByCustomerIdIgnoreCaseOrCustomerPhoneNumberIn(String customerId,
			List<String> customerPhoneNumber);

}
