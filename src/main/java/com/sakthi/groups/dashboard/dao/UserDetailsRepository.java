package com.sakthi.groups.dashboard.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.sakthi.groups.dashboard.model.UserDetail;

public interface UserDetailsRepository extends MongoRepository<UserDetail, String> {

	UserDetail findByUserName(String userName);
	
	UserDetail findByUserNameAndPassword(String userName, String password);
}
