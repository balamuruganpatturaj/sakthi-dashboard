package com.sakthi.groups.dashboard.dao;

import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.sakthi.groups.dashboard.model.ChennaiPurchase;

public interface ChennaiPurchaseRepository extends MongoRepository<ChennaiPurchase, ObjectId> {

	List<ChennaiPurchase> findByProductNameOrderByPurchaseDateDesc(String productName);

	List<ChennaiPurchase> findByPurchaseDateGreaterThanOrderByPurchaseDateDesc(Date purchaseDate);

	List<ChennaiPurchase> findByProductNameAndPurchaseDate(String productName, Date purchaseDate);

	ChennaiPurchase findByProductNameAndPurchaseDateAndMaxRetailPrice(String productName, Date purchaseDate,
			Double maxRetailPrice);
	
	List<ChennaiPurchase> findByEancodeOrderByPurchaseDateDesc(String eancode);

}
