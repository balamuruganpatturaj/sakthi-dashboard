package com.sakthi.groups.dashboard.dao;

import java.math.BigInteger;
import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.sakthi.groups.dashboard.model.GiftDetails;


public interface GiftDetailsRepository extends MongoRepository<GiftDetails, ObjectId> {

	@Query(value = "{ 'phoneNumber' : ?0 , 'offerType' : ?1, 'issuedDate' : { $gte: ?2, $lte: ?3} }")
	public GiftDetails findByphoneNumberdAndOfferTypeAndIssuedeBetween(BigInteger phoneNumber, String offerType,
			Date thresholdDate, Date issuedDate);
}
