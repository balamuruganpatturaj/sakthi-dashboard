package com.sakthi.groups.dashboard.dao;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.sakthi.groups.dashboard.model.PurchaseFieldChange;

public interface PurchaseFieldChangeRepository extends MongoRepository<PurchaseFieldChange, ObjectId> {

	PurchaseFieldChange findByProductObjId(ObjectId productObjId);
}
