package com.sakthi.groups.dashboard.dao;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.sakthi.groups.dashboard.model.OfferDetails;

public interface OfferDetailsRepository extends MongoRepository<OfferDetails, ObjectId> {

	@Query(value = "{ 'customerObjId' : ?0 , 'offerType' : ?1, 'purchaseDate' : { $gte: ?2, $lte: ?3} }")
	public OfferDetails findByCustomerObjIdAndOfferTypeAndPurchaseDateBetween(ObjectId customerObjId, String offerType,
			Date thresholdDate, Date purchaseDate);
}
