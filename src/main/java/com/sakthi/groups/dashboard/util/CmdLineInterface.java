package com.sakthi.groups.dashboard.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sakthi.groups.dashboard.bean.OfferInputDetail;
import com.sakthi.groups.dashboard.dao.CustomerRepository;
import com.sakthi.groups.dashboard.dao.GiftDetailsRepository;
import com.sakthi.groups.dashboard.dao.LatestPurchaseHistRepository;
import com.sakthi.groups.dashboard.dao.OfferDetailsRepository;
import com.sakthi.groups.dashboard.dao.PurchaseHistRepository;
import com.sakthi.groups.dashboard.model.CustomerMaster;
import com.sakthi.groups.dashboard.model.GiftDetails;
import com.sakthi.groups.dashboard.model.InvoiceDetails;
import com.sakthi.groups.dashboard.model.LatestPurchaseHistory;
import com.sakthi.groups.dashboard.model.OfferDetails;
import com.sakthi.groups.dashboard.model.PurchaseHistory;
import com.sakthi.groups.dashboard.service.ConfigurationService;
import com.sakthi.groups.dashboard.service.MongoDBMgmtService;
import com.sakthi.groups.dashboard.service.PeopleMgmtService;
import com.sakthi.groups.dashboard.service.PurchaseHistoryService;

@SuppressWarnings("unused")
@Component
@Profile("dev")
public class CmdLineInterface implements CommandLineRunner {

	@Autowired
	private PurchaseHistoryService purHistoryService;

	@Autowired
	private ConfigurationService configService;

	@Autowired
	private MongoTemplate mongoTemplate;

	// @Autowired
	// private AppConstantRepository appConstantRepo;

	@Autowired
	private PurchaseHistRepository purHistoryRepo;

	@Autowired
	private LatestPurchaseHistRepository latestPurchaseRepo;

	@Autowired
	private CustomerRepository customerRepo;

	@Autowired
	private OfferDetailsRepository offerRepo;

	@Autowired
	private GiftDetailsRepository giftRepo;

	@Autowired
	private MongoDBMgmtService mongoService;

	@Autowired
	private PeopleMgmtService pplMgtService;

	private ObjectMapper mapper = new ObjectMapper();

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public void run(String... args) throws Exception {
		/*
		 * HashMap<String, String> appConstantMap = new HashMap<>(); AppConstants
		 * appConst = new AppConstants(); String props =
		 * "{\"no_of_last_purchase\": \"5\", \"testing\": \"Success\"}";
		 * appConst.setAppConstants(props); appConstantRepo.save(appConst);
		 */

		/*
		 * List<PurchaseHistory> purHistory = new ArrayList<>(); purHistory =
		 * purHistoryRepo.findAll(); purHistory.forEach(entries -> {
		 * entries.setProductStaus(true); entries.setProductAlias("");
		 * purHistoryRepo.save(entries); });
		 */

		/*
		 * Optional<CustomerMaster> customerDetails = customerRepo.findById(new
		 * ObjectId("5d9399765092750e08aab99d"));
		 * System.out.println(customerDetails.get().getId());
		 */

		/*
		 * System.out.println("Purchase history record: " +
		 * purHistoryService.getProductNames().size());
		 * purHistoryService.getPurchaseTrendData("APT KM 50GM");
		 * 
		 * purHistoryService.getProductInfo("APT KM 50GM");
		 * 
		 * System.out.println(purHistoryService.getFilteredProductNames("U T D"));
		 */

		// purHistoryService.exportPurchaseHistoryAsExcel();

		/*
		 * PurchaseHistory purchaseHistFrom =
		 * purHistoryRepo.findByProductName("Test(15 N)");
		 * purHistoryService.checkAndUpdateLatestPurchase(purchaseHistFrom, null);
		 */

		/*
		 * List<PurchaseHistory> purchaseHistList = purHistoryRepo.findAll();
		 * 
		 * purchaseHistList.forEach(purchaseHist -> { PurchaseHistory hist =
		 * purHistoryService.getLatestPurchaseHistory(purchaseHist.getProductName());
		 * LatestPurchaseHistory latestPurchase =
		 * latestPurchaseRepo.findByProductName(hist.getProductName());
		 * LatestPurchaseHistory prodLatest = new LatestPurchaseHistory();
		 * prodLatest.setProductName(hist.getProductName());
		 * prodLatest.setProductCategory(hist.getProductCategory());
		 * prodLatest.setProductAlias(hist.getProductAlias());
		 * prodLatest.setProductStatus(hist.isProductStatus());
		 * hist.getPurPriceHistory().forEach(action -> {
		 * prodLatest.setPurchaseDate(action.getPurchaseDate());
		 * action.getPurPriceList().forEach(priceList -> { switch
		 * (priceList.getQuantity()) { case "Piece":
		 * prodLatest.setPiecePrice(priceList.getNetRate()); break;
		 * 
		 * case "KG": prodLatest.setKgPrice(priceList.getNetRate()); break;
		 * 
		 * case "Dozen": prodLatest.setDozenPrice(priceList.getNetRate()); break;
		 * 
		 * case "Saram": prodLatest.setSaramPrice(priceList.getNetRate()); break;
		 * 
		 * case "Box": prodLatest.setBoxPrice(priceList.getNetRate()); break;
		 * 
		 * case "Bag": prodLatest.setBagPrice(priceList.getNetRate()); break;
		 * 
		 * case "Litre": prodLatest.setLitrePrice(priceList.getNetRate()); break;
		 * 
		 * case "Bundle": prodLatest.setBundlePrice(priceList.getNetRate()); break;
		 * 
		 * default: break; } }); });
		 * 
		 * if (latestPurchase != null) {
		 * logger.info("Updating latest purchase record for {}",
		 * prodLatest.getProductName()); prodLatest.setId(latestPurchase.getId());
		 * latestPurchaseRepo.save(prodLatest); } else {
		 * logger.info("Saving {} for first time", prodLatest.getProductName());
		 * latestPurchaseRepo.insert(prodLatest); } });
		 */

		/*
		 * List<GiftDetails> giftList = giftRepo.findAll(); giftList.forEach(action -> {
		 * if (action.getCustomerName() == null) { List<String> phoneNumberList = new
		 * ArrayList<>(); phoneNumberList.add(action.getPhoneNumber().toString());
		 * List<CustomerMaster> customerMasterList =
		 * customerRepo.findByCustomerPhoneNumberIn(phoneNumberList); if
		 * (customerMasterList != null && !customerMasterList.isEmpty()) {
		 * action.setCustomerName(customerMasterList.get(0).getCustomerName()); }
		 * giftRepo.save(action); } });
		 */

		/*
		 * Query query = new Query();
		 * query.addCriteria(Criteria.where("customerObjId").is(customerDetails.get().
		 * getId())); query.addCriteria(Criteria.where("offerType").is("CALENDAR"));
		 * query.addCriteria(Criteria.where("purchaseDate").gte(new
		 * Date(1574184076424L)).lte(new Date(1576607400000L))); List<OfferDetails>
		 * offerDetails = mongoTemplate.find(query, OfferDetails.class);
		 * 
		 * try { System.out.println("offerDetails: " +
		 * mapper.writeValueAsString(offerDetails)); } catch (JsonProcessingException e)
		 * { // TODO Auto-generated catch block e.printStackTrace(); }
		 */

		/*
		 * List<InvoiceDetails> invList = mongoTemplate.findAll(InvoiceDetails.class);
		 * System.out.println("invList Size: " + invList.size());
		 * 
		 * try { System.out.println("configService: " +
		 * mapper.writeValueAsString(configService.getConfigDetails()));
		 * System.out.println(mapper.writeValueAsString(purHistoryService.
		 * getProductInfo("Nice Thavudu 50KG(TK)"))); System.out
		 * .println(mapper.writeValueAsString(purHistoryService.
		 * getPurchaseHistoryData("Nice Thavudu 50KG(TK)"))); } catch
		 * (JsonProcessingException e) { e.printStackTrace(); }
		 */

		// mongoService.mongoDbBackup();
		// mongoService.googleDriveBackup();
		//checkIssuedGift();
		//checkCustomerAnalytics();
		purHistoryService.migratePurchaseFormula();
	}
	
	private void checkCustomerAnalytics() {
		pplMgtService.getCustomerTrendData("531", "last_month", null, null);
		pplMgtService.getCustomerTrendData("531", "current_month", null, null);
		pplMgtService.getCustomerTrendData("531", "current_year", null, null);
		pplMgtService.getCustomerTrendData("531", "last_year", null, null);
	}

	private void checkIssuedGift() throws ParseException {
		OfferInputDetail inputDetails = new OfferInputDetail();
		//{"searchInput":"9894201784","offerType":"CALENDAR","issuedDate":"2023-02-11T14:47:12.041Z","offerQuantity":"1","offerCategory":"Hindu"}
		inputDetails.setSearchInput("28651");
		inputDetails.setOfferType("CALENDAR");
		String dateString = "2023-02-11T15:07:51.621Z";
		String[] lastSectionArray = dateString.split("[.]");
		String lastSection = lastSectionArray[lastSectionArray.length - 1];
		Date stringToDate = DashboardConstants.stringToDateFormatter.parse(dateString.replaceAll("." + lastSection, "+0000"));
		logger.debug("stringToDate: {}", stringToDate);
		inputDetails.setIssuedDate(stringToDate);
		inputDetails.setOfferQuantity(1);
		inputDetails.setOfferCategory("Hindu");
		System.out.println("Check return Status: " + pplMgtService.checkGiftOffer(inputDetails));
	}

}
