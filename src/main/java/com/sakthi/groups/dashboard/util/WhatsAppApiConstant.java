package com.sakthi.groups.dashboard.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class WhatsAppApiConstant {
	
	@Value("${wati.api.endpoint}")
	private String watiApiEndPoint;
	
	@Value("${wati.api.token}")
	private String watiAuthorizeToken;
	
	public String watiSendTemplateMessageApi = "/api/v1/sendTemplateMessage";
	public String watiGreetingTemplate = "greeting_message";
	public String watiPaymentSuccessTemplate = "payment_success";
	public String watiPaymentPendingTemplate = "payment_pending";
	public String watiDeliveryBillTemplate = "delivery_bill";
	
}
