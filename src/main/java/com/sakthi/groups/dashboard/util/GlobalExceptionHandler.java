/*
 * package com.sakthi.groups.dashboard.util;
 * 
 * import java.sql.SQLException;
 * 
 * import org.springframework.http.HttpStatus; import
 * org.springframework.http.ResponseEntity; import
 * org.springframework.web.bind.annotation.ControllerAdvice; import
 * org.springframework.web.bind.annotation.RequestMapping; import
 * org.springframework.web.bind.annotation.ExceptionHandler; import
 * org.springframework.web.servlet.mvc.method.annotation.
 * ResponseEntityExceptionHandler;
 * 
 * import com.cisco.sstg.onprem.dashboard.exception.KeyGenerationException;
 * import com.cisco.sstg.onprem.dashboard.exception.PackagingException; import
 * com.cisco.sstg.onprem.dashboard.exception.UserManagementException;
 * 
 * import io.jsonwebtoken.ExpiredJwtException;
 * 
 * @ControllerAdvice
 * 
 * @RequestMapping(produces = "application/vnd.error+json") public class
 * GlobalExceptionHandler extends ResponseEntityExceptionHandler {
 * 
 * @ExceptionHandler(UserManagementException.class) public ResponseEntity<?>
 * userManagementException(final UserManagementException ume) { return
 * ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ume.getMessage()
 * ); }
 * 
 * @ExceptionHandler(ExpiredJwtException.class) public ResponseEntity<?>
 * jwtException(final ExpiredJwtException eje) { return
 * ResponseEntity.status(HttpStatus.FORBIDDEN).body(eje.getMessage()); }
 * 
 * @ExceptionHandler(SQLException.class) public ResponseEntity<?>
 * sqlException(final SQLException se) { return
 * ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(se.getMessage())
 * ; }
 * 
 * @ExceptionHandler(PackagingException.class) public ResponseEntity<?>
 * packagingException(final PackagingException pe) { return
 * ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(pe.getMessage())
 * ; }
 * 
 * @ExceptionHandler(KeyGenerationException.class) public ResponseEntity<?>
 * keyGenerationException(final KeyGenerationException kge) { return
 * ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(kge.getMessage()
 * ); }
 * 
 * @ExceptionHandler(Exception.class) public ResponseEntity<?>
 * generalException(final Exception ex) { return
 * ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage())
 * ; } }
 */