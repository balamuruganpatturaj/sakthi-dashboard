package com.sakthi.groups.dashboard.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.time.LocalDate;
import java.time.Year;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.FileList;
import com.sakthi.groups.dashboard.dao.AppConstantRepository;
import com.sakthi.groups.dashboard.model.AppConstants;

@Component
public class DashboardUtil {

	@Autowired
	private AppConstantRepository appConstantRepo;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private static Drive driveService = null;

	@Cacheable("constants")
	public String getAppConstant(String property) {
		String propsValue = "";
		ObjectMapper objectMapper = new ObjectMapper();
		Iterable<AppConstants> appConstants = appConstantRepo.findAll();
		Map<String, String> propsMap = new HashMap<String, String>();
		try {
			propsMap = objectMapper.readValue(appConstants.iterator().next().getAppConstants(),
					new TypeReference<Map<String, Object>>() {
					});
		} catch (IOException e) {
			logger.error("Failed to parse the JSON string {}", e.getMessage());
		}
		propsValue = propsMap.get(property);
		logger.info("Property Key: {}, Property Value: {}", property, propsValue);
		return propsValue;
	}
	
	public HashMap<String, Date> getSpecifiedDates(String durtation) {
		HashMap<String, Date> dateMap = new HashMap<>();
		LocalDate todayDate = LocalDate.now();
		switch (durtation) {
		case "current_month":
			Date curMonStartDate = Date
					.from(todayDate.withDayOfMonth(1).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
			Date currMonEndDate = Date.from(todayDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
			dateMap.put("startDate", curMonStartDate);
			dateMap.put("endDate", currMonEndDate);
			break;

		case "last_month":
			Date lastMonStartDate = Date.from(todayDate.with(TemporalAdjusters.firstDayOfMonth()).minusMonths(1)
					.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
			Date lastMonEndDate = Date.from(todayDate.with(TemporalAdjusters.lastDayOfMonth()).minusMonths(1)
					.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
			dateMap.put("startDate", lastMonStartDate);
			dateMap.put("endDate", lastMonEndDate);
			break;

		case "current_year":
			Date curYearStartDate = Date.from(todayDate.with(TemporalAdjusters.firstDayOfYear()).atStartOfDay()
					.atZone(ZoneId.systemDefault()).toInstant());
			Date currYearEndDate = Date.from(todayDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
			dateMap.put("startDate", curYearStartDate);
			dateMap.put("endDate", currYearEndDate);
			break;

		case "last_year":
			Date lastYearStartDate = Date
					.from(Year.now().minusYears(1).atDay(1).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
			Date lastYearEndDate = Date.from(Year.now().minusYears(1).atDay(1).with(TemporalAdjusters.lastDayOfYear())
					.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
			dateMap.put("startDate", lastYearStartDate);
			dateMap.put("endDate", lastYearEndDate);
			break;

		default:
			break;
		}
		return dateMap;
	}

	/**
	 * Creates an authorized Credential object.
	 * 
	 * @param HTTP_TRANSPORT The network HTTP Transport.
	 * @return An authorized Credential object.
	 * @throws IOException If the credentials.json file cannot be found.
	 */
	protected Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws Exception {
		// Load client secrets.
		InputStream in = DashboardUtil.class.getResourceAsStream(DashboardConstants.CREDENTIALS_FILE_PATH);
		if (in == null) {
			throw new FileNotFoundException("Resource not found: " + DashboardConstants.CREDENTIALS_FILE_PATH);
		}
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(DashboardConstants.JSON_FACTORY,
				new InputStreamReader(in));
		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT,
				DashboardConstants.JSON_FACTORY, clientSecrets, DashboardConstants.SCOPES)
						.setDataStoreFactory(
								new FileDataStoreFactory(new java.io.File(DashboardConstants.TOKENS_DIRECTORY_PATH)))
						.setAccessType("offline").build();
		return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("cruselboy");
	}

	private Drive getGoogleDriveInstance() {
		NetHttpTransport HTTP_TRANSPORT = null;
		try {
			HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			driveService = new Drive.Builder(HTTP_TRANSPORT, DashboardConstants.JSON_FACTORY,
					getCredentials(HTTP_TRANSPORT)).setApplicationName(DashboardConstants.APPLICATION_NAME).build();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return driveService;
	}

	public Drive getGoogleDriveService() {
		if (driveService == null) {
			getGoogleDriveInstance();
		}
		return driveService;
	}
	
	public String getFileId(Drive googleDriveService, String fileName) {
		String fileId = "id";
		FileList result = null;
		logger.info("googleDriveService: " + googleDriveService);
		logger.info("googleDriveService.files(): " + googleDriveService.files());
		try {
			logger.info("googleDriveService.files().list(): " + googleDriveService.files().list());
			logger.info("googleDriveService: "
					+ googleDriveService.files().list().setFields("nextPageToken, files(id, name)"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			result = googleDriveService.files().list().setFields("nextPageToken, files(id, name)").execute();
		} catch (IOException | NullPointerException e) {
			logger.error("Failed to get list of files in Google Drive {}", e.getMessage());
		}
		List<com.google.api.services.drive.model.File> files = result.getFiles();
		if (files == null || files.isEmpty()) {
			logger.info("No files found in Google Drive");
		} else {
			for (com.google.api.services.drive.model.File fileList : files) {
				if (fileName.equalsIgnoreCase(fileList.getName())) {
					fileId = fileList.getId();
					logger.info("File id for {} is {}", fileName, fileId);
				}
			}
		}
		return fileId;
	}
	
	public void googleDriveBackup(String fileName, String fileAbsolutePath, String mediaType) {
		Drive googleDriveService = getGoogleDriveService();
		logger.info("googleDriveBackup () googleDriveService: " + googleDriveService);
		String fileId = getFileId(googleDriveService, fileName);
		com.google.api.services.drive.model.File fileMetadata = new com.google.api.services.drive.model.File();
		fileMetadata.setName(fileName);
		java.io.File filePath = new java.io.File(fileAbsolutePath);
		logger.info("Backup FilePath: {}", filePath.getAbsolutePath());
		FileContent mediaContent = new FileContent(mediaType, filePath);
		com.google.api.services.drive.model.File file = null;

		try {
			if ("id".equalsIgnoreCase(fileId)) {
				file = googleDriveService.files().create(fileMetadata, mediaContent).setFields(fileId).execute();
			} else {
				file = googleDriveService.files().update(fileId, fileMetadata, mediaContent).execute();
			}
			logger.info("File uploaded Successfully with file ID {}", file.getId());
		} catch (IOException e1) {
			e1.printStackTrace();
			logger.error("Failed to upload the backup file to Google Drive");
		} finally {
			if (filePath.exists()) {
				filePath.delete();
			}
		}
	}

	public static void main(String... args) throws IOException, GeneralSecurityException {
		DashboardUtil util = new DashboardUtil();

		com.google.api.services.drive.model.File fileMetadata = new com.google.api.services.drive.model.File();
		fileMetadata.setName(DashboardConstants.MONGO_BACKUP_NAME);
		java.io.File filePath = new java.io.File(DashboardConstants.MONGO_BACKUP_NAME);
		FileContent mediaContent = new FileContent("application/gzip", filePath);
		com.google.api.services.drive.model.File file = null;
		try {
			file = util.getGoogleDriveService().files().create(fileMetadata, mediaContent).setFields("id").execute();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		System.out.println("File ID: " + file.getId());

		FileList result = null;
		try {
			result = util.getGoogleDriveService().files().list().setPageSize(10)
					.setFields("nextPageToken, files(id, name)").execute();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<com.google.api.services.drive.model.File> files = result.getFiles();
		if (files == null || files.isEmpty()) {
			System.out.println("No files found.");
		} else {
			System.out.println("Files:");
			for (com.google.api.services.drive.model.File fileList : files) {
				System.out.printf("%s (%s)\n", fileList.getName(), fileList.getId());
			}
		}

	}

}
