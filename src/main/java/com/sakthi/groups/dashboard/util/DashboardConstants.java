package com.sakthi.groups.dashboard.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.DriveScopes;

public class DashboardConstants {
	public static final String SECRET = "sakthi_26bwxpQwFQZwI4E8JH6L";
	public static final long EXPIRATION_TIME = 300000; // 5 minutes
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER_STRING = "Authorization";
	public static final String LOGIN_URL = "/dashboard/login";

	public static final String NO_OF_LAST_PURCHASE = "no_of_last_purchase";
	public final static String dateFormat = "MM/dd/yyyy";
	public final static String stringToDateFormat = "yyyy-MM-dd'T'HH:mm:ssZ";
	public static final String THRESHOLD_DATE = "threshold_date";

	public final static DateFormat dateFomatter = new SimpleDateFormat(dateFormat);
	public final static DateFormat stringToDateFormatter = new SimpleDateFormat(stringToDateFormat, Locale.US);

	public final static String OUT_FOR_DELIVERY = "Out for Delivery";
	public final static String ORDER_PLACED = "Order Placed";
	public final static String ORDER_DELIVERED = "Delivered";
	public final static String PAYMENT_PENDING = "Payment Pending";
	public final static String ORDER_CANCELLED = "Cancelled";
	public final static String NOT_AVAILABLE = "Not Available";
	public final static String PARTIALLY_PAID = "Partially Paid";
	public final static String BATCH_OPEN = "OPEN";
	public final static String BATCH_CLOSED = "CLOSED";

	public final static String APPLICATION_NAME = " SakthiPortal";
	public final static JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	public final static String TOKENS_DIRECTORY_PATH = "tokens";
	public final static List<String> SCOPES = Collections.singletonList(DriveScopes.DRIVE);
	public final static String CREDENTIALS_FILE_PATH = "/credentialsdesktop.json";

	public final static List<String> MONGO_EXPORT_PATH = Arrays.asList("C:\\Program Files\\MongoDB\\Tools\\100\\bin",
			"C:\\Program Files\\MongoDB\\Server\\4.2\\bin", "C:\\Program Files\\MongoDB\\Server\\4.3\\bin");
	public final static String MONGO_EXPORT_CMD = "mongodump.exe";
	public final static String MONGO_BACKUP_NAME = "SakthiPortalBackup.gz";
	public final static String MONGO_BACKUP_ARCHIVE = String.format("--archive=%s", MONGO_BACKUP_NAME);
	public final static String MONGO_DB_NAME = "sakthi";
	
	
}
