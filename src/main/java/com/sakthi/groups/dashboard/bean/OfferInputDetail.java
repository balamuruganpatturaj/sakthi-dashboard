package com.sakthi.groups.dashboard.bean;

import java.util.Date;

public class OfferInputDetail {

	private String searchInput;
	private Date issuedDate;
	private String offerType;
	private int offerQuantity;
	private String offerCategory;

	public String getSearchInput() {
		return searchInput;
	}

	public void setSearchInput(String searchInput) {
		this.searchInput = searchInput;
	}

	public Date getIssuedDate() {
		return issuedDate;
	}

	public void setIssuedDate(Date issuedDate) {
		this.issuedDate = issuedDate;
	}

	public String getOfferType() {
		return offerType;
	}

	public void setOfferType(String offerType) {
		this.offerType = offerType;
	}

	public int getOfferQuantity() {
		return offerQuantity;
	}

	public void setOfferQuantity(int offerQuantity) {
		this.offerQuantity = offerQuantity;
	}

	public String getOfferCategory() {
		return offerCategory;
	}

	public void setOfferCategory(String offerCategory) {
		this.offerCategory = offerCategory;
	}
}
