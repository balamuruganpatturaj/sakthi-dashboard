package com.sakthi.groups.dashboard.bean;

public class ChennaiProductInfo {

	public ChennaiProductInfo(String productName, Double maxRetailPrice, Integer unitPerCase, String eancode) {
		super();
		this.productName = productName;
		this.maxRetailPrice = maxRetailPrice;
		this.unitPerCase = unitPerCase;
		this.eancode = eancode;
	}

	public ChennaiProductInfo() {
		super();
	}

	private String productName;
	private Double maxRetailPrice;
	private Integer unitPerCase;
	private String eancode;

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Double getMaxRetailPrice() {
		return maxRetailPrice;
	}

	public void setMaxRetailPrice(Double maxRetailPrice) {
		this.maxRetailPrice = maxRetailPrice;
	}

	public Integer getUnitPerCase() {
		return unitPerCase;
	}

	public void setUnitPerCase(Integer unitPerCase) {
		this.unitPerCase = unitPerCase;
	}

	public String getEancode() {
		return eancode;
	}

	public void setEancode(String eancode) {
		this.eancode = eancode;
	}

}
