package com.sakthi.groups.dashboard.bean;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PurchaseRowData {

	public PurchaseRowData() {
		super();
	}

	public PurchaseRowData(List<PurchaseRowValue> rowValueList) {
		super();
		this.rowValueList = rowValueList;
	}

	private List<PurchaseRowValue> rowValueList;

	public List<PurchaseRowValue> getRowValueList() {
		return rowValueList;
	}

	public void setRowValueList(List<PurchaseRowValue> rowValueList) {
		this.rowValueList = rowValueList;
	}

}
