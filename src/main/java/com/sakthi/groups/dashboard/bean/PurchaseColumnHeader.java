package com.sakthi.groups.dashboard.bean;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PurchaseColumnHeader {

	public PurchaseColumnHeader(String headerName) {
		super();
		this.headerName = headerName;
	}

	public PurchaseColumnHeader() {
		super();
	}

	private String headerName;

	public String getHeaderName() {
		return headerName;
	}

	public void setHeaderName(String headerName) {
		this.headerName = headerName;
	}

	@Override
	public String toString() {
		return "PurchaseColumnHeader [headerName=" + headerName + "]";
	}
}
