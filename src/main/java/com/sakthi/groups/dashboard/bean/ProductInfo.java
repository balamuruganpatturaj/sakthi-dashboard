package com.sakthi.groups.dashboard.bean;

public class ProductInfo {

	public ProductInfo() {
		super();
	}

	public ProductInfo(String productName, String productCategory, String productAlias) {
		super();
		this.productName = productName;
		this.productCategory = productCategory;
		this.productAlias = productAlias;
	}

	private String productName;
	private String productCategory;
	private String productAlias;

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public String getProductAlias() {
		return productAlias;
	}

	public void setProductAlias(String productAlias) {
		this.productAlias = productAlias;
	}

}
