package com.sakthi.groups.dashboard.bean;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.sakthi.groups.dashboard.model.ReversalDetail;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SettlementReport {

	private Date txnDate;
	private String bankName;
	private String assignedParty;
	private BigInteger tidNo;
	private BigInteger totalTxn;
	private BigDecimal txnAmount;
	private BigDecimal totalAmount;
	private BigDecimal settledAmount;
	private Set<ReversalDetail> addReversal;
	private Integer rowSpan;

	public Date getTxnDate() {
		return txnDate;
	}

	public void setTxnDate(Date txnDate) {
		this.txnDate = txnDate;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAssignedParty() {
		return assignedParty;
	}

	public void setAssignedParty(String assignedParty) {
		this.assignedParty = assignedParty;
	}

	public BigInteger getTidNo() {
		return tidNo;
	}

	public void setTidNo(BigInteger tidNo) {
		this.tidNo = tidNo;
	}

	public BigInteger getTotalTxn() {
		return totalTxn;
	}

	public void setTotalTxn(BigInteger totalTxn) {
		this.totalTxn = totalTxn;
	}

	public BigDecimal getTxnAmount() {
		return txnAmount;
	}

	public void setTxnAmount(BigDecimal txnAmount) {
		this.txnAmount = txnAmount;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getSettledAmount() {
		return settledAmount;
	}

	public void setSettledAmount(BigDecimal settledAmount) {
		this.settledAmount = settledAmount;
	}

	public Set<ReversalDetail> getAddReversal() {
		return addReversal;
	}

	public void setAddReversal(Set<ReversalDetail> addReversal) {
		this.addReversal = addReversal;
	}

	public Integer getRowSpan() {
		return rowSpan;
	}

	public void setRowSpan(Integer rowSpan) {
		this.rowSpan = rowSpan;
	}

}
