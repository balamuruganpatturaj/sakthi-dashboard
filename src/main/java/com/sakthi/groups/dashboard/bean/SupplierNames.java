package com.sakthi.groups.dashboard.bean;

public class SupplierNames {

	public SupplierNames() {
		super();
	}

	public SupplierNames(Integer supplierCode, String supplierName) {
		super();
		this.supplierCode = supplierCode;
		this.supplierName = supplierName;
	}

	private Integer supplierCode;
	private String supplierName;

	public Integer getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierCode(Integer supplierCode) {
		this.supplierCode = supplierCode;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

}
