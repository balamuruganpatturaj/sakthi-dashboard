package com.sakthi.groups.dashboard.bean;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PurchaseHistoryData {

	public PurchaseHistoryData() {
		super();
	}

	public PurchaseHistoryData(String productName, String productCategory, String productAlias, boolean productStatus,
			Integer columnSize, List<PurchaseColumnHeader> columnHeader, List<PurchaseRowData> rowData) {
		super();
		this.productName = productName;
		this.productCategory = productCategory;
		this.productAlias = productAlias;
		this.productStatus = productStatus;
		this.columnSize = columnSize;
		this.columnHeader = columnHeader;
		this.rowData = rowData;
	}

	private String productName;
	private String productCategory;
	private String productAlias;
	private boolean productStatus;
	private Integer columnSize;
	private List<PurchaseColumnHeader> columnHeader;
	private List<PurchaseRowData> rowData;

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public String getProductAlias() {
		return productAlias;
	}

	public void setProductAlias(String productAlias) {
		this.productAlias = productAlias;
	}

	public boolean isProductStatus() {
		return productStatus;
	}

	public void setProductStatus(boolean productStatus) {
		this.productStatus = productStatus;
	}

	public Integer getColumnSize() {
		return columnSize;
	}

	public void setColumnSize(Integer columnSize) {
		this.columnSize = columnSize;
	}

	public List<PurchaseColumnHeader> getColumnHeader() {
		return columnHeader;
	}

	public void setColumnHeader(List<PurchaseColumnHeader> columnHeader) {
		this.columnHeader = columnHeader;
	}

	public List<PurchaseRowData> getRowData() {
		return rowData;
	}

	public void setRowData(List<PurchaseRowData> rowData) {
		this.rowData = rowData;
	}

}
