package com.sakthi.groups.dashboard.bean;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PurchaseRowValue {

	public PurchaseRowValue() {
		super();
	}

	public PurchaseRowValue(Object rowValue) {
		super();
		this.rowValue = rowValue;
	}

	private Object rowValue;

	public Object getValue() {
		return rowValue;
	}

	public void setValue(Object rowValue) {
		this.rowValue = rowValue;
	}

	@Override
	public String toString() {
		return "PurchaseRowValue [rowValue=" + rowValue + "]";
	}

}
