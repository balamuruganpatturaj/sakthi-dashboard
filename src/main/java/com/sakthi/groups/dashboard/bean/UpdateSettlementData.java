package com.sakthi.groups.dashboard.bean;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.sakthi.groups.dashboard.model.ReversalDetail;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateSettlementData {

	private Date txnDate;
	private Set<BigInteger> tidNo;
	private BigDecimal totalTxnAmt;
	private BigDecimal settledAmount;
	private BigDecimal differenceAmt;
	private Set<ReversalDetail> reversalData;

	public Date getTxnDate() {
		return txnDate;
	}

	public void setTxnDate(Date txnDate) {
		this.txnDate = txnDate;
	}

	public Set<BigInteger> getTidNo() {
		return tidNo;
	}

	public void setTidNo(Set<BigInteger> tidNo) {
		this.tidNo = tidNo;
	}

	public BigDecimal getTotalTxnAmt() {
		return totalTxnAmt;
	}

	public void setTotalTxnAmt(BigDecimal totalTxnAmt) {
		this.totalTxnAmt = totalTxnAmt;
	}

	public BigDecimal getSettledAmount() {
		return settledAmount;
	}

	public void setSettledAmount(BigDecimal settledAmount) {
		this.settledAmount = settledAmount;
	}

	public BigDecimal getDifferenceAmt() {
		return differenceAmt;
	}

	public void setDifferenceAmt(BigDecimal differenceAmt) {
		this.differenceAmt = differenceAmt;
	}

	public Set<ReversalDetail> getReversalData() {
		return reversalData;
	}

	public void setReversalData(Set<ReversalDetail> reversalData) {
		this.reversalData = reversalData;
	}

}
