package com.sakthi.groups.dashboard.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.sakthi.groups.dashboard.dao.UserDetailsRepository;
import com.sakthi.groups.dashboard.model.UserDetail;

@Service
public class MongoDBAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

	@Autowired
	private UserDetailsRepository userDetailsRepo;
	
	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails,
			UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {

	}

	@Override
	protected UserDetails retrieveUser(String userName, UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {
		UserDetails loggedInUser = null;
		UserDetail userDetail = userDetailsRepo.findByUserNameAndPassword(userName, authentication.getCredentials().toString());
		if(userDetail == null) {
			throw new InternalAuthenticationServiceException(
                    "UserDetailsService returned null, which is an interface contract violation");
		} else {
			List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
			userDetail.getRoles().forEach(role -> authList.add(new SimpleGrantedAuthority(role)));
			loggedInUser = new User(userName, userDetail.getPassword(), authList);
		}
		return loggedInUser;
	}

}
