package com.sakthi.groups.dashboard.service;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sakthi.groups.dashboard.bean.ProductInfo;
import com.sakthi.groups.dashboard.bean.PurchaseColumnHeader;
import com.sakthi.groups.dashboard.bean.PurchaseHistoryData;
import com.sakthi.groups.dashboard.bean.PurchaseRowData;
import com.sakthi.groups.dashboard.bean.PurchaseRowValue;
import com.sakthi.groups.dashboard.dao.LatestPurchaseHistRepository;
import com.sakthi.groups.dashboard.dao.PurchaseFieldChangeRepository;
import com.sakthi.groups.dashboard.dao.PurchaseHistRepository;
import com.sakthi.groups.dashboard.model.LatestPurchaseHistory;
import com.sakthi.groups.dashboard.model.PurchaseFieldChange;
import com.sakthi.groups.dashboard.model.PurchaseHistory;
import com.sakthi.groups.dashboard.model.PurchasePriceHistory;
import com.sakthi.groups.dashboard.model.PurchasePriceList;
import com.sakthi.groups.dashboard.model.chart.CModel;
import com.sakthi.groups.dashboard.model.chart.ColModel;
import com.sakthi.groups.dashboard.model.chart.GoogleChartModel;
import com.sakthi.groups.dashboard.model.chart.RowModel;
import com.sakthi.groups.dashboard.util.DashboardConstants;
import com.sakthi.groups.dashboard.util.DashboardUtil;

@Service
public class PurchaseHistoryServiceImpl implements PurchaseHistoryService {

	@Autowired
	private PurchaseHistRepository purHistoryRepo;

	@Autowired
	private LatestPurchaseHistRepository latestPurchaseRepo;
	
	@Autowired
	private PurchaseFieldChangeRepository fieldChangeRepo;

	@Autowired
	private DashboardUtil dashUtil;

	@Autowired
	private MongoTemplate mongoTemplate;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");

	private List<String> headerDetails = Arrays.asList("Product Name", "Alias", "Category", "Status", "Purchase Date",
			"MRP", "Piece", "KG", "Dozen", "Saram", "Box", "Bag", "Litre", "Bundle");

	private List<String> commonHeaderDetails = Arrays.asList("Product Name", "Alias", "Category", "Status");

	private List<String> priceHeaderDetails = Arrays.asList("Purchase Date", "MRP", "Piece", "KG", "Dozen", "Saram",
			"Box", "Bag", "Litre", "Bundle");

	@Override
	@Cacheable("productNames")
	public List<String> getProductNames() {
		logger.info("Getting product names");
		List<String> productNames = new ArrayList<>();
		purHistoryRepo.findAll().forEach(entries -> productNames.add(entries.getProductName()));
		return productNames;
	}

	@Override
	@Cacheable("filterProductNames")
	public List<String> getFilteredProductNames(String productName) {
		List<String> filteredProduct = new ArrayList<>();
		Query query = new Query();
		Criteria nameCriteria = new Criteria();
		Criteria aliasCriteria = new Criteria();
		try {
			nameCriteria = Criteria.where("product_name").regex(productName.replaceAll("[ /\\*+()]", ".*"), "i");
			aliasCriteria = Criteria.where("product_alias").regex(productName, "i");
		} catch (PatternSyntaxException e) {
			logger.error("Exception occurred in creating query {}", e.getMessage());
		}
		Criteria filterCriteria = new Criteria().orOperator(nameCriteria, aliasCriteria);
		query.addCriteria(filterCriteria);
		List<PurchaseHistory> resultPurHistList = mongoTemplate.find(query, PurchaseHistory.class);
		resultPurHistList.forEach(entries -> filteredProduct.add(entries.getProductName()));
		return filteredProduct;
	}

	@Override
	@Cacheable("productCategory")
	public Set<String> getProductCategory(String prodName) {
		logger.info("Getting product category");
		Set<String> productCategory = new HashSet<>();
		if (prodName == null || "".equals(prodName)) {
			purHistoryRepo.findAll().forEach(entries -> {
				logger.info("Product Name: {}", entries.getProductName());
				productCategory.add(entries.getProductCategory());
			});
		} else {
			PurchaseHistory purHist = purHistoryRepo.findByProductName(prodName);
			if (purHist == null) {
				return productCategory;
			}
			productCategory.add(purHistoryRepo.findByProductName(prodName).getProductCategory());
		}
		return productCategory;
	}

	@Override
	@Cacheable("productInfo")
	public ProductInfo getProductInfo(String prodName) {
		logger.info("Getting product info");
		ProductInfo prodInfo = new ProductInfo();
		PurchaseHistory purHist = purHistoryRepo.findByProductName(prodName);
		if (purHist == null) {
			return prodInfo;
		} else {
			prodInfo = new ProductInfo(purHist.getProductName(), purHist.getProductCategory(),
					purHist.getProductAlias());
		}
		return prodInfo;
	}

	@Override
	public List<PurchaseHistory> getPurchaseHistory(String productName) {
		List<PurchaseHistory> purHistory = new ArrayList<>();
		if (productName == null || "".equals(productName)) {
			purHistory = purHistoryRepo.findAll();
		} else {
			PurchaseHistory purchaseHist = purHistoryRepo.findByProductName(productName);
			if (purchaseHist == null) {
				return purHistory;
			} else {
				purHistory.add(purHistoryRepo.findByProductName(productName));
			}
		}
		List<PurchaseHistory> purHistorySorted = getPurchaseHistorySorted(purHistory);
		return purHistorySorted;
	}

	@Override
	public List<PurchaseHistoryData> getPurchaseHistoryData(String productName) {
		List<PurchaseHistory> purHistorySorted = getPurchaseHistory(productName);
		List<PurchaseHistoryData> purchaseHistoryDataList = new ArrayList<>();
		purHistorySorted.forEach(purHistSort -> {
			List<PurchaseColumnHeader> columnHeaderList = new ArrayList<>();
			List<PurchaseRowData> rowDataList = new ArrayList<>();
			Set<String> purchaseCategory = new HashSet<String>();
			purHistSort.getPurPriceHistory().forEach(puchaseHist -> {
				puchaseHist.getPurPriceList().forEach(priceList -> purchaseCategory.add(priceList.getQuantity()));
				if (puchaseHist.getMaxRetailPrice() != null) {
					purchaseCategory.add("MRP");
				}
			});
			columnHeaderList.add(new PurchaseColumnHeader("Date"));
			purchaseCategory.forEach(category -> {
				columnHeaderList.add(new PurchaseColumnHeader(category));
			});
			purHistSort.getPurPriceHistory()
					.forEach(action -> generatePurchaseHistoryData(action, purchaseCategory, rowDataList));
			purchaseHistoryDataList.add(new PurchaseHistoryData(purHistSort.getProductName(),
					purHistSort.getProductCategory(), purHistSort.getProductAlias(), purHistSort.isProductStatus(),
					columnHeaderList.size(), columnHeaderList, rowDataList));
		});
		return purchaseHistoryDataList;
	}

	private Object generatePurchaseHistoryData(PurchasePriceHistory action, Set<String> purchaseCategory,
			List<PurchaseRowData> rowDataList) {
		List<PurchaseRowValue> rowValueList = new ArrayList<>();
		PurchaseRowValue purchaseRowValue = new PurchaseRowValue(action.getPurchaseDate());
		rowValueList.add(purchaseRowValue);
		purchaseCategory.forEach(cat -> generateRowValue(cat, action, rowValueList));
		rowDataList.add(new PurchaseRowData(rowValueList));
		return null;
	}

	private Object generateRowValue(String purchaseCategory, PurchasePriceHistory priceHistory,
			List<PurchaseRowValue> purRowValueList) {
		boolean isAvailable = false;
		if (purchaseCategory.equals("MRP")) {
			purRowValueList.add(new PurchaseRowValue(priceHistory.getMaxRetailPrice()));
			return null;
		}
		List<PurchasePriceList> purPriceList = priceHistory.getPurPriceList();
		for (int i = 0; i < purPriceList.size(); i++) {
			if (purchaseCategory.equals(purPriceList.get(i).getQuantity())) {
				isAvailable = true;
				purRowValueList.add(new PurchaseRowValue(purPriceList.get(i).getNetRate()));
			}
		}
		if (!isAvailable) {
			purRowValueList.add(new PurchaseRowValue(null));
		}
		return null;
	}

	@Override
	@CacheEvict(cacheNames = { "productCategory", "productNames", "productInfo",
			"filterProductNames" }, allEntries = true)
	public boolean addOrUpdatePurchaseHistory(List<PurchaseHistory> purchaseHistList) {
		boolean returnStatus = false;
		try {
			purchaseHistList.forEach(hist -> checkAndUpdatePurchase(hist));
			returnStatus = true;
		} catch (Exception e) {
			logger.error("Failed to Add/Update record in DB {}", e.getMessage());
		}
		return returnStatus;
	}

	@Override
	public boolean checkPurchaseHistory(PurchaseHistory hist) {
		Map<String, Boolean> returnStatusMap = new HashMap<>();
		returnStatusMap.put("returnStatus", false);
		String newDate = dateFormatter.format(hist.getPurPriceHistory().get(0).getPurchaseDate());
		PurchaseHistory purHistory = purHistoryRepo.findByProductName(hist.getProductName());
		purHistory.getPurPriceHistory().forEach(purchase -> {
			String availableDate = dateFormatter.format(purchase.getPurchaseDate());
			if (availableDate.equals(newDate)) {
				returnStatusMap.put("returnStatus", true);
			}
		});
		return returnStatusMap.get("returnStatus");
	}

	private void checkAndUpdatePurchase(PurchaseHistory hist) {
		logger.info("Product name {}", hist.getProductName());
		PurchaseHistory purHistory = purHistoryRepo.findByProductName(hist.getProductName());
		checkAndUpdateLatestPurchase(hist, null);
		if (purHistory != null) {
			logger.info("Updating the record");
			hist.setId(purHistory.getId());
			purHistory.getPurPriceHistory().add(hist.getPurPriceHistory().get(0));
			hist.setPurPriceHistory(purHistory.getPurPriceHistory());
			hist.setPieceChangeFormula(purHistory.getPieceChangeFormula());
			hist.setKgChangeFormula(purHistory.getKgChangeFormula());
			hist.setDozenChangeFormula(purHistory.getDozenChangeFormula());
			hist.setSaramChangeFormula(purHistory.getSaramChangeFormula());
			hist.setBoxChangeFormula(purHistory.getBoxChangeFormula());
			hist.setBagChangeFormula(purHistory.getBagChangeFormula());
			hist.setLitreChangeFormula(purHistory.getLitreChangeFormula());
			hist.setBundleChangeFormula(purHistory.getBundleChangeFormula());
			purHistoryRepo.save(hist);
		} else {
			logger.info("Saving the record for 1st time");
			purHistoryRepo.insert(hist);
		}
	}

	public void checkAndUpdateLatestPurchase(PurchaseHistory hist, PurchaseHistory purchaseHistRecord) {
		LatestPurchaseHistory latestPurchase = new LatestPurchaseHistory();
		if (purchaseHistRecord != null) {
			logger.info("Product name purchaseHistRecord {}", purchaseHistRecord.getProductName());
			latestPurchase = latestPurchaseRepo.findByProductName(purchaseHistRecord.getProductName());
		} else {
			logger.info("Product name purchaseHistRecord else {}", hist.getProductName());
			latestPurchase = latestPurchaseRepo.findByProductName(hist.getProductName());
		}
		LatestPurchaseHistory prodLatest = new LatestPurchaseHistory();
		prodLatest.setProductName(hist.getProductName());
		prodLatest.setProductCategory(hist.getProductCategory());
		prodLatest.setProductAlias(hist.getProductAlias());
		prodLatest.setProductStatus(hist.isProductStatus());
		hist.getPurPriceHistory().forEach(action -> {
			prodLatest.setPurchaseDate(action.getPurchaseDate());
			prodLatest.setMaxRetailPrice(action.getMaxRetailPrice());
			action.getPurPriceList().forEach(priceList -> {
				logger.info(priceList.getQuantity() + " ------- " + priceList.getNetRate());
				switch (priceList.getQuantity()) {
				case "Piece":
					prodLatest.setPiecePrice(priceList.getNetRate());
					break;

				case "KG":
					prodLatest.setKgPrice(priceList.getNetRate());
					break;

				case "Dozen":
					prodLatest.setDozenPrice(priceList.getNetRate());
					break;

				case "Saram":
					prodLatest.setSaramPrice(priceList.getNetRate());
					break;

				case "Box":
					prodLatest.setBoxPrice(priceList.getNetRate());
					break;

				case "Bag":
					prodLatest.setBagPrice(priceList.getNetRate());
					break;

				case "Litre":
					prodLatest.setLitrePrice(priceList.getNetRate());
					break;

				case "Bundle":
					prodLatest.setBundlePrice(priceList.getNetRate());
					break;

				default:
					break;
				}
			});
		});

		if (latestPurchase != null) {
			logger.info("Updating latest purchase record for {}", prodLatest.getProductName());
			prodLatest.setId(latestPurchase.getId());
			latestPurchaseRepo.save(prodLatest);
		} else {
			logger.info("Saving {} for first time", prodLatest.getProductName());
			latestPurchaseRepo.insert(prodLatest);
		}

	}

	@Override
	@CacheEvict(cacheNames = { "productCategory", "productNames", "productInfo",
			"filterProductNames" }, allEntries = true)
	public boolean mergePurchaseHistory(String fromProdName, String toProdName) {
		boolean returnStatus = false;
		PurchaseHistory purchaseHistFrom = purHistoryRepo.findByProductName(fromProdName);
		PurchaseHistory purchaseHistTo = purHistoryRepo.findByProductName(toProdName);
		logger.info("Merging product from {} to {}", purchaseHistFrom.getProductName(),
				purchaseHistTo.getProductName());
		try {
			purchaseHistFrom.getPurPriceHistory().forEach(purHist -> {
				PurchaseHistory mergeTo = new PurchaseHistory();
				mergeTo.setProductName(purchaseHistTo.getProductName());
				mergeTo.setProductAlias(purchaseHistTo.getProductAlias());
				mergeTo.setProductCategory(purchaseHistTo.getProductCategory());
				mergeTo.setProductStatus(true);
				List<PurchasePriceHistory> purHistList = new ArrayList<>();
				purHistList.add(purHist);
				mergeTo.setPurPriceHistory(purHistList);
				checkAndUpdatePurchase(mergeTo);
			});
			logger.info("Deleting product {} after merging with {}", purchaseHistFrom.getProductName(),
					purchaseHistTo.getProductName());
			purHistoryRepo.delete(purchaseHistFrom);
			deleteLatestPurchaseEntry(purchaseHistFrom.getProductName());
			returnStatus = true;
		} catch (Exception e) {
			logger.error("Failed to merge {} with {}", purchaseHistFrom.getProductName(),
					purchaseHistTo.getProductName());
		}
		return returnStatus;
	}

	@Override
	@CacheEvict(cacheNames = { "productCategory", "productNames", "productInfo" }, allEntries = true)
	public void updatePurchaseCache() {
		logger.info("Updating caches for productCategory, productNames");
	}

	@Override
	public PurchaseHistory getLatestPurchaseHistory(String productName) {
		PurchaseHistory latestPurchase = new PurchaseHistory();
		List<PurchaseHistory> purchaseHistList = getPurchaseHistory(productName);
		if (!purchaseHistList.isEmpty()) {
			PurchaseHistory productPurchaseHist = purchaseHistList.get(0);
			latestPurchase.setId(productPurchaseHist.getId());
			latestPurchase.setProductName(productPurchaseHist.getProductName());
			latestPurchase.setProductCategory(productPurchaseHist.getProductCategory());
			latestPurchase.setProductAlias(productPurchaseHist.getProductAlias());
			latestPurchase.setProductStatus(productPurchaseHist.isProductStatus());
			List<PurchasePriceHistory> latestPurPriceList = new ArrayList<>();
			latestPurPriceList.add(productPurchaseHist.getPurPriceHistory().get(0));
			latestPurchase.setPurPriceHistory(latestPurPriceList);
		}
		return latestPurchase;
	}

	@Override
	@CacheEvict(cacheNames = { "productCategory", "productNames", "productInfo",
			"filterProductNames" }, allEntries = true)
	public boolean updateProductDetials(PurchaseHistory purchaseHist) {
		boolean updateStatus = false;
		PurchaseHistory updateProduct = new PurchaseHistory();
		Optional<PurchaseHistory> purchaseHistRecord = purHistoryRepo.findById(purchaseHist.getId());
		checkAndUpdateLatestPurchase(purchaseHist, purchaseHistRecord.get());
		if (purchaseHistRecord.isPresent()) {
			PurchaseHistory productPurchaseHist = purchaseHistRecord.get();
			updateProduct.setId(purchaseHist.getId());
			updateProduct.setProductName(purchaseHist.getProductName());
			updateProduct.setProductCategory(purchaseHist.getProductCategory());
			updateProduct.setProductAlias(purchaseHist.getProductAlias());
			updateProduct.setProductStatus(purchaseHist.isProductStatus());
			updateProduct.setPieceChangeFormula(productPurchaseHist.getPieceChangeFormula());
			updateProduct.setKgChangeFormula(productPurchaseHist.getKgChangeFormula());
			updateProduct.setDozenChangeFormula(productPurchaseHist.getDozenChangeFormula());
			updateProduct.setSaramChangeFormula(productPurchaseHist.getSaramChangeFormula());
			updateProduct.setBoxChangeFormula(productPurchaseHist.getBoxChangeFormula());
			updateProduct.setBagChangeFormula(productPurchaseHist.getBagChangeFormula());
			updateProduct.setLitreChangeFormula(productPurchaseHist.getLitreChangeFormula());
			updateProduct.setBundleChangeFormula(productPurchaseHist.getBundleChangeFormula());
			List<PurchaseHistory> latestPurHistoryList = getPurchaseHistory(productPurchaseHist.getProductName());
			List<PurchasePriceHistory> latestPurPriceList = latestPurHistoryList.get(0).getPurPriceHistory();
			if (latestPurPriceList != null && latestPurPriceList.size() == 1) {
				updateProduct.setPurPriceHistory(purchaseHist.getPurPriceHistory());
			} else {
				ObjectMapper Obj = new ObjectMapper();
				try {
					logger.debug("purchaseHist.getPurPriceHistory().get(0): {}",
							Obj.writeValueAsString(purchaseHist.getPurPriceHistory().get(0)));
					logger.debug("latestPurPriceList: {}", Obj.writeValueAsString(latestPurPriceList));
					logger.debug("latestPurPriceList: {}", Obj.writeValueAsString(latestPurPriceList.get(0)));
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
				latestPurPriceList.set(0, purchaseHist.getPurPriceHistory().get(0));
				updateProduct.setPurPriceHistory(latestPurPriceList);
			}
			try {
				purHistoryRepo.save(updateProduct);
				updateStatus = true;
			} catch (Exception e) {
				logger.error("Failed to update record in DB: {}", e.getMessage());
			}
		}
		return updateStatus;
	}

	// @Scheduled(cron = "${purge.purchase.history.cron}")
	public void purgeOldPurchaseHistory() {

	}

	private void deleteLatestPurchaseEntry(String productName) {
		LatestPurchaseHistory latestPurchase = latestPurchaseRepo.findByProductName(productName);
		if (latestPurchase != null) {
			latestPurchaseRepo.delete(latestPurchase);
		}
	}

	@Override
	@CacheEvict(cacheNames = { "productCategory", "productNames", "productInfo",
			"filterProductNames" }, allEntries = true)
	public boolean deleteProduct(List<PurchaseHistory> purchaseHist) {
		boolean returnStatus = false;
		try {
			purchaseHist.forEach(record -> {
				purHistoryRepo.delete(record);
				deleteLatestPurchaseEntry(record.getProductName());
			});
			returnStatus = true;
		} catch (Exception e) {
			logger.error("Failed to delete the record: {}", e.getMessage());
		}
		return returnStatus;
	}

	@Override
	public GoogleChartModel getPurchaseTrendData(String productName) {
		List<ColModel> columnModelList = new ArrayList<>();
		List<RowModel> rowModelList = new ArrayList<>();
		List<PurchaseHistory> purchaseHistoryList = getPurchaseHistory(productName);
		List<PurchasePriceHistory> latestPurPriceList = purchaseHistoryList.get(0).getPurPriceHistory();
		Set<String> purchaseCategory = new HashSet<String>();
		latestPurPriceList.forEach(
				action -> action.getPurPriceList().forEach(priceList -> purchaseCategory.add(priceList.getQuantity())));
		ColModel colModel = new ColModel("", "Date", "", "string");
		columnModelList.add(colModel);
		purchaseCategory.forEach(category -> {
			columnModelList.add(new ColModel("", category, "", "number"));
		});
		latestPurPriceList.forEach(action -> generateChartModel(action, purchaseCategory, rowModelList));
		GoogleChartModel googleChartModel = new GoogleChartModel(columnModelList, rowModelList);
		ObjectMapper Obj = new ObjectMapper();
		try {
			logger.info("googleChartModel: {}", Obj.writeValueAsString(googleChartModel));
		} catch (JsonProcessingException e) {
			logger.error("Failed to parse object {}", e.getMessage());
		}
		return googleChartModel;
	}

	private Object generateChartModel(PurchasePriceHistory action, Set<String> purchaseCategory,
			List<RowModel> rowModelList) {
		List<CModel> cModelList = new ArrayList<>();
		CModel cModel = new CModel(dateFormatter.format(action.getPurchaseDate()), null);
		cModelList.add(cModel);
		purchaseCategory.forEach(cat -> generatePurchasePrice(cat, action, cModelList));
		rowModelList.add(new RowModel(cModelList));
		return null;
	}

	private Object generatePurchasePrice(String purchaseCategory, PurchasePriceHistory priceHistory,
			List<CModel> cModelList) {
		boolean isAvailable = false;
		List<PurchasePriceList> purPriceList = priceHistory.getPurPriceList();
		for (int i = 0; i < purPriceList.size(); i++) {
			if (purchaseCategory.equals(purPriceList.get(i).getQuantity())) {
				isAvailable = true;
				cModelList.add(new CModel(purPriceList.get(i).getNetRate(), null));
			}
		}
		if (!isAvailable) {
			cModelList.add(new CModel(0, null));
		}
		return null;
	}

	private List<PurchaseHistory> getPurchaseHistorySorted(List<PurchaseHistory> purchaseHistoryList) {
		Collections.sort(purchaseHistoryList, Collections.reverseOrder());

		List<PurchaseHistory> purHistorySorted = purchaseHistoryList.stream()
				.sorted((o1, o2) -> o1.getProductName().compareTo(o2.getProductName())).map(purHist -> {
					List<PurchasePriceHistory> purPriceHistList = purHist.getPurPriceHistory().stream()
							.sorted((o1, o2) -> o2.getPurchaseDate().compareTo(o1.getPurchaseDate()))
							.collect(Collectors.toList());
					purHist.setPurPriceHistory(
							purPriceHistList.size() > 5
									? purPriceHistList.subList(0,
											Integer.valueOf(
													dashUtil.getAppConstant(DashboardConstants.NO_OF_LAST_PURCHASE)))
									: purPriceHistList);
					return purHist;
				}).collect(Collectors.toList());

		return purHistorySorted;
	}

	@SuppressWarnings("deprecation")
	private Date stringToDateFormatted(String dateString) {
		Date formattedDate = null;
		try {
			Date stringToDate = DashboardConstants.stringToDateFormatter.parse(dateString.replaceAll(".000Z", "+0000"));
			logger.info("stringToDate: " + stringToDate);
			formattedDate = new Date(DashboardConstants.dateFomatter.format(stringToDate));
		} catch (ParseException e) {
			e.printStackTrace();
			logger.error("Failed to convert String to date formatted {}", e.getMessage());
		}
		return formattedDate;
	}

	@Override
	public List<LatestPurchaseHistory> getPurchaseSearchResult(Map<String, Object> searchParameter) {
		Query query = new Query();
		if (searchParameter.get("searchCategory").equals("Purchase Price")) {
			switch (searchParameter.get("searchPriceQuantity").toString()) {
			case "Piece":
				query.addCriteria(Criteria.where("piecePrice")
						.gte(Integer.valueOf(searchParameter.get("searchPriceStart").toString()))
						.lte(Integer.valueOf(searchParameter.get("searchPriceEnd").toString())));
				break;

			case "KG":
				query.addCriteria(Criteria.where("kgPrice")
						.gte(Integer.valueOf(searchParameter.get("searchPriceStart").toString()))
						.lte(Integer.valueOf(searchParameter.get("searchPriceEnd").toString())));
				break;

			case "Dozen":
				query.addCriteria(Criteria.where("dozenPrice")
						.gte(Integer.valueOf(searchParameter.get("searchPriceStart").toString()))
						.lte(Integer.valueOf(searchParameter.get("searchPriceEnd").toString())));
				break;

			case "Saram":
				query.addCriteria(Criteria.where("saramPrice")
						.gte(Integer.valueOf(searchParameter.get("searchPriceStart").toString()))
						.lte(Integer.valueOf(searchParameter.get("searchPriceEnd").toString())));
				break;

			case "Box":
				query.addCriteria(Criteria.where("boxPrice")
						.gte(Integer.valueOf(searchParameter.get("searchPriceStart").toString()))
						.lte(Integer.valueOf(searchParameter.get("searchPriceEnd").toString())));
				break;

			case "Bag":
				query.addCriteria(Criteria.where("bagPrice")
						.gte(Integer.valueOf(searchParameter.get("searchPriceStart").toString()))
						.lte(Integer.valueOf(searchParameter.get("searchPriceEnd").toString())));
				break;

			case "Litre":
				query.addCriteria(Criteria.where("litrePrice")
						.gte(Integer.valueOf(searchParameter.get("searchPriceStart").toString()))
						.lte(Integer.valueOf(searchParameter.get("searchPriceEnd").toString())));
				break;

			case "Bundle":
				query.addCriteria(Criteria.where("bundlePrice")
						.gte(Integer.valueOf(searchParameter.get("searchPriceStart").toString()))
						.lte(Integer.valueOf(searchParameter.get("searchPriceEnd").toString())));
				break;

			default:
				break;
			}

			if (searchParameter.get("searchProductCategory") != null
					&& !"".equals(searchParameter.get("searchProductCategory"))) {
				query.addCriteria(Criteria.where("productCategory").is(searchParameter.get("searchProductCategory")));
			}
		} else {
			Date startDate = stringToDateFormatted(searchParameter.get("searchStartDate").toString());
			Date endDate = stringToDateFormatted(searchParameter.get("searchEndDate").toString());
			query.addCriteria(Criteria.where("purchaseDate").gte(startDate).lte(endDate));
		}

		List<LatestPurchaseHistory> resultPurHistList = mongoTemplate.find(query, LatestPurchaseHistory.class);

		return resultPurHistList;
	}

	private XSSFCellStyle createCellStyle(XSSFWorkbook workBook, boolean isHeader, boolean isDate, boolean isCurrency) {
		XSSFColor color = new XSSFColor(Color.WHITE, null);
		XSSFCellStyle cellStyle = workBook.createCellStyle();
		if (isHeader) {
			color = new XSSFColor(Color.LIGHT_GRAY, null);
			cellStyle.setAlignment(HorizontalAlignment.CENTER);
		} else {
			cellStyle.setAlignment(HorizontalAlignment.LEFT);
			cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		}
		cellStyle.setFillForegroundColor(color);
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		cellStyle.setBorderBottom(BorderStyle.THIN);
		cellStyle.setBorderLeft(BorderStyle.THIN);
		cellStyle.setBorderRight(BorderStyle.THIN);
		cellStyle.setBorderTop(BorderStyle.THIN);
		if (isDate) {
			cellStyle.setDataFormat(workBook.getCreationHelper().createDataFormat().getFormat("dd/mm/yyyy"));
		}
		if (isCurrency) {
			cellStyle.setDataFormat((short) 8);
		}
		return cellStyle;
	}

	private Integer populateHeader(XSSFCellStyle rowStyle, Sheet sheet, Integer rowNum) {
		Row row = sheet.createRow(rowNum++);
		int columNum = 0;
		for (int i = 0; i < headerDetails.size(); i++) {
			Cell cell = row.createCell(columNum++);
			cell.setCellValue(headerDetails.get(i));
			cell.setCellStyle(rowStyle);
		}
		return rowNum;
	}

	private void setPurchasePrice(Cell purchaseDataCell, String header, List<PurchasePriceList> priceList,
			XSSFCellStyle rowStyle) {
		priceList.forEach(price -> {
			if (price.getQuantity().equals(header)) {
				purchaseDataCell.setCellValue(price.getNetRate());
			}
		});
		purchaseDataCell.setCellStyle(rowStyle);
	}

	private void setMergedCellBorder(Cell cell, CellRangeAddress range, Sheet sheet, XSSFCellStyle rowStyle) {
		RegionUtil.setBorderBottom(BorderStyle.THIN, range, sheet);
		RegionUtil.setBorderTop(BorderStyle.THIN, range, sheet);
		RegionUtil.setBorderLeft(BorderStyle.THIN, range, sheet);
		RegionUtil.setBorderRight(BorderStyle.THIN, range, sheet);
		cell.setCellStyle(rowStyle);
	}

	private void setBordersToMergedCells(Sheet sheet) {
		int numMerged = sheet.getNumMergedRegions();
		for (int i = 0; i < numMerged; i++) {
			CellRangeAddress mergedRegions = sheet.getMergedRegion(i);
			RegionUtil.setBorderLeft(BorderStyle.THIN, mergedRegions, sheet);
			RegionUtil.setBorderRight(BorderStyle.THIN, mergedRegions, sheet);
			RegionUtil.setBorderTop(BorderStyle.THIN, mergedRegions, sheet);
			RegionUtil.setBorderBottom(BorderStyle.THIN, mergedRegions, sheet);

		}
	}

	private CellRangeAddress getMergeCellRange(int rowNum, int columnNum, int purchaseHistLength) {
		CellRangeAddress cellRange = new CellRangeAddress(rowNum - 1, (rowNum - 1) + (purchaseHistLength - 1),
				columnNum - 1, columnNum - 1);
		return cellRange;
	}

	private Integer populateRowValue(XSSFWorkbook workBook, XSSFCellStyle rowStyle, PurchaseHistory purchaseHist,
			Sheet sheet, Integer rowNum) {
		int columNum = 0;
		List<PurchasePriceHistory> purchasePriceHistList = purchaseHist.getPurPriceHistory();
		int purchaseHistLength = purchasePriceHistList.size();
		Row row = sheet.createRow(rowNum++);
		for (int i = 0; i < commonHeaderDetails.size(); i++) {
			logger.debug("purchaseHist.getProductName(): {}", purchaseHist.getProductName());
			Cell cell = row.createCell(columNum++);
			switch (commonHeaderDetails.get(i)) {
			case "Product Name":
				cell.setCellValue(purchaseHist.getProductName());
				if (purchaseHistLength > 1) {
					CellRangeAddress cellRange = getMergeCellRange(rowNum, columNum, purchaseHistLength);
					setMergedCellBorder(cell, cellRange, sheet, rowStyle);
					sheet.addMergedRegion(cellRange);
				} else {
					cell.setCellStyle(rowStyle);
				}
				break;

			case "Alias":
				cell.setCellValue(purchaseHist.getProductAlias());
				if (purchaseHistLength > 1) {
					CellRangeAddress cellRange = getMergeCellRange(rowNum, columNum, purchaseHistLength);
					setMergedCellBorder(cell, cellRange, sheet, rowStyle);
					sheet.addMergedRegion(cellRange);
				} else {
					cell.setCellStyle(rowStyle);
				}
				break;

			case "Category":
				cell.setCellValue(purchaseHist.getProductCategory());
				if (purchaseHistLength > 1) {
					CellRangeAddress cellRange = getMergeCellRange(rowNum, columNum, purchaseHistLength);
					setMergedCellBorder(cell, cellRange, sheet, rowStyle);
					sheet.addMergedRegion(cellRange);
				} else {
					cell.setCellStyle(rowStyle);
				}
				break;

			case "Status":
				cell.setCellValue(purchaseHist.isProductStatus());
				if (purchaseHistLength > 1) {
					CellRangeAddress cellRange = getMergeCellRange(rowNum, columNum, purchaseHistLength);
					setMergedCellBorder(cell, cellRange, sheet, rowStyle);
					sheet.addMergedRegion(cellRange);
				} else {
					cell.setCellStyle(rowStyle);
				}
				break;

			}
		}

		for (int j = 0; j < purchasePriceHistList.size(); j++) {
			if (j > 0) {
				row = sheet.createRow(rowNum++);
			}
			int priceStartingColum = columNum;
			XSSFCellStyle priceCellStyle = createCellStyle(workBook, false, false, false);
			for (int k = 0; k < priceHeaderDetails.size(); k++) {
				switch (priceHeaderDetails.get(k)) {
				case "Purchase Date":
					Cell purchasePriceCell = row.createCell(priceStartingColum++);
					purchasePriceCell.setCellValue(purchasePriceHistList.get(j).getPurchaseDate());
					purchasePriceCell.setCellStyle(createCellStyle(workBook, false, true, false));
					break;

				case "MRP":
					Cell mrpCell = row.createCell(priceStartingColum++);
					if (purchasePriceHistList.get(j).getMaxRetailPrice() != null) {
						mrpCell.setCellValue(purchasePriceHistList.get(j).getMaxRetailPrice());
					}
					mrpCell.setCellStyle(priceCellStyle);
					break;

				case "Piece":
					Cell pieceCell = row.createCell(priceStartingColum++);
					setPurchasePrice(pieceCell, priceHeaderDetails.get(k),
							purchasePriceHistList.get(j).getPurPriceList(), priceCellStyle);
					break;

				case "KG":
					Cell kgCell = row.createCell(priceStartingColum++);
					setPurchasePrice(kgCell, priceHeaderDetails.get(k), purchasePriceHistList.get(j).getPurPriceList(),
							priceCellStyle);
					break;

				case "Dozen":
					Cell dozenCell = row.createCell(priceStartingColum++);
					setPurchasePrice(dozenCell, priceHeaderDetails.get(k),
							purchasePriceHistList.get(j).getPurPriceList(), priceCellStyle);
					break;

				case "Saram":
					Cell saramCell = row.createCell(priceStartingColum++);
					setPurchasePrice(saramCell, priceHeaderDetails.get(k),
							purchasePriceHistList.get(j).getPurPriceList(), priceCellStyle);
					break;

				case "Box":
					Cell boxCell = row.createCell(priceStartingColum++);
					setPurchasePrice(boxCell, priceHeaderDetails.get(k), purchasePriceHistList.get(j).getPurPriceList(),
							priceCellStyle);
					break;

				case "Bag":
					Cell bagCell = row.createCell(priceStartingColum++);
					setPurchasePrice(bagCell, priceHeaderDetails.get(k), purchasePriceHistList.get(j).getPurPriceList(),
							priceCellStyle);
					break;

				case "Litre":
					Cell litreCell = row.createCell(priceStartingColum++);
					setPurchasePrice(litreCell, priceHeaderDetails.get(k),
							purchasePriceHistList.get(j).getPurPriceList(), priceCellStyle);
					break;

				case "Bundle":
					Cell bundleCell = row.createCell(priceStartingColum++);
					setPurchasePrice(bundleCell, priceHeaderDetails.get(k),
							purchasePriceHistList.get(j).getPurPriceList(), priceCellStyle);
					break;
				}
			}

		}
		return rowNum;
	}

	public ByteArrayOutputStream exportProductDetails(List<PurchaseHistory> purchaseHistory) {
		ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
		XSSFWorkbook workBook = new XSSFWorkbook();
		try {
			Integer rowNum = 0;
			XSSFCellStyle headerStyle = createCellStyle(workBook, true, false, false);
			Sheet sheet = workBook.createSheet("Purchase History");
			rowNum = populateHeader(headerStyle, sheet, rowNum);
			for (int i = 0; i < purchaseHistory.size(); i++) {
				XSSFCellStyle rowCellStyle = createCellStyle(workBook, false, false, false);
				rowNum = populateRowValue(workBook, rowCellStyle, purchaseHistory.get(i), sheet, rowNum);
			}
			sheet.createFreezePane(0, 1);
			for (int i = 0; i < headerDetails.size(); i++) {
				sheet.autoSizeColumn(i);
			}
			setBordersToMergedCells(sheet);
			sheet.setAutoFilter(
					new CellRangeAddress(0, sheet.getLastRowNum(), 0, sheet.getRow(0).getLastCellNum() - 1));
			workBook.write(byteArrayStream);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				workBook.close();
			} catch (IOException e) {
				logger.error("Failed to close the object {}", e.getMessage());
			}
		}
		return byteArrayStream;
	}

	@Override
	@Scheduled(cron = "${purchase.export.frequency}")
	public void exportPurchaseHistoryAsExcel() {
		logger.info("Fetching purchase history details");
		List<PurchaseHistory> purchaseHistory = getPurchaseHistory(null);
		logger.info("Purchase history size {}", purchaseHistory.size());
		logger.info("Started creating excel sheet");
		ByteArrayOutputStream byteArrayStream = exportProductDetails(purchaseHistory);
		try (OutputStream outputStream = new FileOutputStream(
				System.getProperty("user.dir") + "/PurchaseHistory.xlsx")) {
			byteArrayStream.writeTo(outputStream);
			dashUtil.googleDriveBackup("PurchaseHistory.xlsx", System.getProperty("user.dir") + "/PurchaseHistory.xlsx",
					MediaType.APPLICATION_OCTET_STREAM.toString());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (byteArrayStream != null) {
				try {
					byteArrayStream.close();
				} catch (IOException e) {
					logger.error("Failed to close the object {}", e.getMessage());
				}
			}
		}
	}

	@Override
	public PurchaseFieldChange getPurchaseFieldFomula(String productName) {
		PurchaseHistory purchaseHistory = purHistoryRepo.findByProductName(productName);
		if(purchaseHistory == null) {
			return null;
		}
		if (purchaseHistory.getPieceChangeFormula() == null && purchaseHistory.getKgChangeFormula() == null
				&& purchaseHistory.getDozenChangeFormula() == null && purchaseHistory.getSaramChangeFormula() == null
				&& purchaseHistory.getBoxChangeFormula() == null && purchaseHistory.getBagChangeFormula() == null
				&& purchaseHistory.getLitreChangeFormula() == null
				&& purchaseHistory.getBundleChangeFormula() == null) {
			return new PurchaseFieldChange();
		} else {
			PurchaseFieldChange fieldChangeFormula = new PurchaseFieldChange(purchaseHistory.getId(),
					purchaseHistory.getPieceChangeFormula(), purchaseHistory.getKgChangeFormula(),
					purchaseHistory.getDozenChangeFormula(), purchaseHistory.getSaramChangeFormula(),
					purchaseHistory.getBoxChangeFormula(), purchaseHistory.getBagChangeFormula(),
					purchaseHistory.getLitreChangeFormula(), purchaseHistory.getBundleChangeFormula());
			return fieldChangeFormula;
		}

	}

	@Override
	public boolean savePurchaseFieldFomula(PurchaseFieldChange purchaseFieldFormula) {
		Optional<PurchaseHistory> purchaseHistory = purHistoryRepo.findById(purchaseFieldFormula.getProductObjId());
		purchaseHistory.ifPresent(purchase -> {
			purchase.setPieceChangeFormula(purchaseFieldFormula.getPieceChangeFormula());
			purchase.setKgChangeFormula(purchaseFieldFormula.getKgChangeFormula());
			purchase.setDozenChangeFormula(purchaseFieldFormula.getDozenChangeFormula());
			purchase.setSaramChangeFormula(purchaseFieldFormula.getSaramChangeFormula());
			purchase.setBoxChangeFormula(purchaseFieldFormula.getBoxChangeFormula());
			purchase.setBagChangeFormula(purchaseFieldFormula.getBagChangeFormula());
			purchase.setLitreChangeFormula(purchaseFieldFormula.getLitreChangeFormula());
			purchase.setBundleChangeFormula(purchaseFieldFormula.getBundleChangeFormula());
			purHistoryRepo.save(purchase);
		});
		return true;
	}

	@Override
	public void migratePurchaseFormula() {
		fieldChangeRepo.findAll().forEach(formula -> {
			Optional<PurchaseHistory> purchaseHistory = purHistoryRepo.findById(formula.getProductObjId());
			purchaseHistory.ifPresent(purchase -> {
				logger.info(purchase.getProductName());
				purchase.setPieceChangeFormula(formula.getPieceChangeFormula());
				purchase.setKgChangeFormula(formula.getKgChangeFormula());
				purchase.setDozenChangeFormula(formula.getDozenChangeFormula());
				purchase.setSaramChangeFormula(formula.getSaramChangeFormula());
				purchase.setBoxChangeFormula(formula.getBoxChangeFormula());
				purchase.setBagChangeFormula(formula.getBagChangeFormula());
				purchase.setLitreChangeFormula(formula.getLitreChangeFormula());
				purchase.setBundleChangeFormula(formula.getBundleChangeFormula());
				purHistoryRepo.save(purchase);
			});
		});
	}
}
