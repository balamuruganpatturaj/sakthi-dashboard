package com.sakthi.groups.dashboard.service;

import java.util.List;

import com.sakthi.groups.dashboard.bean.SettlementReport;
import com.sakthi.groups.dashboard.bean.UpdateSettlementData;
import com.sakthi.groups.dashboard.model.EdcPosDetail;
import com.sakthi.groups.dashboard.model.ReversalReport;
import com.sakthi.groups.dashboard.model.SettlementDetail;

public interface SettlementMgmtService {

	List<EdcPosDetail> getAvailablePOS(boolean isActive);

	String createNewPOSEntry(EdcPosDetail edcPosDetail);

	boolean saveDailySettlement(SettlementDetail settlementDetail);

	List<SettlementReport> getSettlementReport();

	boolean checkSettlementStatus(SettlementDetail settlementDetail);

	boolean updateDailySettlement(UpdateSettlementData updateSettlementData);

	String editPOSEntry(EdcPosDetail edcPosDetail);

	List<ReversalReport> getReversalReport();

	boolean updateReversalRefund(ReversalReport reversalReport);

	boolean editDailySettlement(SettlementDetail settlementDetail);

}
