package com.sakthi.groups.dashboard.service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sakthi.groups.dashboard.dao.BatchDeliveryRepository;
import com.sakthi.groups.dashboard.dao.CustomerRepository;
import com.sakthi.groups.dashboard.model.BatchChangeProvided;
import com.sakthi.groups.dashboard.model.BatchDeliveryDetails;
import com.sakthi.groups.dashboard.model.BatchDeliveryOrder;
import com.sakthi.groups.dashboard.model.CustomerMaster;
import com.sakthi.groups.dashboard.util.DashboardConstants;
import com.sakthi.groups.migration.portal.dao.DeliveryDetailRepository;
import com.sakthi.groups.migration.portal.model.DeliveryOrderDetail;

@Service
public class DeliveryManageServiceImpl implements DeliveryManageService {

	@Autowired
	private DeliveryDetailRepository deliveryRepo;

	@Autowired
	private CustomerRepository customerRepo;

	@Autowired
	private BatchDeliveryRepository batchDeliveryRepo;

	@Autowired
	private MongoTemplate mongoTemplate;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private Double totalBillAmount;

	private static Set<String> phoneNumbers = new HashSet<>();

	@Override
	public List<DeliveryOrderDetail> getdeliveryOrders() {
		return deliveryRepo.findAll(Sort.by(Sort.Direction.DESC, "orderPlacedDate"));
	}

	@Override
	public List<DeliveryOrderDetail> getPendingdeliveryOrders() {
		return deliveryRepo.findByDeliveryStatusOrderByOrderPlacedDateDesc(DashboardConstants.ORDER_PLACED);
	}

	@Override
	public Set<String> getPhoneNumbers(String phoneNumber) {
		Set<String> phoneNumbersSet = new HashSet<>();
		phoneNumbers = getPhoneNumbers();
		phoneNumbersSet = phoneNumbers.stream().filter(number -> number.contains(phoneNumber))
				.collect(Collectors.toSet());
		return phoneNumbersSet;
	}

	@Override
	@Cacheable("phoneNumbers")
	public Set<String> getPhoneNumbers() {
		Set<String> phoneNumbersSet = new HashSet<>();
		List<DeliveryOrderDetail> deliveryOrderList = deliveryRepo.findAll();
		deliveryOrderList.forEach(entries -> phoneNumbersSet.add(entries.getPhoneNumber()));

		List<CustomerMaster> customerInfoList = customerRepo.findAll();
		customerInfoList.forEach(entry -> {
			entry.getCustomerPhoneNumber().forEach(phoneEntry -> {
				String phoneEntryReplace = phoneEntry.replaceAll("[0-9]", "");
				if (phoneEntryReplace.equalsIgnoreCase("") && !phoneEntry.equalsIgnoreCase("")) {
					try {
						phoneNumbersSet.add(phoneEntry);
					} catch (Exception e) {
						System.out.println("phoneEntryReplace: /" + phoneEntryReplace + "/");
						System.out.println("phoneEntry: /" + phoneEntry + "/");
						e.printStackTrace();
					}
				}
			});
		});
		return phoneNumbersSet;
	}

	@Override
	@Cacheable("filterPhoneNumber")
	public Set<String> getFilteredPhoneNum(String phoneNumber) {
		Set<String> filteredPhoneNum = new HashSet<>();
		Query query = new Query();
		Criteria phoneCriteria = Criteria.where("$where").is("/" + phoneNumber + ".*/.test(this.phoneNumber)");
		query.addCriteria(phoneCriteria);
		List<DeliveryOrderDetail> resultOrderList = mongoTemplate.find(query, DeliveryOrderDetail.class);
		resultOrderList.forEach(entries -> filteredPhoneNum.add(entries.getPhoneNumber()));
		return filteredPhoneNum;
	}

	@SuppressWarnings("deprecation")
	@Override
	@CacheEvict(cacheNames = { "filterPhoneNumber", "phoneNumbers" }, allEntries = true)
	public boolean addDeliveryOrderDetail(List<DeliveryOrderDetail> deliveryOrderList) {
		boolean returnStatus = false;
		ObjectMapper Obj = new ObjectMapper();
		try {
			deliveryOrderList.forEach(order -> {
				try {
					System.out.println("order: " + Obj.writeValueAsString(order));
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
				logger.info("order: {}", order.toString());
				logger.info("Saving the record for 1st time");
				order.setPhoneNumber(order.getPhoneNumber());
				order.setOrderPlacedDate(new Date(DashboardConstants.dateFomatter.format(order.getOrderPlacedDate())));
				deliveryRepo.insert(order);
			});
			returnStatus = true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Failed to Add/Update record in DB {}", e.getMessage());
		}
		return returnStatus;
	}

	@Override
	public HashMap<String, String> getCustomerInfo(String phoneNumber) {
		List<String> phoneList = new ArrayList<>();
		HashMap<String, String> customerInfo = new HashMap<>();
		phoneList.add(phoneNumber);
		List<CustomerMaster> customerInfoList = customerRepo.findByCustomerPhoneNumberIn(phoneList);
		if (customerInfoList != null && !customerInfoList.isEmpty()) {
			customerInfo.put("customerAddress", customerInfoList.get(0).getCustomerAddress());
			customerInfo.put("customerName", customerInfoList.get(0).getCustomerName());
		} else {
			List<DeliveryOrderDetail> deliverOrderList = deliveryRepo.findByPhoneNumber(phoneNumber);
			if (deliverOrderList != null && !deliverOrderList.isEmpty()) {
				customerInfo.put("customerAddress", deliverOrderList.get(0).getCustomerAddress());
				customerInfo.put("customerName", deliverOrderList.get(0).getCustomerName());
			}
		}
		return customerInfo;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean checkDeliveryOrder(String phoneNumber, Date orderPlacedDate) {
		boolean returnStatus = false;
		try {
			Date orderDateFomatted = new Date(DashboardConstants.dateFomatter.format(orderPlacedDate));
			DeliveryOrderDetail deliveryOrder = deliveryRepo.findByPhoneNumberAndOrderPlacedDate(phoneNumber,
					orderDateFomatted);
			if (deliveryOrder != null) {
				returnStatus = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Failed to parse the string to date {}", e.getMessage());
		}
		return returnStatus;
	}

	@Override
	public boolean checkDeliveryOrder(String phoneNumber, Integer billNo) {
		boolean returnStatus = false;
		try {
			DeliveryOrderDetail deliveryOrder = deliveryRepo.findByPhoneNumberAndBillNo(phoneNumber, billNo);
			if (deliveryOrder != null) {
				returnStatus = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Failed to parse the string to date {}", e.getMessage());
		}
		return returnStatus;
	}

	@Override
	public String checkSimilarDeliveryOrder(String phoneNumber) {
		String returnStatus = "";
		String returnString = "Bill No %s has already processed for mobile number %s. Please verify and proceed";
		Query query = new Query();
		Date currentDate = new Date();
		LocalDateTime localDateTime = currentDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		localDateTime = localDateTime.minusDays(3);
		Date startDate = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
		Date endDate = currentDate;
		query.addCriteria(Criteria.where("orderPlacedDate").gte(startDate).lte(endDate));
		query.addCriteria(Criteria.where("phoneNumber").is(phoneNumber));
		List<DeliveryOrderDetail> deliveryOrderList = mongoTemplate.find(query, DeliveryOrderDetail.class);
		if (deliveryOrderList != null && !deliveryOrderList.isEmpty()) {
			returnStatus = String.format(returnString, deliveryOrderList.get(0).getBillNo(), phoneNumber);
		}
		return returnStatus;
	}

	@SuppressWarnings("deprecation")
	@Override
	@CacheEvict(cacheNames = { "filterPhoneNumber", "phoneNumbers" }, allEntries = true)
	public boolean updateDeliveryOrder(DeliveryOrderDetail deliveryOrder) {
		boolean returnStatus = false;
		try {
			deliveryOrder.setOrderPlacedDate(
					new Date(DashboardConstants.dateFomatter.format(deliveryOrder.getOrderPlacedDate())));
			deliveryRepo.save(deliveryOrder);
			if (deliveryOrder.getBatchNumber() != null) {
				updateBatchDelivery(deliveryOrder);
			}
			returnStatus = true;
		} catch (Exception e) {
			logger.error("Problem with updating delivery order in DB {} ", e.getMessage());
		}
		return returnStatus;
	}

	private void updateBatchDelivery(DeliveryOrderDetail deliveryOrder) {
		logger.info("Invoking updateBatchDelivery for {}", deliveryOrder.getPhoneNumber());
		BatchDeliveryDetails deliveryDetail = batchDeliveryRepo.findByBatchNumber(deliveryOrder.getBatchNumber());
		List<BatchDeliveryOrder> batchDeliveryList = deliveryDetail.getBatchDeliveryList();
		logger.info("batchDeliveryList size {}", batchDeliveryList.size());
		deliveryDetail.getBatchDeliveryList().forEach(entry -> {
			Integer listIndex = batchDeliveryList.indexOf(entry);
			if (deliveryOrder.getPhoneNumber().equals(entry.getPhoneNumber())) {
				if (!entry.isDelivered()) {
					logger.info("Delivery Status {} {}", deliveryOrder.getDeliveryStatus(),
							deliveryOrder.getPaymentMode());
					if ((deliveryOrder.getDeliveryStatus().equals(DashboardConstants.ORDER_DELIVERED)
							&& !deliveryOrder.getDeliveryStatus().equals(DashboardConstants.PAYMENT_PENDING))
							&& ((!deliveryOrder.getPaymentMode().equals(DashboardConstants.PAYMENT_PENDING)
									&& !deliveryOrder.getDeliveryStatus().equals(DashboardConstants.PARTIALLY_PAID)))) {
						Double billAmountPaid = deliveryDetail.getPaidBillAmount() + deliveryOrder.getPayment();
						Double balanceAmount = deliveryDetail.getTotalAmount() - billAmountPaid;
						deliveryDetail.setPaidBillAmount(billAmountPaid);
						deliveryDetail.setBalanceAmount(balanceAmount);
						BatchDeliveryOrder batchDeliveryOrder = new BatchDeliveryOrder(deliveryOrder.getPhoneNumber(),
								deliveryOrder.getOrderPlacedDate(), deliveryOrder.getBillNo(),
								deliveryOrder.getBillAmount(), true);
						batchDeliveryList.set(listIndex, batchDeliveryOrder);
						deliveryDetail.setBatchDeliveryList(batchDeliveryList);
						batchDeliveryRepo.save(deliveryDetail);
					}
				} else {
					if (!deliveryOrder.getBillAmount().equals(entry.getBillAmount())) {
						logger.info(
								"Assuming that the value of amount would not be changes after status moved to Out for Delivery");
					}
				}
			}
		});
	}

	@Override
	@CacheEvict(cacheNames = { "filterPhoneNumber", "phoneNumbers" }, allEntries = true)
	public boolean updateDeliveryOrderList(List<DeliveryOrderDetail> deliveryOrder, UUID batchId) {
		boolean returnStatus = false;
		try {
			deliveryOrder.forEach(order -> {
				if (batchId != null) {
					order.setBatchNumber(batchId);
				}
				updateDeliveryOrder(order);
			});
			returnStatus = true;
		} catch (Exception e) {
			logger.error("Problem with updating delivery order in DB {} ", e.getMessage());
		}
		return returnStatus;
	}

	@Override
	@SuppressWarnings("deprecation")
	@CacheEvict(cacheNames = { "filterPhoneNumber", "phoneNumbers" }, allEntries = true)
	public boolean updatebatchDeliveryOrder(List<DeliveryOrderDetail> deliveryOrder, Double denominationTotal,
			List<BatchChangeProvided> changesProvided) {
		boolean returnStatus = false;
		List<BatchDeliveryOrder> batchDeliverOrderList = new ArrayList<>();
		this.totalBillAmount = 0.0;
		UUID batchId = UUID.randomUUID();
		returnStatus = this.updateDeliveryOrderList(deliveryOrder, batchId);
		deliveryOrder.forEach(order -> {
			BatchDeliveryOrder batchDeliveryOrder = new BatchDeliveryOrder(order.getPhoneNumber(),
					order.getOrderPlacedDate(), order.getBillNo(), order.getBillAmount(), false);
			this.totalBillAmount += order.getBillAmount();
			batchDeliverOrderList.add(batchDeliveryOrder);
		});
		Date batchDate = new Date(DashboardConstants.dateFomatter.format(new Date()));
		BatchDeliveryDetails batchDetails = new BatchDeliveryDetails(batchId, batchDate, deliveryOrder.size(),
				this.totalBillAmount, denominationTotal, 0.0, this.totalBillAmount + denominationTotal, "OPEN", 0.0, "",
				"", batchDeliverOrderList, changesProvided);
		batchDeliveryRepo.save(batchDetails);
		return returnStatus;
	}

	@Override
	public List<BatchDeliveryDetails> getBatchdeliveryOrders() {
		return batchDeliveryRepo.findByBatchStatusOrderByDeliveryDateDesc(DashboardConstants.BATCH_OPEN);
	}

	@Override
	public List<DeliveryOrderDetail> getBatchDeliveryOrders(UUID batchId) {
		return deliveryRepo.findByBatchNumber(batchId);
	}

	@Override
	public boolean closeDeliveryBatch(UUID batchId) {
		boolean returnStatus = false;
		BatchDeliveryDetails batchDetail = batchDeliveryRepo.findByBatchNumber(batchId);
		batchDetail.setBatchStatus(DashboardConstants.BATCH_CLOSED);
		batchDeliveryRepo.save(batchDetail);
		return returnStatus;
	}

}
