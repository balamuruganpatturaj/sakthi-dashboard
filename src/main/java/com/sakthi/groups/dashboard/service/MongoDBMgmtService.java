package com.sakthi.groups.dashboard.service;

public interface MongoDBMgmtService {

	public void mongoDbBackup();
	
	public void googleDriveBackup();
}
