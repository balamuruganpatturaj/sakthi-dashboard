package com.sakthi.groups.dashboard.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.sakthi.groups.dashboard.dao.ProductRepository;
import com.sakthi.groups.migration.portal.model.ProductMaster;

@Service
public class SalesMgmtServiceImpl implements SalesMgmtService {

	@Autowired
	private ProductRepository productRepo;

	@Autowired
	private MongoTemplate mongoTemplate;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public List<ProductMaster> getProductDetails(String productSearch) {
		logger.info("String pattern entered {}", productSearch);
		List<ProductMaster> prodMasterList = productRepo.findByProductAliasIgnoreCase(productSearch);
		if (prodMasterList != null && !prodMasterList.isEmpty()) {
			return prodMasterList;
		} else {
			Query query = new Query();
			Criteria nameCriteria = Criteria.where("productName").regex(productSearch.replace(" ", ".*"), "i");
			Criteria aliasCriteria = Criteria.where("productAlias").regex(productSearch, "i");
			Criteria filterCriteria = new Criteria().orOperator(nameCriteria, aliasCriteria);
			query.addCriteria(filterCriteria);
			prodMasterList = mongoTemplate.find(query, ProductMaster.class);
			return prodMasterList;
		}
	}

}
