package com.sakthi.groups.dashboard.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.sakthi.groups.migration.portal.model.BillInfo;
import com.sakthi.groups.migration.portal.model.BillItemDetails;
import com.sakthi.groups.migration.portal.model.BillSearchCriteria;
import com.sakthi.groups.migration.service.DataMigrationService;
import com.sakthi.groups.migration.service.DeliveryBillSyncService;
import com.sakthi.groups.migration.service.PurchaseEntryMigration;

@Service
public class DataSyncServiceImpl implements DataSyncService {

	@Autowired
	private DataMigrationService dataMigrationService;
	
	@Autowired
	private PurchaseEntryMigration purchaseEntryService;
	
	@Autowired
	private DeliveryBillSyncService deliverBillService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@CacheEvict(cacheNames = { "customers", "phoneNumbers", "filterPhoneNumber", "deliveryorder", "phoneNumbers",
			"pendingorder", "supplierNames", "supplierDetails" }, allEntries = true)
	@Override
	public boolean syncSuppliers(boolean incrementalSync) {
		boolean syncStatus = dataMigrationService.migrateSupplierDetails(false);
		return syncStatus;
	}

	@CacheEvict(cacheNames = { "customers", "phoneNumbers", "filterPhoneNumber", "deliveryorder", "phoneNumbers",
			"pendingorder", "supplierNames", "supplierDetails" }, allEntries = true)
	@Override
	public boolean syncCustomers(boolean incrementalSync) {
		boolean syncStatus = dataMigrationService.migrateCustomerDetails(false);
		return syncStatus;
	}

	@CacheEvict(cacheNames = { "customers", "phoneNumbers", "filterPhoneNumber", "deliveryorder", "phoneNumbers",
			"pendingorder", "supplierNames", "supplierDetails" }, allEntries = true)
	@Override
	public boolean syncProducts(boolean incrementalSync) {
		boolean syncStatus = dataMigrationService.migrateProductDetails(false);
		return syncStatus;
	}
	
	@Override
	@Scheduled(cron = "${mongo.data.sync.frequency}")
	public boolean syncPurchaseEntry() {
		boolean syncStatus = purchaseEntryService.migratePurchaseEntry(false);
		return syncStatus;
	}

	@Scheduled(cron = "${mongo.data.sync.frequency}")
	@CacheEvict(cacheNames = { "customers", "phoneNumbers", "filterPhoneNumber", "deliveryorder", "phoneNumbers",
			"pendingorder", "supplierNames", "supplierDetails" }, allEntries = true)
	public void goFrugalDataSync() {
		logger.info("Cron started to sync data from GoFrugal");
		boolean syncSupplierStatus = this.syncSuppliers(false);
		boolean syncCustomerStatus = this.syncCustomers(false);
		boolean syncProductStatus = this.syncProducts(false);
		if (syncSupplierStatus && syncCustomerStatus && syncProductStatus) {
			logger.info("Data sync from GoFrugal completed successfully");
		} else {
			logger.error("Failed to sync data from GoFrugal");
		}
	}

	@Scheduled(cron = "${delivery.billamt.sync.frequency}")
	public void syncDeliveryBillAmount() {
		logger.info("Cron started to sync delivery bill amounts");
		deliverBillService.syncDeliveryBills();
		logger.info("Sync completed for delivery bill amounts");
	}

	@Override
	public List<BillInfo> getBillInfoList(BillSearchCriteria searchCriteria) {
		return dataMigrationService.getBillInfo(searchCriteria);
	}

	@Override
	public List<BillItemDetails> getBillDetailsList(Integer billNo, boolean isQuotationBill) {
		return dataMigrationService.getBillItemDetails(billNo, isQuotationBill);
	}

	@Override
	public BillInfo getSingleBillInfo(Integer billNo, boolean isQuotationBill) {
		BillSearchCriteria searchCriteria = new BillSearchCriteria();
		searchCriteria.setBillNumber(billNo);
		searchCriteria.setQuotationBill(isQuotationBill);
		List<BillInfo> billInfoList = getBillInfoList(searchCriteria);
		return billInfoList.get(0);
	}

	@Override
	public Double getBillAmount(Integer mBillNo, String phoneNumber) {
		return dataMigrationService.getBillAmount(mBillNo, phoneNumber);
	}

	@Override
	public Integer getPBillNumber(Integer billNo, String phoneNo) {
		return dataMigrationService.getPBillNumber(billNo, phoneNo);
	}

	@Override
	public List<String> getCounterList() {
		return dataMigrationService.getCounterName();
	}
}
