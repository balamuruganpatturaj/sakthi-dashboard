package com.sakthi.groups.dashboard.service;

import java.io.InputStream;

public interface RodeoDataVerifyService {

	public byte[] getMissingProducts(InputStream excelStream, Integer daysToCheck, String chooseItemFrom);

}
