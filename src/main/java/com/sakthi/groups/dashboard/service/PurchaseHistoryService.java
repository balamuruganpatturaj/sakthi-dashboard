package com.sakthi.groups.dashboard.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sakthi.groups.dashboard.bean.ProductInfo;
import com.sakthi.groups.dashboard.bean.PurchaseHistoryData;
import com.sakthi.groups.dashboard.model.LatestPurchaseHistory;
import com.sakthi.groups.dashboard.model.PurchaseFieldChange;
import com.sakthi.groups.dashboard.model.PurchaseHistory;
import com.sakthi.groups.dashboard.model.chart.GoogleChartModel;

public interface PurchaseHistoryService {

	List<String> getProductNames();

	List<PurchaseHistory> getPurchaseHistory(String productName);

	boolean addOrUpdatePurchaseHistory(List<PurchaseHistory> purchaseHistList);

	Set<String> getProductCategory(String prodName);

	void updatePurchaseCache();

	PurchaseHistory getLatestPurchaseHistory(String productName);

	boolean updateProductDetials(PurchaseHistory purchaseHist);

	boolean deleteProduct(List<PurchaseHistory> purchaseHist);

	GoogleChartModel getPurchaseTrendData(String productName);

	ProductInfo getProductInfo(String prodName);

	List<LatestPurchaseHistory> getPurchaseSearchResult(Map<String, Object> searchParameter);

	List<String> getFilteredProductNames(String productName);

	boolean mergePurchaseHistory(String fromProdName, String toProdName);

	void checkAndUpdateLatestPurchase(PurchaseHistory purchaseHistFrom, PurchaseHistory purchaseHistRecord);

	List<PurchaseHistoryData> getPurchaseHistoryData(String productName);

	void exportPurchaseHistoryAsExcel();

	boolean checkPurchaseHistory(PurchaseHistory hist);

	PurchaseFieldChange getPurchaseFieldFomula(String productName);

	boolean savePurchaseFieldFomula(PurchaseFieldChange purchaseFieldFormula);
	
	void migratePurchaseFormula();

}
