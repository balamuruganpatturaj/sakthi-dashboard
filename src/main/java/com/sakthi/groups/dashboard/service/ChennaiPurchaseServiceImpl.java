package com.sakthi.groups.dashboard.service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.PatternSyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.sakthi.groups.dashboard.bean.ChennaiProductInfo;
import com.sakthi.groups.dashboard.dao.ChennaiPurchaseRepository;
import com.sakthi.groups.dashboard.model.ChennaiPurchase;
import com.sakthi.groups.dashboard.util.DashboardConstants;
import com.sakthi.groups.migration.portal.dao.ProductEanCodeRepository;
import com.sakthi.groups.migration.portal.model.ProductEanCode;

@Service
public class ChennaiPurchaseServiceImpl implements ChennaiPurchaseService {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private ChennaiPurchaseRepository chennaiPurRepo;

	@Autowired
	private ProductEanCodeRepository eanCodeRepo;

	@Autowired
	private PurchaseHistoryService purchaseService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public Set<String> getFilteredProductNames(String productName) {
		logger.debug("Query for {} ", productName);
		Set<String> filteredProduct = new HashSet<>();
		Query query = new Query();
		Criteria nameCriteria = new Criteria();
		try {
			nameCriteria = Criteria.where("productName").regex(productName.replace(" ", ".*"), "i");
		} catch (PatternSyntaxException e) {
			logger.error("Exception occurred in creating query {}", e.getMessage());
		}
		Criteria filterCriteria = new Criteria().orOperator(nameCriteria);
		query.addCriteria(filterCriteria);
		List<ChennaiPurchase> resultPurchaseList = mongoTemplate.find(query, ChennaiPurchase.class);
		resultPurchaseList.forEach(entries -> filteredProduct.add(entries.getProductName()));
		purchaseService.getFilteredProductNames(productName).forEach(entry -> filteredProduct.add(entry));
		return filteredProduct;
	}

	@Override
	public ChennaiProductInfo getEanCodeProductInfo(String eanCode) {
		ChennaiPurchase latestPurchase = new ChennaiPurchase();
		ChennaiProductInfo chennaiProdInfo = new ChennaiProductInfo();
		List<ChennaiPurchase> chennaiPurchaseList = chennaiPurRepo.findByEancodeOrderByPurchaseDateDesc(eanCode);
		if (chennaiPurchaseList != null && !chennaiPurchaseList.isEmpty()) {
			latestPurchase = chennaiPurchaseList.get(0);
			chennaiProdInfo = new ChennaiProductInfo(latestPurchase.getProductName(),
					latestPurchase.getMaxRetailPrice(), latestPurchase.getUnitPerCase(), latestPurchase.getEancode());
		} else {
			chennaiProdInfo = new ChennaiProductInfo(eanCode, null, null, eanCode);
		}
		return chennaiProdInfo;
	}

	@Override
	public ChennaiProductInfo getProductInfo(String productName) {
		ChennaiProductInfo chennaiProdInfo = new ChennaiProductInfo();
		List<ChennaiPurchase> chennaiPurList = chennaiPurRepo.findByProductNameOrderByPurchaseDateDesc(productName);
		if (chennaiPurList != null && !chennaiPurList.isEmpty()) {
			ChennaiPurchase latestPurchase = chennaiPurList.get(0);
			chennaiProdInfo = new ChennaiProductInfo(latestPurchase.getProductName(),
					latestPurchase.getMaxRetailPrice(), latestPurchase.getUnitPerCase(), latestPurchase.getEancode());
		}
		return chennaiProdInfo;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean updateChennaiPurchase(List<ChennaiPurchase> chennaiPurchaseList) {
		boolean returnStatus = false;
		chennaiPurchaseList.forEach(purchase -> {
			purchase.setItemCode(getProductItemCode(purchase.getEancode()));
			purchase.setPurchaseDate((new Date(DashboardConstants.dateFomatter.format(purchase.getPurchaseDate()))));
			chennaiPurRepo.save(purchase);
		});
		returnStatus = true;
		return returnStatus;
	}

	private Integer getProductItemCode(String eanCode) {
		Integer itemCode = null;
		if (eanCode != null && !eanCode.equalsIgnoreCase("")) {
			ProductEanCode prodEanCode = eanCodeRepo.findByProductEANCode(eanCode);
			if (prodEanCode != null) {
				itemCode = prodEanCode.getProductCode();
			}
		}
		return itemCode;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean saveChennaiPurchase(List<ChennaiPurchase> chennaiPurchaseList) {
		boolean returnStatus = false;
		chennaiPurchaseList.forEach(purchase -> {
			Query query = new Query();
			query.addCriteria(Criteria.where("productName").is(purchase.getProductName()));
			query.addCriteria(Criteria.where("purchaseDate")
					.is(new Date(DashboardConstants.dateFomatter.format(purchase.getPurchaseDate()))));
			query.addCriteria(Criteria.where("maxRetailPrice").is(purchase.getMaxRetailPrice()));
			query.addCriteria(Criteria.where("quantityValue").is(purchase.getQuantityValue()));
			List<ChennaiPurchase> chennaiPurchase = mongoTemplate.find(query, ChennaiPurchase.class);
			purchase.setItemCode(getProductItemCode(purchase.getEancode()));
			if (chennaiPurchase != null && !chennaiPurchase.isEmpty()) {
				ChennaiPurchase latestChennaiPurchase = chennaiPurchase.get(0);
				latestChennaiPurchase.setQuantity(purchase.getQuantity() + latestChennaiPurchase.getQuantity());
				StringBuffer buffer = new StringBuffer();
				if (purchase.getRemarks() != null) {
					buffer.append(purchase.getRemarks() + ", ");
				}
				if (latestChennaiPurchase.getRemarks() != null) {
					buffer.append(latestChennaiPurchase.getRemarks());
				}

				latestChennaiPurchase.setRemarks(buffer.toString());
				chennaiPurRepo.save(latestChennaiPurchase);
			} else {
				purchase.setPurchaseDate(
						(new Date(DashboardConstants.dateFomatter.format(purchase.getPurchaseDate()))));
				chennaiPurRepo.save(purchase);
			}
		});
		returnStatus = true;
		return returnStatus;
	}

	@Override
	public boolean deleteProduct(List<ChennaiPurchase> chennaiPurchaseList) {
		boolean returnStatus = false;
		chennaiPurchaseList.forEach(purchase -> chennaiPurRepo.delete(purchase));
		returnStatus = true;
		return returnStatus;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean isPurchaseAlreadyAdded(ChennaiPurchase chennaiPurchase) {
		boolean returnStatus = false;
		List<ChennaiPurchase> chennaiPurList = chennaiPurRepo.findByProductNameAndPurchaseDate(
				chennaiPurchase.getProductName(),
				new Date(DashboardConstants.dateFomatter.format(chennaiPurchase.getPurchaseDate())));
		if (chennaiPurList != null && !chennaiPurList.isEmpty()) {
			returnStatus = true;
		}
		return returnStatus;
	}

	@Override
	public List<ChennaiPurchase> getChennaiPurchase(String productName, String dateSlotValue) {
		List<ChennaiPurchase> chennaiPurchaseList = new ArrayList<>();
		if (productName == null || "".equals(productName)) {
			LocalDate currentDate = LocalDate.now();
			if (dateSlotValue != null && !dateSlotValue.equalsIgnoreCase("ALL")) {
				LocalDate startingDate = currentDate.plusDays(-Long.valueOf(dateSlotValue));
				Date purchaseSlot = Date.from(startingDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
				chennaiPurchaseList = chennaiPurRepo.findByPurchaseDateGreaterThanOrderByPurchaseDateDesc(purchaseSlot);
			} else {
				chennaiPurchaseList = chennaiPurRepo.findAll(Sort.by(Sort.Direction.DESC, "purchaseDate"));
			}
		} else {
			chennaiPurchaseList = chennaiPurRepo.findByProductNameOrderByPurchaseDateDesc(productName);
		}
		return chennaiPurchaseList;
	}
}
