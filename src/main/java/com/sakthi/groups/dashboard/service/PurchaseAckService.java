package com.sakthi.groups.dashboard.service;

import java.util.Date;
import java.util.List;

import com.sakthi.groups.dashboard.bean.SupplierNames;
import com.sakthi.groups.dashboard.model.InvoiceDetails;
import com.sakthi.groups.dashboard.model.SupplierMaster;

public interface PurchaseAckService {

	List<SupplierNames> getSupplierNames();

	List<SupplierMaster> getSupplierDetails();

	boolean saveInvoiceDetails(List<InvoiceDetails> invoiceDetails);

	boolean checkInvoiceAvailable(String supplierName, String invoiceNo, Date invoiceDate);

	boolean createNewSupplier(SupplierMaster supplierDetails, String loggedInUser);

	boolean editSupplier(SupplierMaster supplierDetails, String loggedInUser);

}
