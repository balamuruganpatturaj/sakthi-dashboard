package com.sakthi.groups.dashboard.service;

import java.util.List;

import com.sakthi.groups.dashboard.model.Company;

public interface CompanyService {

	List<Company> getAvailableCompany(boolean isActive);

	String createNewCompany(Company company);

	String editCompany(Company company);

	Company getAvailableCompanyDetail(boolean isActive, String companyName);

}
