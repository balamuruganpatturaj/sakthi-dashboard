package com.sakthi.groups.dashboard.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.NumberUtils;
import org.bson.types.Binary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.sakthi.groups.dashboard.bean.OfferInputDetail;
import com.sakthi.groups.dashboard.dao.CustomerRepository;
import com.sakthi.groups.dashboard.dao.EmployeeRepository;
import com.sakthi.groups.dashboard.dao.GiftDetailsRepository;
import com.sakthi.groups.dashboard.dao.OfferDetailsRepository;
import com.sakthi.groups.dashboard.model.CustomerMaster;
import com.sakthi.groups.dashboard.model.EmployeeMaster;
import com.sakthi.groups.dashboard.model.GiftDetails;
import com.sakthi.groups.dashboard.model.OfferDetails;
import com.sakthi.groups.dashboard.util.DashboardConstants;
import com.sakthi.groups.dashboard.util.DashboardUtil;
import com.sakthi.groups.migration.portal.model.BillAnalyticsData;
import com.sakthi.groups.migration.portal.model.BillInfo;
import com.sakthi.groups.migration.portal.model.BillSearchCriteria;
import com.sakthi.groups.migration.portal.model.TopGrossTrendData;
import com.sakthi.groups.migration.portal.model.TopSellingTrendData;
import com.sakthi.groups.migration.service.BillDataMigration;
import com.sakthi.groups.migration.service.DataMigrationService;

@Service
public class PeopleMgmtServiceImpl implements PeopleMgmtService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private CustomerRepository customerRepo;

	@Autowired
	private OfferDetailsRepository offerRepo;

	@Autowired
	private GiftDetailsRepository giftRepo;

	@Autowired
	private EmployeeRepository empRepo;

	@Autowired
	private DashboardUtil dashUtil;

	@Autowired
	private DataMigrationService dataMigrationService;

	@Autowired
	private BillDataMigration billDataMigration;

	private static Set<BigInteger> phoneNumbers = new HashSet<>();

	@Override
	@Cacheable("customers")
	public List<CustomerMaster> getCustomerDetails() {
		return customerRepo.findAll(Sort.by(Sort.Direction.ASC, "customerName"));
	}

	@Override
	@CacheEvict(cacheNames = { "customers" }, allEntries = true)
	public boolean saveNewCustomer(CustomerMaster customer, String remoteUser) {
		boolean returnStatus = false;
		return returnStatus;
	}

	@Override
	@CacheEvict(cacheNames = { "customers" }, allEntries = true)
	public boolean editCustomer(CustomerMaster customer, String remoteUser) {
		boolean returnStatus = false;
		return returnStatus;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean addCustomerOffer(OfferDetails offerDetails) {
		boolean returnStatus = false;
		try {
			offerDetails.setPurchaseDate(
					(new Date(DashboardConstants.dateFomatter.format(offerDetails.getPurchaseDate()))));
			offerRepo.save(offerDetails);
			returnStatus = true;
		} catch (Exception e) {
			logger.error("Failed to store the data in DB {}", e.getMessage());
		}
		return returnStatus;
	}

	@SuppressWarnings("deprecation")
	@Override
	public String checkCustomerOffer(OfferDetails offerDetails) {
		String returnStatus = "false";
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, Integer.valueOf(dashUtil.getAppConstant(DashboardConstants.THRESHOLD_DATE)));
		System.out.println("Date = " + cal.getTime());
		try {
			Date purchaseDateFormatted = new Date(
					DashboardConstants.dateFomatter.format(offerDetails.getPurchaseDate()));
			OfferDetails offerUsed = offerRepo.findByCustomerObjIdAndOfferTypeAndPurchaseDateBetween(
					offerDetails.getCustomerObjId(), offerDetails.getOfferType(), cal.getTime(), purchaseDateFormatted);
			if (offerUsed != null) {
				returnStatus = offerUsed.getOfferType() + " already been availed on "
						+ DashboardConstants.dateFomatter.format(offerUsed.getPurchaseDate());
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Failed to parse the string to date {}", e.getMessage());
		}
		return returnStatus;
	}

	private BillInfo getTodayBillInfo(Integer billNo) {
		BillSearchCriteria searchCriteria = new BillSearchCriteria();
		searchCriteria.setBillNumber(billNo);
		searchCriteria.setQuotationBill(false);
		searchCriteria.setPBillSearch(true);
		String currentDate = DashboardConstants.stringToDateFormatter.format(new Date());
		searchCriteria.setBillStartDate(currentDate);
		searchCriteria.setBillEndDate(currentDate);
		List<BillInfo> billInfoList = dataMigrationService.getBillInfo(searchCriteria);
		return billInfoList.get(0);
	}

	private String getPhoneNumber(BillInfo todayBillInfo) {
		String phoneNumber = "";
		if (todayBillInfo.getPhoneNo2() != null && !".".equalsIgnoreCase(todayBillInfo.getPhoneNo2())) {
			phoneNumber = todayBillInfo.getPhoneNo2();
		} else if (todayBillInfo.getPhoneNo3() != null && !".".equalsIgnoreCase(todayBillInfo.getPhoneNo3())) {
			phoneNumber = todayBillInfo.getPhoneNo3();
		} else if (todayBillInfo.getPhoneNo1() != null && !".".equalsIgnoreCase(todayBillInfo.getPhoneNo1())) {
			phoneNumber = todayBillInfo.getPhoneNo1();
		}
		return phoneNumber;
	}

	@SuppressWarnings("deprecation")
	private GiftDetails generateGiftDetailsBean(OfferInputDetail offerInputDetail)
			throws CustomerDetailsNotFoundException {
		GiftDetails giftDetails = new GiftDetails();
		List<String> phoneNumberList = new ArrayList<>();
		if (NumberUtils.isNumber(offerInputDetail.getSearchInput())) {
			if (offerInputDetail.getSearchInput().length() > 9) {
				giftDetails.setPhoneNumber(new BigInteger(offerInputDetail.getSearchInput()));
				phoneNumberList.add(offerInputDetail.getSearchInput());
				List<CustomerMaster> customerMasterList = customerRepo.findByCustomerPhoneNumberIn(phoneNumberList);
				if (customerMasterList != null && !customerMasterList.isEmpty()) {
					giftDetails.setCustomerName(customerMasterList.get(0).getCustomerName());
				}
			} else {
				Integer billNo = Integer.parseInt(offerInputDetail.getSearchInput());
				BillInfo todayBillInfo = getTodayBillInfo(billNo);
				if (todayBillInfo.getCustomerName() == null || "".equals(todayBillInfo.getCustomerName())) {
					throw new CustomerDetailsNotFoundException("Bill doesn't have customer information to proceed");
				}
				giftDetails.setCustomerName(todayBillInfo.getCustomerName());
				giftDetails.setPhoneNumber(new BigInteger(getPhoneNumber(todayBillInfo)));
			}
		} else if (offerInputDetail.getSearchInput().startsWith("S")
				|| offerInputDetail.getSearchInput().startsWith("s")) {
			CustomerMaster customerMaster = customerRepo.findByCustomerIdIgnoreCase(offerInputDetail.getSearchInput());
			if (customerMaster != null) {
				giftDetails.setCustomerName(customerMaster.getCustomerName());
				String customerPhone = (customerMaster.getCustomerPhoneNumber().get(0) == null
						|| "".equals(customerMaster.getCustomerPhoneNumber().get(0)))
								? customerMaster.getCustomerPhoneNumber().get(1)
								: customerMaster.getCustomerPhoneNumber().get(0);
				giftDetails.setPhoneNumber(new BigInteger(customerPhone));
			}
		} else {
			String inputSubString = offerInputDetail.getSearchInput().substring(2);
			if (NumberUtils.isNumber(inputSubString)) {
				Integer billNo = Integer.parseInt(inputSubString);
				BillInfo todayBillInfo = getTodayBillInfo(billNo);
				if (todayBillInfo.getCustomerName() == null || "".equals(todayBillInfo.getCustomerName())) {
					throw new CustomerDetailsNotFoundException("Bill doesn't have customer information to proceed");
				}
				giftDetails.setCustomerName(todayBillInfo.getCustomerName());
				giftDetails.setPhoneNumber(new BigInteger(getPhoneNumber(todayBillInfo)));
			}
		}
		giftDetails.setIssuedDate((new Date(DashboardConstants.dateFomatter.format(offerInputDetail.getIssuedDate()))));
		giftDetails.setOfferType(offerInputDetail.getOfferType());
		giftDetails.setOfferQuantity(offerInputDetail.getOfferQuantity());
		giftDetails.setOfferCategory(offerInputDetail.getOfferCategory());
		return giftDetails;
	}

	@Override
	@CacheEvict(cacheNames = { "issuedOffer" }, allEntries = true)
	public boolean addGiftOffer(OfferInputDetail offerInputDetail) {
		boolean returnStatus = false;
		try {
			GiftDetails giftDetails = generateGiftDetailsBean(offerInputDetail);
			giftRepo.save(giftDetails);
			returnStatus = true;
		} catch (Exception e) {
			logger.error("Failed to store the data in DB {}", e.getMessage());
		}
		return returnStatus;
	}

	@Override
	@SuppressWarnings("deprecation")
	public String checkGiftOffer(OfferInputDetail offerInputDetail) {
		String returnStatus = "false";
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, Integer.valueOf(dashUtil.getAppConstant(DashboardConstants.THRESHOLD_DATE)));
		System.out.println("Date = " + cal.getTime());
		try {
			GiftDetails giftDetails = generateGiftDetailsBean(offerInputDetail);
			System.out.println("giftDetails: " + giftDetails);
			Date purchaseDateFormatted = new Date(DashboardConstants.dateFomatter.format(giftDetails.getIssuedDate()));
			System.out.println("purchaseDateFormatted = " + purchaseDateFormatted);
			GiftDetails offerUsed = giftRepo.findByphoneNumberdAndOfferTypeAndIssuedeBetween(
					giftDetails.getPhoneNumber(), giftDetails.getOfferType(), cal.getTime(), purchaseDateFormatted);
			if (offerUsed != null) {
				returnStatus = offerUsed.getOfferQuantity() + " " + offerUsed.getOfferType()
						+ " already been availed on "
						+ DashboardConstants.dateFomatter.format(offerUsed.getIssuedDate());
			}
		} catch (IncorrectResultSizeDataAccessException dataException) {
			returnStatus = "Multiple records found and assumed customer already availed the offer";
			logger.error("Failed to parse the string to date {}", dataException.getMessage());
		} catch (CustomerDetailsNotFoundException custNotFound) {
			returnStatus = custNotFound.getMessage();
			logger.error("Failed to parse the string to date {}", custNotFound.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Failed to parse the string to date {}", e.getMessage());
		}
		return returnStatus;
	}

	@Override
	@Cacheable("issuedOffer")
	public List<GiftDetails> getIssuedOffers() {
		return giftRepo.findAll(Sort.by(Sort.Direction.DESC, "issuedDate"));
	}

	@Override
	@Cacheable("employees")
	public List<EmployeeMaster> getEmployeeDetails() {
		return empRepo.findAll();
	}

	@Override
	@CacheEvict(cacheNames = { "employees" }, allEntries = true)
	public boolean saveNewEmployee(EmployeeMaster employee, BigInteger unModifiedPhNo, String remoteUser) {
		boolean returnStatus = false;
		EmployeeMaster empRecord = new EmployeeMaster();
		try {
			if (unModifiedPhNo != null) {
				empRecord = empRepo.findByPhoneNumber(unModifiedPhNo);
			} else {
				empRecord = empRepo.findByPhoneNumber(employee.getPhoneNumber());
			}
			if (empRecord == null) {
				empRepo.save(employee);
			} else {
				employee.setId(empRecord.getId());
				if (employee.getAddressProof() == null) {
					employee.setAddressProof(empRecord.getAddressProof());
					employee.setProofFileName(empRecord.getProofFileName());
				}
				empRepo.save(employee);
			}
			returnStatus = true;
		} catch (Exception e) {
			logger.error("Failed to store the data in DB {}", e.getMessage());
		}
		return returnStatus;
	}

	@Override
	@CacheEvict(cacheNames = { "employees" }, allEntries = true)
	public boolean editEmployee(EmployeeMaster employee, String remoteUser) {
		return false;
	}

	@Override
	public Binary getAddressProof(BigInteger phoneNumber) {
		EmployeeMaster empDetails = empRepo.findByPhoneNumber(phoneNumber);
		return empDetails.getAddressProof();
	}

	@Override
	public Set<String> getCustomerId(String searchParam) {
		Set<String> customerIdSet = new HashSet<>();
		CustomerMaster customerDetail = customerRepo.findByCustomerIdIgnoreCase(searchParam);
		if (customerDetail != null) {
			customerIdSet.add(customerDetail.getCustomerId());
		}
		return customerIdSet;
	}

	@Override
	public Set<BigInteger> getCustomerPhoneNum(String searchParam) {
		BigInteger phoneBigInt = new BigInteger(searchParam);
		Set<BigInteger> phoneNumbers = getPhoneNumbers(phoneBigInt);
		return phoneNumbers;
	}

	@Override
	public Set<BigInteger> getPhoneNumbers(BigInteger phoneNumber) {
		Set<BigInteger> phoneNumbersSet = new HashSet<>();
		phoneNumbers = getPhoneNumbers();
		phoneNumbersSet = phoneNumbers.stream().filter(number -> number.toString().contains(phoneNumber.toString()))
				.collect(Collectors.toSet());
		return phoneNumbersSet;
	}

	@Override
	@Cacheable("phoneNumbers")
	public Set<BigInteger> getPhoneNumbers() {
		Set<BigInteger> phoneNumbersSet = new HashSet<>();

		List<CustomerMaster> customerInfoList = customerRepo.findAll();
		customerInfoList.forEach(entry -> {
			entry.getCustomerPhoneNumber().forEach(phoneEntry -> {
				String phoneEntryReplace = phoneEntry.replaceAll("[0-9]", "");
				if (phoneEntryReplace.equalsIgnoreCase("") && !phoneEntry.equalsIgnoreCase("")) {
					try {
						BigInteger phoneBigInt = new BigInteger(phoneEntry);
						phoneNumbersSet.add(phoneBigInt);
					} catch (Exception e) {
						System.out.println("phoneEntryReplace: /" + phoneEntryReplace + "/");
						System.out.println("phoneEntry: /" + phoneEntry + "/");
						e.printStackTrace();
					}
				}
			});
		});
		return phoneNumbersSet;
	}

	@Override
	public CustomerMaster getCustomerDetail(String searchParam) {
		List<String> phoneNumberList = new ArrayList<>();
		phoneNumberList.add(searchParam.toString());
		CustomerMaster customerDetail = customerRepo.findByCustomerIdIgnoreCaseOrCustomerPhoneNumberIn(searchParam,
				phoneNumberList);
		return customerDetail;
	}

	@Override
	public List<String> getCustomerNames() {
		List<String> customerNameList = new ArrayList<>();
		List<CustomerMaster> customerList = customerRepo.findAll(Sort.by(Sort.Direction.ASC, "customerName"));
		customerList.forEach(customer -> {
			customerNameList.add(customer.getCustomerName());
			customerNameList.add(customer.getCustomerCode().toString());
			customerNameList.addAll(customer.getCustomerPhoneNumber());
		});
		return customerNameList;
	}

	private BillSearchCriteria getSearchCriteria(String customerCode, Date startDate, Date endDate) {
		BillSearchCriteria searchCriteria = new BillSearchCriteria();
		searchCriteria.setQuotationBill(false);
		searchCriteria.setPBillSearch(true);
		searchCriteria.setCustomerName(customerCode);
		String startDateStr = DashboardConstants.stringToDateFormatter.format(startDate);
		String endDateStr = DashboardConstants.stringToDateFormatter.format(endDate);
		searchCriteria.setBillStartDate(startDateStr);
		searchCriteria.setBillEndDate(endDateStr);
		return searchCriteria;
	}

	private BillAnalyticsData getBillInfo(String customerCode, Date startDate, Date endDate) {
		BillSearchCriteria searchCriteria = this.getSearchCriteria(customerCode, startDate, endDate);
		List<BillAnalyticsData> analyticsDataList = billDataMigration.getBillAnalyticsData(searchCriteria);
		return analyticsDataList != null ? analyticsDataList.get(0) : null;
	}

	

	private BillAnalyticsData invokeDurationSpecific(String customerCode, String searchDuration) {
		Integer productCount = 0;
		BillAnalyticsData billAnalyticData = new BillAnalyticsData();
		HashMap<String, Date> dateMap = this.dashUtil.getSpecifiedDates(searchDuration);
		billAnalyticData = this.getBillInfo(customerCode, dateMap.get("startDate"), dateMap.get("endDate"));
		productCount = billDataMigration.getProductCount(
				this.getSearchCriteria(customerCode, dateMap.get("startDate"), dateMap.get("endDate")));
		billAnalyticData.setProductCount(productCount);
		return billAnalyticData;
	}

	@Override
	public BillAnalyticsData getCustomerAnalyticsData(String customerCode, String searchDuration, Date startDate,
			Date endDate) {
		BillAnalyticsData billAnalyticData = new BillAnalyticsData();
		Integer productCount = 0;
		if (startDate != null && endDate != null) {
			billAnalyticData = this.getBillInfo(customerCode, startDate, endDate);
			productCount = billDataMigration.getProductCount(this.getSearchCriteria(customerCode, startDate, endDate));
			billAnalyticData.setProductCount(productCount);
		} else {
			switch (searchDuration) {
			case "current_month":
				billAnalyticData = this.invokeDurationSpecific(customerCode, searchDuration);
				break;

			case "last_month":
				billAnalyticData = this.invokeDurationSpecific(customerCode, searchDuration);
				break;

			case "current_year":
				billAnalyticData = this.invokeDurationSpecific(customerCode, searchDuration);
				break;

			case "last_year":
				billAnalyticData = this.invokeDurationSpecific(customerCode, searchDuration);
				break;

			default:
				break;
			}
		}
		return billAnalyticData;
	}

	private List<TopSellingTrendData> invokeDurationSpecificTrend(String customerCode, String searchParam) {
		List<TopSellingTrendData> trendData = new ArrayList<TopSellingTrendData>();
		HashMap<String, Date> dateMap = this.dashUtil.getSpecifiedDates(searchParam);
		trendData = billDataMigration.getTopSellingProduct(
				this.getSearchCriteria(customerCode, dateMap.get("startDate"), dateMap.get("endDate")));
		return trendData;
	}

	@Override
	public List<TopSellingTrendData> getCustomerTrendData(String customerCode, String searchParam, Date searchStartDate,
			Date searchEndDate) {
		List<TopSellingTrendData> trendData = new ArrayList<TopSellingTrendData>();
		if (searchStartDate != null && searchEndDate != null) {
			trendData = billDataMigration
					.getTopSellingProduct(this.getSearchCriteria(customerCode, searchStartDate, searchEndDate));
		} else {
			switch (searchParam) {
			case "current_month":
				trendData = this.invokeDurationSpecificTrend(customerCode, searchParam);
				break;

			case "last_month":
				trendData = this.invokeDurationSpecificTrend(customerCode, searchParam);
				break;

			case "current_year":
				trendData = this.invokeDurationSpecificTrend(customerCode, searchParam);
				break;

			case "last_year":
				trendData = this.invokeDurationSpecificTrend(customerCode, searchParam);
				break;

			default:
				break;
			}
		}
		return trendData;
	}

	private List<TopGrossTrendData> invokeDurationSpecificGross(String customerCode, String searchParam) {
		List<TopGrossTrendData> grossTrendData = new ArrayList<TopGrossTrendData>();
		HashMap<String, Date> dateMap = this.dashUtil.getSpecifiedDates(searchParam);
		grossTrendData = billDataMigration.getTopGrossProduct(
				this.getSearchCriteria(customerCode, dateMap.get("startDate"), dateMap.get("endDate")));
		return grossTrendData;
	}

	@Override
	public List<TopGrossTrendData> getCustomerGrossTrendData(String customerCode, String searchParam,
			Date searchStartDate, Date searchEndDate) {
		List<TopGrossTrendData> grossTrendData = new ArrayList<TopGrossTrendData>();
		if (searchStartDate != null && searchEndDate != null) {
			grossTrendData = billDataMigration
					.getTopGrossProduct(this.getSearchCriteria(customerCode, searchStartDate, searchEndDate));
		} else {
			switch (searchParam) {
			case "current_month":
				grossTrendData = this.invokeDurationSpecificGross(customerCode, searchParam);
				break;

			case "last_month":
				grossTrendData = this.invokeDurationSpecificGross(customerCode, searchParam);
				break;

			case "current_year":
				grossTrendData = this.invokeDurationSpecificGross(customerCode, searchParam);
				break;

			case "last_year":
				grossTrendData = this.invokeDurationSpecificGross(customerCode, searchParam);
				break;

			default:
				break;
			}
		}
		return grossTrendData;
	}
}
