package com.sakthi.groups.dashboard.service;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.google.api.client.http.FileContent;
import com.google.api.services.drive.Drive;
import com.sakthi.groups.dashboard.util.DashboardConstants;
import com.sakthi.groups.dashboard.util.DashboardUtil;

@Service
public class MongoDBMgmtServiceImpl implements MongoDBMgmtService {

	@Value("${mongo.backup.name}")
	private String backupName;

	@Autowired
	private DashboardUtil util;

	String exportFileName = null;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private String findMongoExportFilePath() {
		DashboardConstants.MONGO_EXPORT_PATH.stream().forEach(path -> {
			File exportPathFile = new File(path + File.separator + DashboardConstants.MONGO_EXPORT_CMD);
			if (exportPathFile.exists()) {
				exportFileName = exportPathFile.getAbsolutePath();
			}
		});
		return exportFileName;
	}

	@Override
	public void mongoDbBackup() {
		String exportFileCmd = findMongoExportFilePath();
		logger.info("Mongo DB export command path {}", exportFileCmd);
		java.io.File filePath = new java.io.File(System.getProperty("user.dir") + File.separator + backupName);
		if (filePath.exists()) {
			filePath.delete();
		}
		String archiveCommand = String.format("--archive=%s",
				System.getProperty("user.dir") + File.separator + backupName);
		logger.debug("archiveCommand {}", archiveCommand);
		List<String> cmdArgs = Arrays.asList(exportFileCmd, archiveCommand, "--gzip", "--db",
				DashboardConstants.MONGO_DB_NAME);
		ProcessBuilder builder = new ProcessBuilder(cmdArgs).redirectErrorStream(true)
				.directory(new File(System.getProperty("user.dir")));
		try {
			Process process = builder.inheritIO().start();
			logger.info("Waiting for backup to complete");
			process.waitFor();
			int exitValue = process.exitValue();
			if (exitValue == 0) {
				logger.info("Backup created successfully : {}", backupName);
			}
		} catch (IOException | InterruptedException e) {
			logger.error("Failed to take Mongo DB backup");
		}
	}

	@Override
	public void googleDriveBackup() {
		Drive googleDriveService = util.getGoogleDriveService();
		logger.info("googleDriveBackup () googleDriveService: " + googleDriveService);
		String fileId = util.getFileId(googleDriveService, backupName);
		com.google.api.services.drive.model.File fileMetadata = new com.google.api.services.drive.model.File();
		fileMetadata.setName(backupName);
		java.io.File filePath = new java.io.File(System.getProperty("user.dir") + File.separator + backupName);
		logger.info("Backup FilePath: {}", filePath.getAbsolutePath());
		FileContent mediaContent = new FileContent("application/gzip", filePath);
		com.google.api.services.drive.model.File file = null;

		try {
			if ("id".equalsIgnoreCase(fileId)) {
				file = googleDriveService.files().create(fileMetadata, mediaContent).setFields(fileId).execute();
			} else {
				file = googleDriveService.files().update(fileId, fileMetadata, mediaContent).execute();
			}
			logger.info("File uploaded Successfully with file ID {}", file.getId());
		} catch (IOException e1) {
			logger.error("Failed to upload the backup file to Google Drive");
		} finally {
			if (filePath.exists()) {
				filePath.delete();
			}
		}
	}

	@Scheduled(cron = "${mongo.backup.frequency}")
	public void backupAndUploadFile() {
		mongoDbBackup();
		googleDriveBackup();
	}
}
