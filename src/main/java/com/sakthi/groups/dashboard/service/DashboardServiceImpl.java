package com.sakthi.groups.dashboard.service;

import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Clock;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClock;

@Service
public class DashboardServiceImpl implements DashboardService {
	
	@Value("${jwt.token.secret}")
	private String jwtSecret;

	@Value("${jwt.token.expires}")
	private Long expires;

	@Value("${jwt.token.header}")
	private String tokenHeader;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private Clock clock = DefaultClock.INSTANCE;

	@Override
	public String generateHMACToken(String userName, Collection<? extends GrantedAuthority> authorities) {
		String token = Jwts.builder().setSubject(userName).setExpiration(new Date(System.currentTimeMillis() + expires))
				.signWith(SignatureAlgorithm.HS512, jwtSecret).compact();
		return token;
	}

	@Override
	public String refreshJwtToken(HttpServletRequest request) {
		String authToken = request.getHeader(tokenHeader);
		final String token = authToken.substring(7);
		Claims claims = getClaimsFromToken(token);
		return refreshToken(claims, token);
	}

	private String refreshToken(Claims claims, String token) {
		final Date createdDate = clock.now();
		claims.setIssuedAt(createdDate);
		claims.setExpiration(new Date(System.currentTimeMillis() + expires));
		String refreshedToken = Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS512, jwtSecret)
				.compact();
		logger.debug("Refreshed token: {}", refreshedToken);
		return refreshedToken;
	}

	private Claims getClaimsFromToken(String token) {
		final Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
		return claims;
	}
}
