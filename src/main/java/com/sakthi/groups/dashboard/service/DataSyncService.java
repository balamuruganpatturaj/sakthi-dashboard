package com.sakthi.groups.dashboard.service;

import java.util.List;

import com.sakthi.groups.migration.portal.model.BillInfo;
import com.sakthi.groups.migration.portal.model.BillItemDetails;
import com.sakthi.groups.migration.portal.model.BillSearchCriteria;

public interface DataSyncService {

	boolean syncSuppliers(boolean incrementalSync);

	boolean syncCustomers(boolean incrementalSync);

	boolean syncProducts(boolean incrementalSync);

	List<BillInfo> getBillInfoList(BillSearchCriteria searchCriteria);

	List<BillItemDetails> getBillDetailsList(Integer billNo, boolean isQuotationBill);

	BillInfo getSingleBillInfo(Integer billNo, boolean isQuotationBill);

	Double getBillAmount(Integer mBillNo, String phoneNo);

	Integer getPBillNumber(Integer billNo, String phoneNo);

	List<String> getCounterList();

	boolean syncPurchaseEntry();

}
