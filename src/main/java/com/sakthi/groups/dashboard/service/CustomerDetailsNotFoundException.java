package com.sakthi.groups.dashboard.service;

public class CustomerDetailsNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomerDetailsNotFoundException(String errorMessage) {
		super(errorMessage);
	}

	public CustomerDetailsNotFoundException() {
		super();
	}
}
