package com.sakthi.groups.dashboard.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sakthi.groups.migration.portal.model.PurchaseBillEntry;
import com.sakthi.groups.migration.portal.model.PurchaseBillItemDetails;
import com.sakthi.groups.migration.service.PurchaseEntryMigration;

@Service
public class PurchaseBillEntryServiceImpl implements PurchaseBillEntryService {

	@Autowired
	private PurchaseEntryMigration purchaseMigration;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public List<PurchaseBillEntry> getBillEntry(Integer supplierCode) {
		List<PurchaseBillEntry> billEntryList = new ArrayList<>();
		billEntryList = purchaseMigration.getPurchaseBillEntry(supplierCode);
		logger.info("Total bill entry list: {}", billEntryList.size());
		return billEntryList;
	}

	@Override
	public List<PurchaseBillItemDetails> getBillItemEntry(Integer productCode) {
		List<PurchaseBillItemDetails> billItemEntryList = new ArrayList<>();
		billItemEntryList = purchaseMigration.getPurchaseBillItemEntry(productCode);
		logger.info("Total bill entry list: {}", billItemEntryList.size());
		return billItemEntryList;
	}

	@Override
	public List<PurchaseBillItemDetails> getGrnBillItemEntry(Integer grnRefNo) {
		List<PurchaseBillItemDetails> billItemEntryList = new ArrayList<>();
		billItemEntryList = purchaseMigration.getGrnBillItemEntry(grnRefNo);
		logger.info("Total bill entry list: {}", billItemEntryList.size());
		return billItemEntryList;
	}

}
