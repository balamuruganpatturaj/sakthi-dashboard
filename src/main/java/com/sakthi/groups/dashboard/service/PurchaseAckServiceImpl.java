package com.sakthi.groups.dashboard.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.sakthi.groups.dashboard.bean.SupplierNames;
import com.sakthi.groups.dashboard.dao.InvoiceDetailsRepository;
import com.sakthi.groups.dashboard.dao.SupplierRepository;
import com.sakthi.groups.dashboard.model.InvoiceDetails;
import com.sakthi.groups.dashboard.model.SupplierMaster;
import com.sakthi.groups.dashboard.util.DashboardConstants;

@Service
public class PurchaseAckServiceImpl implements PurchaseAckService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private SupplierRepository supplierRepo;

	@Autowired
	private InvoiceDetailsRepository invoiceRepo;

	@Override
	@Cacheable("supplierNames")
	public List<SupplierNames> getSupplierNames() {
		List<SupplierMaster> supplierMaster = supplierRepo.findAll();
		List<SupplierNames> supplierMapList = new ArrayList<>();
		supplierMaster.forEach(supplier -> supplierMapList
				.add(new SupplierNames(supplier.getSupplierCode(), supplier.getSupplierName())));
		return supplierMapList;
	}

	@Override
	@Cacheable("supplierDetails")
	public List<SupplierMaster> getSupplierDetails() {
		List<SupplierMaster> supplierMasterList = supplierRepo.findAll(Sort.by(Sort.Direction.ASC, "supplierName"));
		return supplierMasterList;
	}

	@SuppressWarnings("deprecation")
	@Override
	@CacheEvict(cacheNames = { "supplierNames", "supplierDetails" }, allEntries = true)
	public boolean saveInvoiceDetails(List<InvoiceDetails> invoiceDetails) {
		boolean returnStatus = false;
		try {
			invoiceDetails.forEach(invoice -> {
				invoice.setInvoiceDate(new Date(DashboardConstants.dateFomatter.format(invoice.getInvoiceDate())));
				invoiceRepo.save(invoice);
			});
			returnStatus = true;
		} catch (Exception e) {
			logger.error("Failed to store the data in DB {}", e.getMessage());
		}
		return returnStatus;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean checkInvoiceAvailable(String supplierName, String invoiceNo, Date invoiceDate) {
		boolean returnStatus = false;
		try {
			Date invoiceDateFormatted = new Date(DashboardConstants.dateFomatter.format(invoiceDate));
			InvoiceDetails invoiceDetails = invoiceRepo.findBySupplierNameAndInvoiceNoAndInvoiceDate(supplierName,
					invoiceNo, invoiceDateFormatted);
			if (invoiceDetails != null) {
				returnStatus = true;
			}
		} catch (Exception e) {
			logger.error("Failed to parse the string to date {}", e.getMessage());
		}
		return returnStatus;
	}

	@Override
	@CacheEvict(cacheNames = { "supplierNames", "supplierDetails" }, allEntries = true)
	public boolean createNewSupplier(SupplierMaster supplierDetails, String loggedInUser) {
		boolean returnStatus = false;
		supplierDetails.setStatus(true);
		supplierDetails.setCreatedBy(loggedInUser);
		supplierDetails.setCreatedTime(new Date());
		if (supplierDetails.getSupplierGstNo() != null && !"".equals(supplierDetails.getSupplierGstNo())) {
			supplierDetails.setGstRegisterType("Registered");
		} else {
			supplierDetails.setGstRegisterType("Un-Registered");
		}
		try {
			supplierRepo.save(supplierDetails);
			returnStatus = true;
		} catch (Exception e) {
			logger.error("Failed to save the details in DB {}", e.getMessage());
		}
		return returnStatus;
	}

	@Override
	@CacheEvict(cacheNames = { "supplierNames", "supplierDetails" }, allEntries = true)
	public boolean editSupplier(SupplierMaster supplierDetails, String loggedInUser) {
		boolean returnStatus = false;
		supplierDetails.setUpdatedBy(loggedInUser);
		supplierDetails.setUpdatedTime(new Date());
		if (supplierDetails.getSupplierGstNo() != null && !"".equals(supplierDetails.getSupplierGstNo())) {
			supplierDetails.setGstRegisterType("Registered");
		} else {
			supplierDetails.setGstRegisterType("Un-Registered");
		}
		try {
			supplierRepo.save(supplierDetails);
			returnStatus = true;
		} catch (Exception e) {
			logger.error("Failed to save the details in DB {}", e.getMessage());
		}
		return returnStatus;
	}

}
