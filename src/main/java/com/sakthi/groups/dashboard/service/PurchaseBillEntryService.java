package com.sakthi.groups.dashboard.service;

import java.util.List;

import com.sakthi.groups.migration.portal.model.PurchaseBillEntry;
import com.sakthi.groups.migration.portal.model.PurchaseBillItemDetails;

public interface PurchaseBillEntryService {

	List<PurchaseBillEntry> getBillEntry(Integer supplierCode);

	List<PurchaseBillItemDetails> getBillItemEntry(Integer productCode);

	List<PurchaseBillItemDetails> getGrnBillItemEntry(Integer grnRefNo);

}
