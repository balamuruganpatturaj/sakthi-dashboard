package com.sakthi.groups.dashboard.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import com.sakthi.groups.dashboard.model.BatchChangeProvided;
import com.sakthi.groups.dashboard.model.BatchDeliveryDetails;
import com.sakthi.groups.migration.portal.model.DeliveryOrderDetail;

public interface DeliveryManageService {

	List<DeliveryOrderDetail> getdeliveryOrders();

	Set<String> getFilteredPhoneNum(String phoneNumber);

	boolean addDeliveryOrderDetail(List<DeliveryOrderDetail> deliveryOrderList);

	HashMap<String, String> getCustomerInfo(String phoneNumber);

	boolean checkDeliveryOrder(String bigInteger, Date orderPlacedDate);

	boolean updateDeliveryOrder(DeliveryOrderDetail deliveryOrder);

	Set<String> getPhoneNumbers();

	Set<String> getPhoneNumbers(String phoneNumber);

	List<DeliveryOrderDetail> getPendingdeliveryOrders();

	boolean updateDeliveryOrderList(List<DeliveryOrderDetail> deliveryOrder, UUID batchId);

	boolean checkDeliveryOrder(String phoneNumber, Integer billNo);
	
	String checkSimilarDeliveryOrder(String phoneNumber);

	List<BatchDeliveryDetails> getBatchdeliveryOrders();

	List<DeliveryOrderDetail> getBatchDeliveryOrders(UUID batchId);

	boolean closeDeliveryBatch(UUID batchId);

	boolean updatebatchDeliveryOrder(List<DeliveryOrderDetail> deliveryOrder, Double denominationTotal,
			List<BatchChangeProvided> changesProvided);

}
