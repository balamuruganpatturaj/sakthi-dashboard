package com.sakthi.groups.dashboard.service;

import java.util.Map;

public interface ConfigurationService {

	Map<String, String> getConfigDetails();

}
