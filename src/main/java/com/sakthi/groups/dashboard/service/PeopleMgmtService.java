package com.sakthi.groups.dashboard.service;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.bson.types.Binary;

import com.sakthi.groups.dashboard.bean.OfferInputDetail;
import com.sakthi.groups.dashboard.model.CustomerMaster;
import com.sakthi.groups.dashboard.model.EmployeeMaster;
import com.sakthi.groups.dashboard.model.GiftDetails;
import com.sakthi.groups.dashboard.model.OfferDetails;
import com.sakthi.groups.migration.portal.model.BillAnalyticsData;
import com.sakthi.groups.migration.portal.model.TopGrossTrendData;
import com.sakthi.groups.migration.portal.model.TopSellingTrendData;

public interface PeopleMgmtService {

	List<CustomerMaster> getCustomerDetails();

	boolean saveNewCustomer(CustomerMaster customer, String remoteUser);

	boolean editCustomer(CustomerMaster customer, String remoteUser);

	boolean addCustomerOffer(OfferDetails offerDetails);

	String checkCustomerOffer(OfferDetails offerDetails);

	boolean addGiftOffer(OfferInputDetail offerInputDetail);

	String checkGiftOffer(OfferInputDetail offerInputDetail);

	List<GiftDetails> getIssuedOffers();

	List<EmployeeMaster> getEmployeeDetails();

	boolean saveNewEmployee(EmployeeMaster employee, BigInteger unModifiedPhNo, String remoteUser);

	boolean editEmployee(EmployeeMaster employee, String remoteUser);

	Binary getAddressProof(BigInteger phoneNumber);

	Set<String> getCustomerId(String searchParam);

	Set<BigInteger> getCustomerPhoneNum(String searchParam);

	Set<BigInteger> getPhoneNumbers();

	Set<BigInteger> getPhoneNumbers(BigInteger phoneNumber);

	CustomerMaster getCustomerDetail(String searchParam);

	List<String> getCustomerNames();

	BillAnalyticsData getCustomerAnalyticsData(String customerCode, String searchDuration, Date startDate, Date endDate);

	List<TopSellingTrendData> getCustomerTrendData(String customerCode, String searchParam, Date searchStartDate,
			Date searchEndDate);

	List<TopGrossTrendData> getCustomerGrossTrendData(String customerCode, String searchParam, Date searchStartDate,
			Date searchEndDate);

}
