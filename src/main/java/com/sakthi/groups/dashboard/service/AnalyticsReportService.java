package com.sakthi.groups.dashboard.service;

import java.util.Date;
import java.util.List;

import com.sakthi.groups.dashboard.model.CustomerAnalyticsReport;
import com.sakthi.groups.migration.portal.model.BillAnalyticsData;
import com.sakthi.groups.migration.portal.model.BillSearchCriteria;
import com.sakthi.groups.migration.portal.model.BillTrendData;
import com.sakthi.groups.migration.portal.model.ProductSupplierWiseData;
import com.sakthi.groups.migration.portal.model.SalesPurchaseTrendData;
import com.sakthi.groups.migration.portal.model.TopSellingTrendData;

public interface AnalyticsReportService {

	BillTrendData getSalesTrendData(BillSearchCriteria searchCriteria);

	List<TopSellingTrendData> getTopSellingProduct(BillSearchCriteria searchCriteria);

	List<BillAnalyticsData> getBillAnalyticsData(BillSearchCriteria searchCriteria, boolean isSaveSearch);

	boolean checkCustomerAnalytics(BillSearchCriteria searchCriteria, boolean isSaveSearch);

	List<CustomerAnalyticsReport> getSavedCustomerAnalytics();

	boolean updateCustomerAnalytics(CustomerAnalyticsReport customerAnalyticsReport);

	List<BillAnalyticsData> getCustomerAnalyticsList(BillSearchCriteria searchCriteria);

	List<String> getFilteredProductNames(String productName);

	List<SalesPurchaseTrendData> getPurchaseTrend(String itemName, String searchParam, Date searchStartDate,
			Date searchEndDate);

	List<SalesPurchaseTrendData> getSalesTrend(String itemName, String searchParam, Date searchStartDate,
			Date searchEndDate);

	List<ProductSupplierWiseData> getProductSuppliedData(Integer supplierCode, String searchParam, Date searchStartDate,
			Date searchEndDate);

}
