package com.sakthi.groups.dashboard.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sakthi.groups.dashboard.dao.CompanyRepository;
import com.sakthi.groups.dashboard.model.Company;

@Service
public class CompanyServiceImpl implements CompanyService {

	@Autowired
	private CompanyRepository companyRepo;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public List<Company> getAvailableCompany(boolean isActive) {
		if (isActive) {
			return companyRepo.findByStatus(isActive);
		} else {
			return companyRepo.findAll();
		}
	}

	@Override
	public String createNewCompany(Company company) {
		String returnStatus = "";
		Company availableCompany = companyRepo.findByGstNo(company.getGstNo());
		if (availableCompany != null) {
			returnStatus = "Company already registered in DB";
			return returnStatus;
		} else {
			company.setStatus(true);
			companyRepo.save(company);
			returnStatus = "Successfully registered company in DB";
		}
		logger.debug(returnStatus);
		return returnStatus;
	}

	@Override
	public String editCompany(Company company) {
		String returnStatus = "";
		companyRepo.save(company);
		returnStatus = "Successfully saved the company in DB";
		logger.debug(returnStatus);
		return returnStatus;
	}

	@Override
	public Company getAvailableCompanyDetail(boolean isActive, String companyName) {
		return companyRepo.findByStatusAndCompanyName(isActive, companyName);
	}

}
