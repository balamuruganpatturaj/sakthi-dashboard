package com.sakthi.groups.dashboard.service;

import java.util.List;

import com.sakthi.groups.migration.portal.model.ProductMaster;

public interface ProductMgmtService {

	List<ProductMaster> getProductInventory();

}
