package com.sakthi.groups.dashboard.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.PatternSyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.sakthi.groups.dashboard.dao.CustomerReportRepository;
import com.sakthi.groups.dashboard.dao.ProductRepository;
import com.sakthi.groups.dashboard.model.CustomerAnalyticsReport;
import com.sakthi.groups.dashboard.util.DashboardConstants;
import com.sakthi.groups.dashboard.util.DashboardUtil;
import com.sakthi.groups.migration.portal.model.BillAnalyticsData;
import com.sakthi.groups.migration.portal.model.BillSearchCriteria;
import com.sakthi.groups.migration.portal.model.BillTrendData;
import com.sakthi.groups.migration.portal.model.ProductMaster;
import com.sakthi.groups.migration.portal.model.ProductSupplierWiseData;
import com.sakthi.groups.migration.portal.model.SalesPurchaseTrendData;
import com.sakthi.groups.migration.portal.model.TopSellingTrendData;
import com.sakthi.groups.migration.service.BillDataMigration;

@Service
public class AnalyticsReportServiceImpl implements AnalyticsReportService {

	@Autowired
	private BillDataMigration billDataMigration;

	@Autowired
	private CustomerReportRepository customerReportRepo;

	@Autowired
	private ProductRepository productRepo;

	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private DashboardUtil dashUtil;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public BillTrendData getSalesTrendData(BillSearchCriteria searchCriteria) {
		return billDataMigration.getSalesTrendData(searchCriteria);
	}

	@Override
	public List<TopSellingTrendData> getTopSellingProduct(BillSearchCriteria searchCriteria) {
		return billDataMigration.getTopSellingProduct(searchCriteria);
	}

	@Override
	public List<BillAnalyticsData> getBillAnalyticsData(BillSearchCriteria searchCriteria, boolean isSaveSearch) {
		searchCriteria.setLoyaltyAllowed(true);
		List<BillAnalyticsData> analyticsDataList = billDataMigration.getBillAnalyticsData(searchCriteria);
		if (isSaveSearch) {
			CustomerAnalyticsReport customerAnalyticsReport = checkCustomerReportExists(searchCriteria);
			if (customerAnalyticsReport != null) {
				customerAnalyticsReport.setAnalyticsDataList(analyticsDataList);
				customerAnalyticsReport.setSearchCriteria(searchCriteria);
				customerReportRepo.save(customerAnalyticsReport);
			} else {
				customerReportRepo.save(new CustomerAnalyticsReport(searchCriteria, analyticsDataList));
			}
		}
		return analyticsDataList;
	}

	private CustomerAnalyticsReport checkCustomerReportExists(BillSearchCriteria searchCriteria) {
		CustomerAnalyticsReport customerAnalyticsReport = customerReportRepo.findBySearchCriteria(searchCriteria);
		return customerAnalyticsReport;
	}

	@Override
	public boolean checkCustomerAnalytics(BillSearchCriteria searchCriteria, boolean isSaveSearch) {
		boolean returnStatus = false;
		CustomerAnalyticsReport customerAnalyticsReport = checkCustomerReportExists(searchCriteria);
		if (customerAnalyticsReport != null) {
			returnStatus = true;
		}
		return returnStatus;
	}

	@Override
	public List<CustomerAnalyticsReport> getSavedCustomerAnalytics() {
		return customerReportRepo.findAll();
	}

	@Override
	public boolean updateCustomerAnalytics(CustomerAnalyticsReport customerAnalyticsReport) {
		boolean returnStatus = false;
		CustomerAnalyticsReport customerAnalyticsReportList = checkCustomerReportExists(
				customerAnalyticsReport.getSearchCriteria());
		customerAnalyticsReport.getAnalyticsDataList().forEach(updatedData -> {
			customerAnalyticsReportList.getAnalyticsDataList().forEach(customerData -> {
				if (customerData.getCustomerCode().equals(updatedData.getCustomerCode())) {
					logger.info("Gift issued value {}", updatedData.isGiftIssued());
					customerData.setGiftIssued(Boolean.valueOf(updatedData.isGiftIssued()));
					customerData.setCustomerNotified(Boolean.valueOf(updatedData.isCustomerNotified()));
				}
			});
		});
		customerReportRepo.save(customerAnalyticsReportList);
		returnStatus = true;
		return returnStatus;
	}

	@Override
	public List<BillAnalyticsData> getCustomerAnalyticsList(BillSearchCriteria searchCriteria) {
		CustomerAnalyticsReport customerAnalyticsReportList = checkCustomerReportExists(searchCriteria);
		return customerAnalyticsReportList.getAnalyticsDataList();
	}

	@Override
	public List<String> getFilteredProductNames(String productName) {
		List<String> filteredProduct = new ArrayList<>();
		Query query = new Query();
		Criteria nameCriteria = new Criteria();
		Criteria aliasCriteria = new Criteria();
		Criteria itemCodeCriteria = new Criteria();
		boolean isItemCodeUsed = false;
		try {
			nameCriteria = Criteria.where("productName").regex(productName.replace(" ", ".*"), "i");
			aliasCriteria = Criteria.where("productAlias").regex(productName, "i");
			if ("".equals(productName.replaceAll("[0-9]", ""))) {
				isItemCodeUsed = true;
			}
			itemCodeCriteria = Criteria.where("productCode")
					.is(isItemCodeUsed ? Integer.valueOf(productName) : productName);
		} catch (PatternSyntaxException e) {
			logger.error("Exception occurred in creating query {}", e.getMessage());
		}
		Criteria filterCriteria = new Criteria().orOperator(nameCriteria, aliasCriteria, itemCodeCriteria);
		query.addCriteria(filterCriteria);
		List<ProductMaster> resultPurHistList = mongoTemplate.find(query, ProductMaster.class);
		resultPurHistList.forEach(entries -> filteredProduct.add(entries.getProductName()));
		return filteredProduct;
	}

	private BillSearchCriteria getSearchCriteria(Integer itemCode, Integer supplierCode, Date startDate, Date endDate) {
		BillSearchCriteria searchCriteria = new BillSearchCriteria();
		searchCriteria.setQuotationBill(false);
		searchCriteria.setPBillSearch(true);
		searchCriteria.setItemCode(itemCode);
		searchCriteria.setSupplierCode(supplierCode);
		String startDateStr = DashboardConstants.stringToDateFormatter.format(startDate);
		String endDateStr = DashboardConstants.stringToDateFormatter.format(endDate);
		searchCriteria.setBillStartDate(startDateStr);
		searchCriteria.setBillEndDate(endDateStr);
		return searchCriteria;
	}

	private List<SalesPurchaseTrendData> invokeDurationSpecific(Integer itemCode, String searchDuration,
			String productOption) {
		List<SalesPurchaseTrendData> purchaseTrendList = new ArrayList<SalesPurchaseTrendData>();
		HashMap<String, Date> dateMap = this.dashUtil.getSpecifiedDates(searchDuration);
		if ("Sales".equalsIgnoreCase(productOption)) {
			purchaseTrendList = billDataMigration.getSalesTrend(
					this.getSearchCriteria(itemCode, null, dateMap.get("startDate"), dateMap.get("endDate")));
		} else {
			purchaseTrendList = billDataMigration.getPurchaseTrend(
					this.getSearchCriteria(itemCode, null, dateMap.get("startDate"), dateMap.get("endDate")));
		}

		return purchaseTrendList;
	}

	private List<ProductSupplierWiseData> invokeSupplierDataSpecific(Integer supplierCode, String searchDuration) {
		List<ProductSupplierWiseData> productSupplierWiseList = new ArrayList<ProductSupplierWiseData>();
		HashMap<String, Date> dateMap = this.dashUtil.getSpecifiedDates(searchDuration);
		productSupplierWiseList = billDataMigration.getSupplierWiseData(
				this.getSearchCriteria(null, supplierCode, dateMap.get("startDate"), dateMap.get("endDate")));
		return productSupplierWiseList;
	}

	@Override
	public List<SalesPurchaseTrendData> getPurchaseTrend(String itemName, String searchParam, Date searchStartDate,
			Date searchEndDate) {
		List<SalesPurchaseTrendData> purchaseTrendList = new ArrayList<SalesPurchaseTrendData>();
		ProductMaster prodMaster = productRepo.findByProductNameIgnoreCase(itemName);
		Integer itemCode = prodMaster != null ? prodMaster.getProductCode() : 0;
		if (searchStartDate != null && searchEndDate != null) {
			purchaseTrendList = billDataMigration
					.getPurchaseTrend(this.getSearchCriteria(itemCode, null, searchStartDate, searchEndDate));
		} else {
			switch (searchParam) {
			case "current_month":
				purchaseTrendList = this.invokeDurationSpecific(itemCode, searchParam, "Purchase");
				break;

			case "last_month":
				purchaseTrendList = this.invokeDurationSpecific(itemCode, searchParam, "Purchase");
				break;

			case "current_year":
				purchaseTrendList = this.invokeDurationSpecific(itemCode, searchParam, "Purchase");
				break;

			case "last_year":
				purchaseTrendList = this.invokeDurationSpecific(itemCode, searchParam, "Purchase");
				break;

			default:
				break;
			}
		}
		return purchaseTrendList;
	}

	@Override
	public List<SalesPurchaseTrendData> getSalesTrend(String itemName, String searchParam, Date searchStartDate,
			Date searchEndDate) {
		List<SalesPurchaseTrendData> salesTrendList = new ArrayList<SalesPurchaseTrendData>();
		ProductMaster prodMaster = productRepo.findByProductNameIgnoreCase(itemName);
		Integer itemCode = prodMaster != null ? prodMaster.getProductCode() : 0;
		if (searchStartDate != null && searchEndDate != null) {
			salesTrendList = billDataMigration
					.getSalesTrend(this.getSearchCriteria(itemCode, null, searchStartDate, searchEndDate));
		} else {
			switch (searchParam) {
			case "current_month":
				salesTrendList = this.invokeDurationSpecific(itemCode, searchParam, "Sales");
				break;

			case "last_month":
				salesTrendList = this.invokeDurationSpecific(itemCode, searchParam, "Sales");
				break;

			case "current_year":
				salesTrendList = this.invokeDurationSpecific(itemCode, searchParam, "Sales");
				break;

			case "last_year":
				salesTrendList = this.invokeDurationSpecific(itemCode, searchParam, "Sales");
				break;

			default:
				break;
			}
		}
		return salesTrendList;
	}

	@Override
	public List<ProductSupplierWiseData> getProductSuppliedData(Integer supplierCode, String searchParam,
			Date searchStartDate, Date searchEndDate) {
		List<ProductSupplierWiseData> productSupplierWiseList = new ArrayList<ProductSupplierWiseData>();
		if (searchStartDate != null && searchEndDate != null) {
			productSupplierWiseList = billDataMigration
					.getSupplierWiseData(this.getSearchCriteria(null, supplierCode, searchStartDate, searchEndDate));
		} else {
			switch (searchParam) {
			case "current_month":
				productSupplierWiseList = this.invokeSupplierDataSpecific(supplierCode, searchParam);
				break;

			case "last_month":
				productSupplierWiseList = this.invokeSupplierDataSpecific(supplierCode, searchParam);
				break;

			case "current_year":
				productSupplierWiseList = this.invokeSupplierDataSpecific(supplierCode, searchParam);
				break;

			case "last_year":
				productSupplierWiseList = this.invokeSupplierDataSpecific(supplierCode, searchParam);
				break;

			default:
				break;
			}
		}
		return productSupplierWiseList;
	}

}
