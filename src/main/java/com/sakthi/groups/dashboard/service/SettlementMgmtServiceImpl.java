package com.sakthi.groups.dashboard.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sakthi.groups.dashboard.bean.SettlementReport;
import com.sakthi.groups.dashboard.bean.UpdateSettlementData;
import com.sakthi.groups.dashboard.dao.EdcPosDetailRepository;
import com.sakthi.groups.dashboard.dao.ReversalReportRepository;
import com.sakthi.groups.dashboard.dao.SettlementDetailRepository;
import com.sakthi.groups.dashboard.model.EdcPosDetail;
import com.sakthi.groups.dashboard.model.ReversalDetail;
import com.sakthi.groups.dashboard.model.ReversalReport;
import com.sakthi.groups.dashboard.model.SettlementDetail;
import com.sakthi.groups.dashboard.util.DashboardConstants;

@Service
public class SettlementMgmtServiceImpl implements SettlementMgmtService {

	@Autowired
	private EdcPosDetailRepository edcPosRepo;

	@Autowired
	private SettlementDetailRepository settlementRepo;

	@Autowired
	private ReversalReportRepository reversalReportRepo;

	private Integer settlementCnt = 0;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public List<EdcPosDetail> getAvailablePOS(boolean isActive) {
		if (isActive) {
			return edcPosRepo.findByStatus(isActive);
		} else {
			return edcPosRepo.findAll();
		}
	}

	@Override
	public String createNewPOSEntry(EdcPosDetail edcPosDetail) {
		String returnStatus = "";
		EdcPosDetail edcPosList = edcPosRepo.findByTidNo(edcPosDetail.getTidNo());
		if (edcPosList != null) {
			returnStatus = "POS machine already available";
			return returnStatus;
		} else {
			edcPosDetail.setStatus(true);
			edcPosRepo.save(edcPosDetail);
			returnStatus = "Successfully saved the POS machine";
		}
		logger.debug(returnStatus);
		return returnStatus;
	}

	@Override
	public String editPOSEntry(EdcPosDetail edcPosDetail) {
		String returnStatus = "";
		edcPosRepo.save(edcPosDetail);
		returnStatus = "Successfully saved the POS machine";
		logger.debug(returnStatus);
		return returnStatus;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean saveDailySettlement(SettlementDetail settlementDetail) {
		ObjectMapper objMapper = new ObjectMapper();
		boolean returnStatus = false;
		List<EdcPosDetail> posList = edcPosRepo.findByStatus(true);
		Map<Object, List<EdcPosDetail>> posGroupByData = posList.stream()
				.collect(Collectors.groupingBy(group -> List.of(group.getBankName(), group.getAssignedParty())));
		boolean isAvailable = this.checkSettlementStatus(settlementDetail);
		if (isAvailable) {
			Date txnDate = new Date(DashboardConstants.dateFomatter.format(settlementDetail.getTxnDate()));
			List<SettlementDetail> settlement = settlementRepo.findByTidNoAndTxnDate(settlementDetail.getTidNo(),
					txnDate);
			try {
				logger.info("Existing settlement details: {}", objMapper.writeValueAsString(settlement));
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
			settlement.forEach(settle -> {
				settlementDetail.setTotalTxn(settle.getTotalTxn().add(settlementDetail.getTotalTxn()));
				settlementDetail.setTotalAmount(settle.getTotalAmount().add(settlementDetail.getTxnAmount()));
				settlementDetail.setTxnAmount(settle.getTxnAmount().add(settlementDetail.getTxnAmount()));
				Set<ReversalDetail> reversalSet = settlementDetail.getAddReversal();
				Set<ReversalDetail> existReversalSet = settle.getAddReversal();
				if (existReversalSet != null) {
					reversalSet.addAll(settle.getAddReversal());
					settlementDetail.setAddReversal(reversalSet);
				}
				settlementRepo.delete(settle);
			});
		}
		EdcPosDetail edcPos = edcPosRepo.findByTidNo(settlementDetail.getTidNo());
		settlementDetail.setBankName(edcPos.getBankName());
		settlementDetail.setAssignedParty(edcPos.getAssignedParty());
		settlementDetail.setTotalAmount(settlementDetail.getTxnAmount());
		settlementDetail.setTxnDate((new Date(DashboardConstants.dateFomatter.format(settlementDetail.getTxnDate()))));
		int reversalLength = settlementDetail.getAddReversal().size();
		if (reversalLength == 1) {
			settlementDetail.getAddReversal().forEach(reversal -> {
				if (reversal.getReversalAmount() == null) {
					settlementDetail.setAddReversal(null);
				}
			});
		}
		settlementRepo.save(settlementDetail);
		posGroupByData.forEach((group, edcList) -> {
			if (edcList.size() > 1) {
				List<SettlementDetail> settlementList = settlementRepo.findByBankNameAndAssignedPartyAndTxnDate(
						settlementDetail.getBankName(), settlementDetail.getAssignedParty(),
						settlementDetail.getTxnDate());
				BigDecimal totalTxnAmt = settlementList.stream().map(settle -> settle.getTxnAmount())
						.reduce(BigDecimal.ZERO, BigDecimal::add);
				logger.info("totalTxnAmt: {}", totalTxnAmt);
				settlementList.forEach(settle -> {
					settle.setTotalAmount(totalTxnAmt);
					settlementRepo.save(settle);
				});
			}
		});
		returnStatus = true;
		return returnStatus;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean checkSettlementStatus(SettlementDetail settlementDetail) {
		boolean returnStatus = false;
		try {
			Date txnDate = new Date(DashboardConstants.dateFomatter.format(settlementDetail.getTxnDate()));
			List<SettlementDetail> settlement = settlementRepo.findByTidNoAndTxnDate(settlementDetail.getTidNo(),
					txnDate);
			if (settlement != null && !settlement.isEmpty()) {
				returnStatus = true;
			}
		} catch (Exception e) {
			logger.error("Failed to check the status of settlement {}", e.getMessage());
		}
		return returnStatus;
	}

	@Override
	public List<SettlementReport> getSettlementReport() {
		List<SettlementReport> setReportList = new ArrayList<>();
		List<SettlementDetail> settlementList = settlementRepo.findAll();
		Map<Object, List<SettlementDetail>> groupByData = settlementList.stream().collect(Collectors
				.groupingBy(group -> List.of(group.getBankName(), group.getAssignedParty(), group.getTxnDate())));
		groupByData.forEach((group, edcList) -> {
			settlementCnt = 0;
			edcList.forEach(edc -> {
				SettlementReport settlementReport = new SettlementReport();
				settlementReport.setTidNo(edc.getTidNo());
				if (settlementCnt == 0) {
					settlementReport.setAssignedParty(edc.getAssignedParty());
					settlementReport.setBankName(edc.getBankName());
					settlementReport.setRowSpan(edcList.size());
					settlementReport.setSettledAmount(edc.getSettledAmount());
					settlementReport.setTotalAmount(edc.getTotalAmount());
				}
				settlementReport.setTxnDate(edc.getTxnDate());
				settlementReport.setTotalTxn(edc.getTotalTxn());
				settlementReport.setTxnAmount(edc.getTxnAmount());
				settlementReport.setAddReversal(edc.getAddReversal());
				setReportList.add(settlementReport);
				settlementCnt++;
			});
		});
		return setReportList;
	}

	@Override
	public boolean updateDailySettlement(UpdateSettlementData updateSettlementData) {
		boolean returnStatus = false;
		updateSettlementData.getTidNo().forEach(tidNo -> {
			Set<ReversalDetail> reversalSet = new HashSet<>();
			List<SettlementDetail> settlement = settlementRepo.findByTidNoAndTxnDate(tidNo,
					updateSettlementData.getTxnDate());
			settlement.forEach(settle -> {
				settle.setSettledAmount(updateSettlementData.getSettledAmount());
				settle.setDifferenceAmt(updateSettlementData.getDifferenceAmt());
				if (tidNo.compareTo(settle.getTidNo()) == 0) {
					updateSettlementData.getReversalData().forEach(reversal -> {
						if (settle.getAddReversal() != null) {
							settle.getAddReversal().forEach(existingReversal -> {
								if (existingReversal.getCardNumber().equals(reversal.getCardNumber())
										&& existingReversal.getCardType().equals(reversal.getCardType())
										&& existingReversal.getReversalAmount()
												.compareTo(reversal.getReversalAmount()) == 0) {
									reversalSet.add(reversal);
									if (reversal.isAmountRevered()) {
										ReversalReport existingOne = reversalReportRepo
												.findByTidNoAndTxnDateAndReversalAmount(tidNo, settle.getTxnDate(),
														reversal.getReversalAmount());
										ReversalReport reversalReport = new ReversalReport();
										if (existingOne == null) {
											reversalReport = new ReversalReport(tidNo, settle.getAssignedParty(),
													settle.getTxnDate(), reversal.getCardType(),
													reversal.getCardNumber(), reversal.getReversalAmount(),
													reversal.isAmountRevered(), reversal.isAmountRefunded());
											reversalReportRepo.save(reversalReport);
										} else {
											existingOne.setAmountRevered(reversal.isAmountRevered());
											reversalReportRepo.save(existingOne);
										}
									}
								}
							});
						}
					});
				}
				settle.setAddReversal(reversalSet);
				settlementRepo.save(settle);
			});
		});
		returnStatus = true;
		return returnStatus;
	}

	@Override
	public List<ReversalReport> getReversalReport() {
		return reversalReportRepo.findAll();
	}

	@Override
	public boolean updateReversalRefund(ReversalReport reversalReport) {
		boolean returnStatus = false;
		ReversalReport existingEntry = reversalReportRepo.findByTidNoAndTxnDateAndReversalAmount(
				reversalReport.getTidNo(), reversalReport.getTxnDate(), reversalReport.getReversalAmount());
		reversalReport.setId(existingEntry.getId());
		reversalReportRepo.save(reversalReport);
		returnStatus = true;
		return returnStatus;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean editDailySettlement(SettlementDetail settlementDetail) {
		boolean returnStatus = false;
		List<EdcPosDetail> posList = edcPosRepo.findByStatus(true);
		Map<Object, List<EdcPosDetail>> posGroupByData = posList.stream()
				.collect(Collectors.groupingBy(group -> List.of(group.getBankName(), group.getAssignedParty())));
		Date txnDate = new Date(DashboardConstants.dateFomatter.format(settlementDetail.getTxnDate()));
		List<SettlementDetail> settlement = settlementRepo.findByTidNoAndTxnDate(settlementDetail.getTidNo(), txnDate);
		settlement.forEach(settle -> {
			settle.setTotalTxn(settlementDetail.getTotalTxn());
			settle.setTxnAmount(settlementDetail.getTxnAmount());
			settle.setTotalAmount(settlementDetail.getTxnAmount());
			settle.setTxnDate((new Date(DashboardConstants.dateFomatter.format(settlementDetail.getTxnDate()))));
			settle.setAddReversal(settlementDetail.getAddReversal());
			int reversalLength = settlementDetail.getAddReversal().size();
			if (reversalLength == 1) {
				settlementDetail.getAddReversal().forEach(reversal -> {
					if (reversal.getReversalAmount() == null) {
						settle.setAddReversal(null);
					}
				});
			}
			settlementRepo.save(settle);
		});
		EdcPosDetail edcPos = edcPosRepo.findByTidNo(settlementDetail.getTidNo());
		settlementDetail.setBankName(edcPos.getBankName());
		settlementDetail.setAssignedParty(edcPos.getAssignedParty());
		settlementDetail.setTxnDate((new Date(DashboardConstants.dateFomatter.format(settlementDetail.getTxnDate()))));

		posGroupByData.forEach((group, edcList) -> {
			if (edcList.size() > 1) {
				List<SettlementDetail> settlementList = settlementRepo.findByBankNameAndAssignedPartyAndTxnDate(
						settlementDetail.getBankName(), settlementDetail.getAssignedParty(),
						settlementDetail.getTxnDate());
				BigDecimal totalTxnAmt = settlementList.stream().map(settle -> settle.getTxnAmount())
						.reduce(BigDecimal.ZERO, BigDecimal::add);
				logger.info("totalTxnAmt: {}", totalTxnAmt);
				settlementList.forEach(settle -> {
					settle.setTotalAmount(totalTxnAmt);
					settlementRepo.save(settle);
				});
			}
		});
		returnStatus = true;
		return returnStatus;
	}

}
