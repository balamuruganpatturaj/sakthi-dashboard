package com.sakthi.groups.dashboard.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sakthi.groups.dashboard.dao.AppConstantRepository;
import com.sakthi.groups.dashboard.model.AppConstants;

@Service
public class ConfigurationServiceImpl implements ConfigurationService {

	@Autowired
	private AppConstantRepository appConstantRepo;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public Map<String, String> getConfigDetails() {
		ObjectMapper objectMapper = new ObjectMapper();
		Iterable<AppConstants> appConstants = appConstantRepo.findAll();
		Map<String, String> propsMap = new HashMap<String, String>();
		try {
			propsMap = objectMapper.readValue(appConstants.iterator().next().getAppConstants(),
					new TypeReference<Map<String, Object>>() {
					});
		} catch (IOException e) {
			logger.error("Failed to parse the JSON string {}", e.getMessage());
		}
		return propsMap;
	}

}
