package com.sakthi.groups.dashboard.service;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sakthi.groups.migration.service.RodeoDataCheckService;

@Service
public class RodeoDataVerifyServiceImpl implements RodeoDataVerifyService {

	@Autowired
	private RodeoDataCheckService rodeoCheckService;

	@Override
	public byte[] getMissingProducts(InputStream excelStream, Integer daysToCheck, String chooseItemFrom) {
		ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
		if ("Purchase".equalsIgnoreCase(chooseItemFrom)) {
			byteArrayStream = rodeoCheckService.checkActivePurchaseItems((FileInputStream) excelStream, daysToCheck);
		} else if ("Repackage".equalsIgnoreCase(chooseItemFrom)) {
			byteArrayStream = rodeoCheckService.checkRepackageItems((FileInputStream) excelStream, daysToCheck);
		} else {
			byteArrayStream = rodeoCheckService.checkActiveSalesItems((FileInputStream) excelStream, daysToCheck);
		}
		return byteArrayStream.toByteArray();
	}

}
