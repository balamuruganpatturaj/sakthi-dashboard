package com.sakthi.groups.dashboard.service;

import java.util.List;
import java.util.Set;

import com.sakthi.groups.dashboard.bean.ChennaiProductInfo;
import com.sakthi.groups.dashboard.model.ChennaiPurchase;

public interface ChennaiPurchaseService {

	Set<String> getFilteredProductNames(String productName);

	ChennaiProductInfo getProductInfo(String productName);
	
	boolean isPurchaseAlreadyAdded(ChennaiPurchase chennaiPurchase);

	boolean saveChennaiPurchase(List<ChennaiPurchase> chennaiPurchaseList);

	boolean deleteProduct(List<ChennaiPurchase> chennaiPurchaseList);
	
	List<ChennaiPurchase> getChennaiPurchase(String prodName, String dateSlotValue);

	boolean updateChennaiPurchase(List<ChennaiPurchase> chennaiPurchaseList);

	ChennaiProductInfo getEanCodeProductInfo(String eanCode);

}
