package com.sakthi.groups.dashboard.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;

public class WhatsAppApiServiceImpl implements WhatsAppApiService {
	
	@Value("${wati.api.endpoint}")
	private String watiApiEndPoint;
	
	@Value("${wati.api.token}")
	private String watiAuthorizeToken;
	
	@Autowired
	private RestTemplate restTemplate;

}
