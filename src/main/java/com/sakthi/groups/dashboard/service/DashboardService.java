package com.sakthi.groups.dashboard.service;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.GrantedAuthority;

public interface DashboardService {

	String generateHMACToken(String userName, Collection<? extends GrantedAuthority> authorities);

	String refreshJwtToken(HttpServletRequest request);

}
