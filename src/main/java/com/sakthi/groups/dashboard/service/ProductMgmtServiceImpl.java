package com.sakthi.groups.dashboard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.sakthi.groups.dashboard.dao.ProductRepository;
import com.sakthi.groups.migration.portal.model.ProductMaster;

@Service
public class ProductMgmtServiceImpl implements ProductMgmtService {

	@Autowired
	private ProductRepository productRepo;

	@Override
	public List<ProductMaster> getProductInventory() {
		return productRepo.findAll(Sort.by(Sort.Direction.ASC, "productName"));
	}

}
